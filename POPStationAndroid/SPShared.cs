﻿using System;
using Android.Graphics;

namespace POPStationAndroid
{
	public class SPShared
	{
		public Color RedTintColor { get; private set; }
		public Color BlueTintColor { get; private set; }
		public Color BlackTintColor { get; private set; }
		public Color DarkGreyColor { get; private set; }
		public Color MidGreyColor { get; private set; }
		public Color LightGreyColor { get; private set; }
		public Color DirtyWhiteColor { get; private set; }
		public Color WhiteColor { get; private set; }

		#region Singleton Instance
		private static SPShared m_Instance;
		public static SPShared Instance
		{
			get
			{
				if( m_Instance == null )
				{
					m_Instance = new SPShared();
				}
				return m_Instance;
			}
		}

		// Singleton requires a private constructor.
		private SPShared ()
		{
			RedTintColor = new Color (212, 64, 54);
			BlueTintColor = new Color (51, 110, 166);
			BlackTintColor = Color.Black;
			DarkGreyColor = new Color (26, 26, 26);
			MidGreyColor = new Color (87, 87, 87);
			LightGreyColor = new Color (168, 168, 168);
			DirtyWhiteColor = new Color (230, 230, 230);
			WhiteColor = Color.White;

		}
		#endregion Singeton Instance


	}
}

