﻿using System;
using Android.Support.V4.App;
using Android.Views;
using Android.OS;

namespace POPStationAndroid
{
	public class MyParcelFragment : Fragment
	{
		public MyParcelFragment ()
		{
			this.RetainInstance = true;
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var ignored = base.OnCreateView (inflater, container, savedInstanceState);
			var view = inflater.Inflate (Resource.Layout.my_parcel_fragment, null);

			return view;
		}
	}
}

