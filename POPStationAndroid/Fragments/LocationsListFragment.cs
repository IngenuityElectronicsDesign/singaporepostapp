﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using POPStation.Core.iOS;

namespace POPStationAndroid
{
	public class LocationsListFragment : Fragment
	{
		ExpandableListAdapter m_ListAdapter;
		ExpandableListView m_ExpListView;
		List<string> m_ListDataHeader;
		Dictionary<POPStationLocationData.Regions, List<POPStationLocationData>> m_ListDataChild;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here

		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.locations_list_fragment, null);
			m_ExpListView = (ExpandableListView)view.FindViewById (Resource.Id.locationExpandable);
			PrepateListData ();

			m_ListAdapter = new ExpandableListAdapter (container.Context, m_ListDataHeader, m_ListDataChild);
			m_ExpListView.SetAdapter (m_ListAdapter);

			return view;
		}

		private void PrepateListData() 
		{
			m_ListDataHeader = new List<string> ();
			m_ListDataChild = new Dictionary<POPStationLocationData.Regions, List<POPStationLocationData>> ();
			for( int i = 0; i < (int)POPStationLocationData.Regions.Count; i++ )
			{
				m_ListDataHeader.Add(((POPStationLocationData.Regions)i).ToString ());
				m_ListDataChild [(POPStationLocationData.Regions)i] = new List<POPStationLocationData> ();
			}
				

			SPServerData.Instance.ServerSearchLocation ("", 100, (searchSuccess) => {
				if( searchSuccess )
				{
					foreach( POPStationLocationData location in SPSharedData.Instance.POPStationLocationList )
					{
						m_ListDataChild[location.Region].Add(location);
					}
				}

				this.Activity.RunOnUiThread(() => {
					m_ListAdapter.NotifyDataSetChanged();
				});
			});


		}
	}
}

