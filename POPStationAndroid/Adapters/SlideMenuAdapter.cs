﻿using System;
using Android.Widget;
using Android.Content;
using Android.Views;
using System.Collections.Generic;
using POPStation.Core.iOS;
using Android.Content.Res;

namespace POPStationAndroid
{
	public class SlideMenuAdapter : BaseAdapter
	{
		private static readonly string[] UserMenuTitles = new string[] { "MY PARCEL", "LOCATIONS", "MERCHANTS", "FAQ", "HELP", "ACCOUNT", "FEEDBACK", "TERMS OF USE", "RATE OUR APP" };
		private static readonly string[] GuestMenuTitles = new string[] { "MY PARCEL", "LOCATIONS", "MERCHANTS", "FAQ", "HELP", "FEEDBACK", "TERMS OF USE", "RATE OUR APP" };

		private static readonly string[] UserStoryboardIDs = new string[] { "MyParcelMemberTVC", "LocationsVC", "EMerchantTVC", "FAQVC", "HelpVC", "AccountTVC", "FeedbackVC", "TermsOfUseVC", "" };
		private static readonly string[] GuestStoryboardIDs = new string[] { "MyParcelGuestVC", "LocationsVC", "EMerchantTVC", "FAQVC", "HelpVC", "FeedbackVC", "TermsOfUseVC", "" };

		private int m_Selected = 0;

		private static readonly int[] MenuIcons = new int[] {
			Resource.Drawable.navi_myparcels,
			Resource.Drawable.navi_location,
			Resource.Drawable.navi_merchants,
			Resource.Drawable.navi_faq,
			Resource.Drawable.navi_help,
			Resource.Drawable.navi_account
		};
		private static readonly int[] MenuIconsActive = new int[] {
			Resource.Drawable.navi_myparcels_active,
			Resource.Drawable.navi_location_active,
			Resource.Drawable.navi_merchants_active,
			Resource.Drawable.navi_faq_active,
			Resource.Drawable.navi_help_active,
			Resource.Drawable.navi_account_active
		};

		public SlideMenuAdapter() { }

		public override int Count {
			get {
				return SPSharedData.Instance.IsLoggedIn ? UserMenuTitles.Length : GuestMenuTitles.Length;
			}
		}

		public override Java.Lang.Object GetItem (int position)
		{
			return null;
		}

		public override long GetItemId (int position)
		{
			return position;
		}
		
		public override Android.Views.View GetView (int position, View convertView, ViewGroup parent)
		{
			var imageLimit = SPSharedData.Instance.IsLoggedIn ? MenuIcons.Length : MenuIcons.Length - 1;
			bool isImageView = position < imageLimit;
			LayoutInflater inflater = (LayoutInflater)parent.Context.GetSystemService(Context.LayoutInflaterService);
			View rowView = inflater.Inflate (isImageView ? Resource.Layout.slide_list_item_image : Resource.Layout.slide_list_item, parent, false);
			TextView textView = rowView.FindViewById<TextView> (Resource.Id.slide_list_text);
			textView.Text = SPSharedData.Instance.IsLoggedIn ? UserMenuTitles [position] : GuestMenuTitles [position];
			// If it isn't an image view, we set the background to dirty white
			if (isImageView) {
				ImageView imageView = rowView.FindViewById<ImageView> (Resource.Id.slide_list_image);
				if(m_Selected == position)
				{
					imageView.SetImageResource (MenuIconsActive [position]);
					rowView.SetBackgroundColor (parent.Context.Resources.GetColor (Resource.Color.nav_selected));
					imageView.SetBackgroundColor (parent.Context.Resources.GetColor (Resource.Color.nav_selected));
					textView.SetBackgroundColor (parent.Context.Resources.GetColor (Resource.Color.nav_selected));
				}
				else
				{
					imageView.SetImageResource (MenuIcons [position]);
				}
			}
			else 
			{
				rowView.SetBackgroundColor (parent.Context.Resources.GetColor (Resource.Color.dirty_white));
				textView.SetBackgroundColor (parent.Context.Resources.GetColor (Resource.Color.dirty_white));
			}
			// If it is the selected row, we set the text to blue on all rows
			if(m_Selected == position)
			{
				textView.SetTextColor (parent.Context.Resources.GetColor (Resource.Color.blue_tint));
			}
			return rowView;
		}

		public void SetSelected(int position)
		{
			if(m_Selected != position)
			{
				m_Selected = position;
				NotifyDataSetChanged ();
			}
		}
	}
}

