﻿using System;
using Android.Widget;
using Android.Content;
using Android.Views;

using System.Collections.Generic;
using POPStation.Core.iOS;

namespace POPStationAndroid
{
	public class ExpandableListAdapter : BaseExpandableListAdapter
	{
		private Context m_Context; 
		private List<string> m_ListDataHeaders; // header titles
		// Child data in format of header title, child title
		private Dictionary<POPStationLocationData.Regions, List<POPStationLocationData>> m_ListDataChild;

		public ExpandableListAdapter(Context context, List<string> listDataHeaders, Dictionary<POPStationLocationData.Regions, List<POPStationLocationData>> listDataChild)
		{
			m_Context = context;
			m_ListDataHeaders = listDataHeaders;
			m_ListDataChild = listDataChild;
		}

		public override Java.Lang.Object GetChild (int groupPosition, int childPosition)
		{
			return m_ListDataChild [(POPStationLocationData.Regions)groupPosition] [childPosition];
		}

		public override long GetChildId (int groupPosition, int childPosition)
		{
			return childPosition;
		}

		public override View GetChildView (int groupPosition, int childPosition, bool isLastChild, View convertView, ViewGroup parent)
		{
			POPStationLocationData childData = (POPStationLocationData)GetChild (groupPosition, childPosition);

			if(convertView == null) {
				LayoutInflater inflater = (LayoutInflater)this.m_Context.GetSystemService(Context.LayoutInflaterService);
				convertView = inflater.Inflate (Resource.Layout.location_list_item, null);
			}

			TextView nameLabel = (TextView)convertView.FindViewById (Resource.Id.locationListNameLabel);
			nameLabel.Text = childData.POPStationName;
			TextView addressLabel = (TextView)convertView.FindViewById (Resource.Id.locationListAddressLabel);
			addressLabel.Text = childData.FullAddress;
			TextView accessLabel = (TextView)convertView.FindViewById (Resource.Id.locationListAccessLabel);
			accessLabel.Text = childData.OperationHours;

			return convertView;
		}

		public override int GetChildrenCount (int groupPosition)
		{
			return m_ListDataChild [(POPStationLocationData.Regions)groupPosition].Count;
		}

		public override Java.Lang.Object GetGroup (int groupPosition)
		{
			return m_ListDataHeaders [groupPosition];
		}

		public override int GroupCount {
			get {
				return m_ListDataHeaders.Count;
			}
		}

		public override long GetGroupId (int groupPosition)
		{
			return groupPosition;
		}

		public override View GetGroupView (int groupPosition, bool isExpanded, View convertView, ViewGroup parent)
		{
			string headerTitle = (string)GetGroup (groupPosition);
			if(convertView == null)
			{
				LayoutInflater inflater = (LayoutInflater)m_Context.GetSystemService (Context.LayoutInflaterService);
				convertView = inflater.Inflate (Resource.Layout.location_list_group, null);
			}

			TextView labelListHeader = (TextView)convertView.FindViewById (Resource.Id.locationListHeader);
			labelListHeader.Text = headerTitle;

			return convertView;
		}

		public override bool HasStableIds {
			get {
				return false;
			}
		}

		public override bool IsChildSelectable (int groupPosition, int childPosition)
		{
			return true;
		}
	}
}

