﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Support.V4.App;

namespace POPStationAndroid
{

	[Activity (Label = "POP Station", MainLauncher = true, Icon = "@drawable/icon")]
	public class SlideMenuActivity : FragmentActivity
	{
		private string m_Title;
		private string m_DrawerTitle;
		private DrawerLayout m_Drawer;
		private ListView m_DrawerList;
		private LinearLayout m_NavLayout;
		private SlideMenuAdapter m_Adapter;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.slide_menu);

			this.m_Title = this.m_DrawerTitle = this.Title;

			this.m_Drawer = this.FindViewById<DrawerLayout> (Resource.Id.drawer_layout);
			this.m_DrawerList = this.FindViewById<ListView> (Resource.Id.left_drawer);

			m_Adapter = new SlideMenuAdapter ();
			this.m_DrawerList.Adapter = m_Adapter;
			this.m_DrawerList.ItemClick += DrawerListOnItemClick;

			m_NavLayout = FindViewById<LinearLayout> (Resource.Id.nav_layout);

		}

		private void DrawerListOnItemClick(object sender, AdapterView.ItemClickEventArgs itemClickEventArgs)
		{
			Android.Support.V4.App.Fragment fragment = null;
			switch(itemClickEventArgs.Position)
			{
				case 0: // My Parcel
					fragment = new MyParcelFragment ();
					m_Adapter.SetSelected (0);
					break;
				case 1: // Locations
					fragment = new LocationsListFragment ();
					m_Adapter.SetSelected (1);
					break;
				case 2: // Merchants
					// TODO : Create Fragment
					m_Adapter.SetSelected (2);
					break;
				case 3: // FAQ
					// TODO : Create Fragment
					m_Adapter.SetSelected (3);
					break;
				case 4: // Help
					// TODO : Create Fragment
					m_Adapter.SetSelected (4);
					break;
				case 5: // Feedback
					// TODO : Create Fragment
					m_Adapter.SetSelected (5);
					break;
				case 6:	// Terms of Use
					// TODO : Create Fragment
					m_Adapter.SetSelected (6);
					break;
				case 7: // Rate out app
					// TODO : Create Fragment
					m_Adapter.SetSelected (7);
					break;
				default:
					Console.WriteLine ("Invalid position clicked : " + itemClickEventArgs.Position);
					return;
			}
			if(fragment == null)
			{
				Console.WriteLine ("Item not implemented yet : " + itemClickEventArgs.Position);
				return;
			}
			SupportFragmentManager.BeginTransaction ()
				.Replace (Resource.Id.content_frame, fragment)
				.Commit ();

			this.m_DrawerList.SetItemChecked (itemClickEventArgs.Position, true);
			this.m_Drawer.CloseDrawer (this.m_NavLayout);
		}
	}
}


