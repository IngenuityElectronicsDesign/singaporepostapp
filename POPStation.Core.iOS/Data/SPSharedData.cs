﻿using System;
using System.Collections.Generic;

namespace POPStation.Core.iOS
{
	public class SPSharedData
	{
		#region Properties
		// Server
		public bool IsLoggedIn { get; set; }
		public bool PushNotificationsEnabled { get; set; }
		public string PushNotificationsTokenID { get; set; }

		// Data
		public UserProfileData UserProfile { get; set; }
		public List<POPStationLocationData> POPStationLocationList { get; private set; }
		public List<EMerchantData> EMerchantList { get; set; }
		public List<EMerchantBannerData> EMerchantBannerList { get; set; }

		// Productions URLs
//		public const string PopstationURLMain = "https://www.mypopstation.com/";
//		public const string PopstationURLPlugins = "https://plugins.mypopstation.com/api/";
//		public const string PopstationURLSecurePlugins = "https://plugins.mypopstation.com/API/";
//		public const string PopstationURLImages = "http://www.mypopstation.com/MediaDb/Default/popstations/";

		// Testing URLs
		public const string PopstationURLMain = "http://popstationuat.mypopstation.com/";
		public const string PopstationURLPlugins = "http://uat.plugins.mypopstation.com/api/";
		public const string PopstationURLSecurePlugins = "http://uat.plugins.mypopstation.com/API/";
		public const string PopstationURLImages = "http://popstationuat.mypopstation.com/MediaDb/Default/popstations/";
		#endregion Properties

		#region Singleton Instance
		private static SPSharedData m_Instance;
		public static SPSharedData Instance
		{
			get
			{
				if( m_Instance == null )
				{
					m_Instance = new SPSharedData();
				}
				return m_Instance;
			}
		}
		#endregion Singeton Instance

		public SPSharedData()
		{
			IsLoggedIn = false;

			POPStationLocationList = new List<POPStationLocationData>();
		}

		public void SetSearchLocationList( List<SearchLocationResponse> respList )
		{
			POPStationLocationList.Clear();

			foreach( SearchLocationResponse resp in respList )
			{
				POPStationLocationList.Add(new POPStationLocationData(resp));
			}
		}
	}
}
