﻿using System;
using System.Text;

namespace POPStation.Core.iOS
{
	public class AuthToken
	{
		public string EncodedToken { get; private set; }

		public AuthToken(string email, string token)
		{
			Guid Token = Guid.Parse(token);

			string authString = string.Format("{0}:{1}", email, Token);
			byte[] authBytes = Encoding.UTF8.GetBytes(authString);
			EncodedToken = Convert.ToBase64String(authBytes);
		}

		public AuthToken(string encodedToken)
		{
			EncodedToken = encodedToken;
		}
	}
}
