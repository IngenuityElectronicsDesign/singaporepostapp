﻿using System;
using System.Collections.Generic;

namespace POPStation.Core.iOS
{
	public class TrackAndTraceSearchData
	{
		public string TrackingNo { get; set; }
		public string DestinationCountry { get; set; }
		public string OriginCountry { get; set; }
		public List<TrackingDetailData> TrackingDetails { get; set; }
	}
}

