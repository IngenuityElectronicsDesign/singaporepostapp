﻿using System;
using System.Collections.Generic;

namespace POPStation.Core.iOS
{
	public class RemotePickupResponse
	{
		public bool Success { get; set; }
		public string Error { get; set; }
		public string Data { get; set; }
	}
}
