﻿using System;
using System.Collections.Generic;


namespace POPStation.Core.iOS
{
	public class POPStationLocationData
#if __ANDROID__
		: Java.Lang.Object
#endif
	{
		public enum Regions
		{
			Central,
			West,
			North,
			NorthEast,
			East,

			Count
		}

		public string KioskId { get; private set; }
		public string POPStationName { get; private set; }
		public string FullAddress { get; private set; }
		public string OperationHours { get; private set; }
		public string LocationDescription { get; private set; }
		public int Distance { get; private set; }
		public double Latitude { get; private set; }
		public double Longitude { get; private set; }

		public Regions Region { get; private set; }

		public string ImageFileURL { get; private set; }
		public string LargeImageFileURL { get; private set; }

		public POPStationLocationData(SearchLocationResponse resp)
		{
			KioskId = resp.KioskId;
			POPStationName = resp.POPStationName;
			FullAddress = resp.HouseBlockNumber + " " + resp.StreetName + ", Singapore " + resp.ZipCode;
			OperationHours = resp.OperationHours;
			LocationDescription = resp.Location;
			Distance = resp.Distance;
			Latitude = resp.Latitude;
			Longitude = resp.Longitude;

			Region = GetRegionForName(resp.Region);

			ImageFileURL = SPSharedData.PopstationURLImages + KioskId + ".jpg";
			LargeImageFileURL = SPSharedData.PopstationURLImages + KioskId + "-large.jpg";
		}

		public Regions GetRegionForName(string region)
		{
			switch( region )
			{
				case "central":			return Regions.Central;
				case "west":			return Regions.West;
				case "north":			return Regions.North;
				case "northeast":		return Regions.NorthEast;
				case "east":			return Regions.East;
				default:				return Regions.Count;
			}
		}

		public bool ContainsString(string searchString)
		{
			string data = KioskId + " " + POPStationName + " " + FullAddress;

			if( data.ToLower().Contains(searchString.ToLower()) )
			{
				return true;
			}
			return false;
		}
	}

	public class SearchLocationResponse
	{
		public string KioskId { get; set; }
		public string POPStationName { get; set; }
		public string Storey { get; set; }
		public string UnitNumber { get; set; }
		public string HouseBlockNumber { get; set; }
		public string BuildingName { get; set; }
		public string StreetName { get; set; }
		public string PostCode { get; set; }
		public string Region { get; set; }
		public string Location { get; set; }
		public string OperationHours { get; set; }
		public int Distance { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
		public string ZipCode { get; set; }
	}
}
