﻿using System;
using System.Collections.Generic;

namespace POPStation.Core.iOS
{
	public class PushStringResponse
	{
		public bool Success { get; set; }
		public string Data { get; set; }
	}
}
