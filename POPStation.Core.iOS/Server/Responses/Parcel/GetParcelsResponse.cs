﻿using System;
using System.Collections.Generic;

namespace POPStation.Core.iOS
{
	public class GetParcelsResponse
	{
		public bool Success { get; set; }
		public List<ParcelData> Data { get; set; }
	}
}

