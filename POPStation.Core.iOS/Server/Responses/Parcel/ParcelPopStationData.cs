﻿using System;

namespace POPStation.Core.iOS
{
	public class ParcelPopStationData
	{
		public string Id { get; set; }
		public string LockerNo { get; set; }
		public string CollectionPin { get; set; }
		public string QrImageUrl { get; set; }
		public DateTime ExpiryDate { get; set; }
		public string UUID { get; set; }
		public string Major { get; set; }
		public string Minor { get; set; }
		public int Range { get; set; }

		public int GetMajor()
		{
			if( !string.IsNullOrEmpty(Major) )
			{
				return Convert.ToInt16(Major, 16);
			}
			return -1;
		}

		public int GetMinor()
		{
			if( !string.IsNullOrEmpty(Minor) )
			{
				return Convert.ToInt16(Minor, 16);
			}
			return -1;
		}
	}
}

