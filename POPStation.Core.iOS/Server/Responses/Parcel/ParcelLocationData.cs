﻿using System;

namespace POPStation.Core.iOS
{
	public class ParcelLocationData
	{
		public string Name { get; set; }
		public string Address { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
		public string OperationHours { get; set; }
		public string PostCode { get; set; }
	}
}

