﻿using System;

namespace POPStation.Core.iOS
{
	public enum ParcelStatus
	{
		PendingShipment,
		InProcessing,
		WithDeliveryCourier,
		Transit,
		DirectedToPostOffice,
		FailedPopstaionDelivery,
		DeliveredToPopStation,
		DeliveredToPostOffice,
		Collected,
		FinalDelivery,
		UnsuccessfulDelivery,
		Unknown
	}

	public class ParcelData
	{
		public string TrackingNo { get; set; }
		private string m_Status;
		public string Status { 
			get {
				return m_Status;
			}
			set {
				if(m_Status != value)
				{
					m_Status = value;
					ParcelStatus ps;
					if(!ParcelStatus.TryParse(m_Status, out ps))
					{
						if (m_Status == null) {
							Console.WriteLine ("Error in received Parcel Status : Status was null.");
						} else {
							Console.WriteLine ("Error in received status : " + m_Status);
						}
						ParcelStatus = ParcelStatus.Unknown;
					}
					else 
					{
						ParcelStatus = ps;
					}
				}
			}
		}
		public string StatusString { get; set; }
		public ParcelLocationData Location { get; set; }
		public ParcelPopStationData PopStation { get; set; }
		public string Merchant { get; set; }
		public bool IsInPopStation { get; set; }
		public ParcelStatus ParcelStatus { get; set; }
		public bool IsPaymentRequired { get; set; }

		public string GetIconFileNameForStatus()
		{
			switch (ParcelStatus) {
				case ParcelStatus.PendingShipment:
				case ParcelStatus.InProcessing:
					return "pending.png";
				case ParcelStatus.WithDeliveryCourier:
				case ParcelStatus.Transit:
					return "courier.png";
				case ParcelStatus.DirectedToPostOffice:
				case ParcelStatus.FailedPopstaionDelivery:
					return "redirected.png";
				case ParcelStatus.DeliveredToPopStation:
				case ParcelStatus.DeliveredToPostOffice:
					return "delivered.png";
				case ParcelStatus.Collected:
				case ParcelStatus.FinalDelivery:
					return "collected.png";
				case ParcelStatus.UnsuccessfulDelivery:
				case ParcelStatus.Unknown: // Adding this for is there is an error in data from the server.
					return "failed.png";
				default:
					Console.WriteLine ("Unrecognized status. Returning failed.png");
					return "failed.png";
			}
		}

	}
}

