﻿using System;
using System.Collections.Generic;

namespace POPStation.Core.iOS
{
	public class EMerchantBannerData
	{
		public string BannerKey { get; set; }
		public string BannerCategory { get; set; }
		public string Description { get; set; }
		public string Url { get; set; }
		public bool HasUrl { get; set; }
		public bool OpenInNewWindow { get; set; }
		public int SortOrder { get; set; }
		public string ImageUrl { get; set; }
	}

	public class EMerchantBannerResponse
	{
		public bool Success { get; set; }
		public List<EMerchantBannerData> Data { get; set; }
	}
}
