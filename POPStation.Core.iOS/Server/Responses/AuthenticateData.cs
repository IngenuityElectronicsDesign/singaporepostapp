﻿using System;

namespace POPStation.Core.iOS
{
	public class AuthenticateData
	{
		public string TokenId { get; set; }
	}

	public class AuthenticateResponse
	{
		public bool Success { get; set; }
		public AuthenticateData Data { get; set; }
	}
}
