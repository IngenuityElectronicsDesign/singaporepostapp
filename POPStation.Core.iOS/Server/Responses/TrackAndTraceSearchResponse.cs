﻿using System;

namespace POPStation.Core.iOS
{
	public class TrackAndTraceSearchResponse
	{
		public bool Success { get; set; }
		public TrackAndTraceSearchData Data { get; set; }
		public string Error { get; set; }
	}
}

