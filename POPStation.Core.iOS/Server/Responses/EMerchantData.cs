﻿using System;
using System.Collections.Generic;

namespace POPStation.Core.iOS
{
	public class EMerchantData
	{
		public string Name { get; set; }
		public string EMerchantKey { get; set; }
		public string Description { get; set; }
		public string Url { get; set; }
		public string ImageUrl { get; set; }
		public string LargeImageUrl { get; set; }
		public int SortOrder { get; set; }
	}

	public class EMerchantResponse
	{
		public bool Success { get; set; }
		public List<EMerchantData> Data { get; set; }
	}
}
