﻿using System;

namespace POPStation.Core.iOS
{
	public class TrackingDetailData
	{
		public DateTime Date { get; set; }
		public string Location { get; set; }
		public string Status { get; set; }
		public string StatusString { get; set; }
		public string Descriptions { get; set; }
	}
}

