﻿using System;

namespace POPStation.Core.iOS
{
	public class SearchAddressData
	{
		public string PostalCode { get; set; }
		public string BuildingName { get; set; }
		public string BuildingNo { get; set; }
		public string StreetName { get; set; }
		public bool IsPostOffice { get; set; }
	}

	public class SearchAddressResponse
	{
		public bool Success { get; set; }
		public SearchAddressData Address { get; set; }
	}
}
