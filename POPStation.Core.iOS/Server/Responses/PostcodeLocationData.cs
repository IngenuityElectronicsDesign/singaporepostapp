﻿using System;

namespace POPStation.Core.iOS
{
	public class PostcodeLocationData
	{
		public string PostCode { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
	}
}

