﻿using System;

namespace POPStation.Core.iOS
{
	public class UserProfileData
	{
		public string Email { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string ContactNumber { get; set; }
		public string BuildingName { get; set; }
		public string StreetName { get; set; }
		public string UnitNumber { get; set; }
		public string PostCode { get; set; }
		public bool IsFirstTime { get; set; }

		public UserProfileData() { }

		public UserProfileData(UserProfileData otherData)
		{
			this.Email = otherData.Email;
			this.FirstName = otherData.FirstName;
			this.LastName = otherData.LastName;
			this.ContactNumber = otherData.ContactNumber;
			this.BuildingName = otherData.BuildingName;
			this.StreetName = otherData.StreetName;
			this.UnitNumber = otherData.UnitNumber;
			this.PostCode = otherData.PostCode;
			this.IsFirstTime = otherData.IsFirstTime;
		}

		public bool ValidateData()
		{
			if (IsValidString (Email))
				return false;
			if (IsValidString (FirstName))
				return false;
			if (IsValidString (LastName))
				return false;
			if (IsValidString (ContactNumber))
				return false;
			if (IsValidString (BuildingName))
				return false;
			if (IsValidString (StreetName))
				return false;
			if (IsValidString (UnitNumber))
				return false;
			if (IsValidString (PostCode))
				return false;

			return true;
		}

		private bool IsValidString(string str)
		{
			return string.IsNullOrEmpty (str) || string.IsNullOrWhiteSpace (str);
		}

		public override string ToString ()
		{
			return string.Format ("[UserProfileData: Email={0}, FirstName={1}, LastName={2}, ContactNumber={3}, BuildingName={4}, StreetName={5}, UnitNumber={6}, PostCode={7}, IsFirstTime={8}]", Email, FirstName, LastName, ContactNumber, BuildingName, StreetName, UnitNumber, PostCode, IsFirstTime);
		}
	}

	public class UserProfileResponse
	{
		public bool Success { get; set; }
		public UserProfileData Data { get; set; }
	}
}

