﻿using System;

namespace POPStation.Core.iOS
{
	public class SQLError
	{
		public string code { get; set; }
		public int errno { get; set; }
		public string sqlState { get; set; }
		public int index { get; set; }

		public SQLError ()
		{
		}
	}
}
