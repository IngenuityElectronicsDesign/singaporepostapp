﻿using System;
using System.Collections.Generic;
using System.Json;
using RestSharp;
using RestSharp.Deserializers;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net;

namespace POPStation.Core.iOS
{
	public class SPServerData
	{
		private const string _appjson = "application/json";
		private const string _deviceType = "IOS";

		private enum Clients
		{
			Api,
			SecurePlugins,
			Plugins,

			Count
		}

		#region Properties
		public List<RestClient> m_Clients;

		public readonly RestClient m_ApiClient;
		public readonly RestClient m_SecturePluginClient;
		public readonly RestClient m_PluginsClient;

		private AuthToken m_Token;

		private JsonDeserializer m_JsonDeserializer;

		private Object WriteCookieLock = new Object();
		private Object WriteTokenLock = new Object();
		#endregion Properties

		#region Singleton Instance
		private static SPServerData m_Instance;
		public static SPServerData Instance
		{
			get
			{
				if( m_Instance == null )
				{
					m_Instance = new SPServerData();
				}
				return m_Instance;
			}
		}
		#endregion Singeton Instance

		private SPServerData()
		{
			m_Clients = new List<RestClient>();

			m_ApiClient = new RestClient(SPSharedData.PopstationURLMain + "SingPostApi/");
			m_SecturePluginClient = new RestClient(SPSharedData.PopstationURLSecurePlugins);
			m_PluginsClient = new RestClient(SPSharedData.PopstationURLPlugins);

			m_Clients.Add(m_ApiClient);
			m_Clients.Add(m_SecturePluginClient);
			m_Clients.Add(m_PluginsClient);

			LoadCookieIntoClient();
			LoadTokenFromDisk();

			m_JsonDeserializer = new JsonDeserializer();
		}

		private void ExecuteAsync(IRestRequest request, Clients client, Action<IRestResponse> action)
		{
			m_Clients[(int)client].ExecuteAsync(request, (response) =>
				{
					WriteCookieToDisk();
					action(response);
				});
		}

		#region Cookie
		private void WriteCookieToDisk()
		{
			lock( WriteCookieLock )
			{
				if( File.Exists(GetCookiePath()) )
				{
					File.Delete(GetCookiePath());
				}
				using( Stream stream = File.Create(GetCookiePath()) )
				{
					BinaryFormatter formatter = new BinaryFormatter();
					formatter.Serialize(stream, m_ApiClient.CookieContainer);
				}
			}
		}

		private void LoadCookieIntoClient()
		{
			lock( WriteCookieLock )
			{
				try
				{
					using( Stream stream = File.Open(GetCookiePath(), FileMode.Open) )
					{
						BinaryFormatter formatter = new BinaryFormatter();
						var cookieContainer = (CookieContainer)formatter.Deserialize(stream);
						m_ApiClient.CookieContainer = cookieContainer;
					}
				}
				catch(Exception e)
				{
					// Problem reading cookie from disk.  Returning new one
					Console.WriteLine("Problem reading cookie from disk : " + e.GetType());
					m_ApiClient.CookieContainer = new CookieContainer();
				}
			}
		}

		private string GetCookiePath()
		{
			var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			var cookieFile = Path.Combine(documents, "Cookie.bin");
			return cookieFile;
		}
		#endregion Cookie

		#region Token
		private void WriteTokenToDisk()
		{
			lock( WriteTokenLock )
			{
				if( File.Exists(GetTokenPath()) )
				{
					File.Delete(GetTokenPath());
				}
				using( Stream stream = File.Create(GetTokenPath()) )
				{
					BinaryFormatter formatter = new BinaryFormatter();
					formatter.Serialize(stream, m_Token.EncodedToken);
				}
			}
		}

		private void RemoveTokenFromDisk()
		{
			lock( WriteTokenLock )
			{
				if( File.Exists(GetTokenPath()) )
				{
					File.Delete(GetTokenPath());
				}
			}
		}

		private void LoadTokenFromDisk()
		{
			lock( WriteTokenLock )
			{
				try
				{
					using( Stream stream = File.Open(GetTokenPath(), FileMode.Open) )
					{
						BinaryFormatter formatter = new BinaryFormatter();
						var tokenContainer = (string)formatter.Deserialize(stream);
						m_Token = new AuthToken(tokenContainer);
					}
				}
				catch(Exception e)
				{
					// Problem reading token from disk. Leave blank
					Console.WriteLine("No token found on disk : " + e.GetType());
				}
			}
		}

		private string GetTokenPath()
		{
			var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			var tokenFile = Path.Combine(documents, "Token.bin");
			return tokenFile;
		}
		#endregion Token

		#region Server Commands
		// Error Check Command
		private bool RESTResponseServerErrorCheck( IRestResponse response ) 
		{
			ServerError errorData;

			try
			{
				errorData = m_JsonDeserializer.Deserialize<ServerError>(response);

				// We have an error
				if( errorData.err != null && errorData.err.code != null )
				{
					Console.Error.WriteLine("ServerError: " + errorData.err.code);
					return true;
				}

			}
			catch(Exception e)
			{
				// This means we could not deserialize into a Server Error object, but there could still be an error of some kind, just not a server error
				Console.WriteLine("Exception when getting ServerError: " + e.Message + " : " + response.Content);
			}
			return false;
		}

		// Authenticate Command
		public void ServerAuthenticate(string email, string password, Action<bool>callback)
		{
//			if( m_Token != null && !string.IsNullOrEmpty(m_Token.EncodedToken) )
//			{
//				// Already authenticated, perform action ... TODO: Confirm token is valid
//				if( callback != null )
//				{
//					callback(true);
//				}
//			}
//			else
			{
				RESTServerAuthenticate(email,password,callback);
			}
		}

		private void RESTServerAuthenticate(string email, string password, Action<bool>callback)
		{
			Console.WriteLine ("Server Authenticate: Attempt\nemail:" + email + " password:" + password);

			var request = new RestRequest("MobileAuthenticate", Method.POST);

			request.AddParameter("email", email);
			request.AddParameter("password", password);

			request.AddHeader("Accept", _appjson);

			ExecuteAsync(request, Clients.Api, (response) =>
				{
					Console.WriteLine ("Server Authenticate: Response Received");

					if( RESTResponseServerErrorCheck(response) )
					{
						return;
					}

					bool success;
					AuthenticateResponse resp;

					try
					{
						resp = m_JsonDeserializer.Deserialize<AuthenticateResponse>(response);
					}
					catch(Exception e)
					{
						Console.WriteLine("Exception when deserialising response data: " + e.Message + " : " + response.Content);
						if( callback != null )
						{
							callback(false);
						}
						return;
					}

					if( !resp.Success )
					{
						Console.WriteLine("Server Authenticate: Error\n403" );
						success = false;
					}
					else
					{
						if( string.IsNullOrEmpty(resp.Data.TokenId) )
						{
							Console.WriteLine("Server Authenticate: Error\nTokenID empty" );
							success = false;
						}
						else
						{
							// Successfully logged in, write token ID
							m_Token = new AuthToken(email, resp.Data.TokenId);
							WriteTokenToDisk();

							Console.WriteLine("Server Authenticate: Success\nTokenID: " + m_Token.EncodedToken);

							success = true;
						}
					}

					if( callback != null )
					{
						callback(success);
					}
				});
		}

		// Get Profile Command
		public void ServerGetProfile(Action<bool, bool>callback)
		{
			if( m_Token == null || string.IsNullOrEmpty(m_Token.EncodedToken) )
			{
				Console.WriteLine("Server Get Profile: Error\nEmpty Token" );

				if( callback != null )
				{
					callback(false, true);
				}
			}
			else
			{
				RESTServerGetProfile(callback);
			}
		}

		private void RESTServerGetProfile(Action<bool, bool>callback)
		{
			Console.WriteLine ("Server Get Profile: Attempt");

			var request = new RestRequest("GetProfile", Method.GET);

			request.AddHeader("Authorization", m_Token.EncodedToken);
			request.AddHeader("Accept", _appjson);

			ExecuteAsync(request, Clients.Api, (response) =>
				{
					Console.WriteLine ("Server Get Profile: Response Received");

					if( RESTResponseServerErrorCheck(response) )
					{
						return;
					}

					bool success;
					bool isFirstTime = true;;
					UserProfileResponse resp;

					try
					{
						resp = m_JsonDeserializer.Deserialize<UserProfileResponse>(response);
					}
					catch(Exception e)
					{
						Console.WriteLine("Exception when deserialising response data: " + e.Message + " : " + response.Content);
						if( callback != null )
						{
							callback(false, true);
						}
						return;
					}

					if( !resp.Success )
					{
						Console.WriteLine("Server Get Profile: Error\n403" );
						success = false;
					}
					else
					{
						if( string.IsNullOrEmpty(resp.Data.Email) )
						{
							Console.WriteLine("Server Get Profile: Error\nEmail empty" );
							success = false;
						}
						else
						{
							Console.WriteLine("Server Get Profile: Success\nEmail: " + resp.Data.Email + "\nFirst Time: " + resp.Data.IsFirstTime);
							Console.WriteLine("Profile Data : " + resp.Data.ToString());
							SPSharedData.Instance.UserProfile = resp.Data;

							isFirstTime = resp.Data.IsFirstTime;
							success = true;
						}
					}

					if( callback != null )
					{
						callback(success, isFirstTime);
					}
				});
		}

		public void ServerPushProfile(UserProfileData profileData, Action<bool, string>callback)
		{
			if( m_Token == null || string.IsNullOrEmpty(m_Token.EncodedToken) )
			{
				Console.WriteLine("Server Push Profile: Error\nEmpty Token" );

				if( callback != null )
				{
					callback(false, "Token is not valid");
				}
			}
			else
			{
				RESTServerPushProfile(profileData.FirstName, profileData.LastName, profileData.ContactNumber, profileData.BuildingName, profileData.StreetName, profileData.UnitNumber, profileData.PostCode, callback);
			}
		}

		// Push Profile Command
		public void ServerPushProfile(string firstName, string lastName, string contactNumber, string buildingName, string streetName, string unitNumber, string postCode, Action<bool, string>callback)
		{
			if( m_Token == null || string.IsNullOrEmpty(m_Token.EncodedToken) )
			{
				Console.WriteLine("Server Push Profile: Error\nEmpty Token" );

				if( callback != null )
				{
					callback(false, "Token is not valid");
				}
			}
			else
			{
				RESTServerPushProfile(firstName, lastName, contactNumber, buildingName, streetName, unitNumber, postCode, callback);
			}
		}

		private void RESTServerPushProfile(string firstName, string lastName, string contactNumber, string buildingName, string streetName, string unitNumber, string postCode, Action<bool, string>callback)
		{
			Console.WriteLine ("Server Push Profile: Attempt");

			var request = new RestRequest("PushProfile", Method.POST);

			request.AddParameter("firstName", firstName);
			request.AddParameter("lastName", lastName);
			request.AddParameter("contactNumber", contactNumber);
			request.AddParameter("buildingName", buildingName);
			request.AddParameter("streetName", streetName);
			request.AddParameter("unitNumber", unitNumber);
			request.AddParameter("postCode", postCode);

			request.AddHeader("Authorization", m_Token.EncodedToken);
			request.AddHeader("Accept", _appjson);

			ExecuteAsync(request, Clients.Api, (response) =>
				{
					Console.WriteLine ("Server Push Profile: Response Received");

					if( RESTResponseServerErrorCheck(response) )
					{
						return;
					}

					bool success;
					string responseMessage = "";
					PushStringResponse resp;

					try
					{
						resp = m_JsonDeserializer.Deserialize<PushStringResponse>(response);
					}
					catch(Exception e)
					{
						Console.WriteLine("Exception when deserialising response data: " + e.Message + " : " + response.Content);
						if( callback != null )
						{
							callback(false, "");
						}
						return;
					}

					if( resp == null )
					{
						Console.WriteLine("Server Push Profile: Error" );
						success = false;
					}
					else
					{
						Console.WriteLine("Server Push Profile: Success");

						responseMessage = resp.Data;

						success = true;
					}

					if( callback != null )
					{
						callback(success, responseMessage);
					}
				});
		}

		// Request Lost Password
		public void ServerRequestLostPassword(string email, Action<bool>callback)
		{
			RESTServerRequestLostPassword(email, callback);
		}

		private void RESTServerRequestLostPassword(string email, Action<bool>callback)
		{
			Console.WriteLine ("Server Request Lost Password: Attempt\nemail:" + email);

			var request = new RestRequest("MobileRequestLostPassword", Method.POST);

			request.AddParameter("email", email);

			request.AddHeader("Accept", _appjson);

			ExecuteAsync(request, Clients.Api, (response) =>
				{
					Console.WriteLine ("Server Request Lost Password: Response Received");

					if( RESTResponseServerErrorCheck(response) )
					{
						return;
					}

					bool success;
					PushStringResponse resp;

					try
					{
						resp = m_JsonDeserializer.Deserialize<PushStringResponse>(response);
					}
					catch(Exception e)
					{
						Console.WriteLine("Exception when deserialising response data: " + e.Message + " : " + response.Content);
						if( callback != null )
						{
							callback(false);
						}
						return;
					}

					if( !resp.Success )
					{
						Console.WriteLine("Server Request Lost Password: Error\n403" );
						success = false;
					}
					else
					{
						success = true;
					}

					if( callback != null )
					{
						callback(success);
					}
				});
		}

		// Push First Time User Profile Command
		public void ServerPushFirstTimeUserProfile(string buildingName, string streetName, string unitNumber, string postCode, string oldPassword, string newPassword, string confirmPassword, Action<bool, string>callback)
		{
			if( m_Token == null || string.IsNullOrEmpty(m_Token.EncodedToken) )
			{
				Console.WriteLine("Server Push First Time User Profile: Error\nEmpty Token" );

				if( callback != null )
				{
					callback(false, "Token is not valid");
				}
			}
			else
			{
				RESTServerPushFirstTimeUserProfile(buildingName, streetName, unitNumber, postCode, oldPassword, newPassword, confirmPassword, callback);
			}
		}

		private void RESTServerPushFirstTimeUserProfile(string buildingName, string streetName, string unitNumber, string postCode, string oldPassword, string newPassword, string confirmPassword, Action<bool, string>callback)
		{
			Console.WriteLine ("Server Push First Time User Profile: Attempt");

			var request = new RestRequest("PushFirstTimeUserProfile", Method.POST);

			request.AddParameter("buildingName", buildingName);
			request.AddParameter("streetName", streetName);
			request.AddParameter("unitNumber", unitNumber);
			request.AddParameter("postCode", postCode);
			request.AddParameter("oldPassword", oldPassword);
			request.AddParameter("newPassword", newPassword);
			request.AddParameter("confirmPassword", confirmPassword);

			request.AddHeader("Authorization", m_Token.EncodedToken);
			request.AddHeader("Accept", _appjson);

			ExecuteAsync(request, Clients.Api, (response) =>
				{
					Console.WriteLine ("Server Push First Time User Profile: Response Received");

					if( RESTResponseServerErrorCheck(response) )
					{
						return;
					}

					bool success;
					string responseMessage = "";
					AuthenticateResponse authResp = null;
					PushStringResponse stringResp = null;

					try
					{
						authResp = m_JsonDeserializer.Deserialize<AuthenticateResponse>(response);
					}
					catch(Exception e)
					{
						try
						{
							stringResp = m_JsonDeserializer.Deserialize<PushStringResponse>(response);
						}
						catch
						{
							Console.WriteLine("Exception when deserialising response data: " + e.Message + " : " + response.Content);
							if( callback != null )
							{
								callback(false, "");
							}
							return;
						}
					}

					if( authResp == null && stringResp == null )
					{
						Console.WriteLine("Server Push First Time User Profile: Error" );
						success = false;
					}
					else
					{
						if( stringResp != null && !string.IsNullOrEmpty(stringResp.Data) )
						{
							Console.WriteLine("Server Push First Time User Profile: Error" );
							responseMessage = stringResp.Data;
							success = false;
						}
						else if( authResp != null && string.IsNullOrEmpty(authResp.Data.TokenId))
						{
							Console.WriteLine("Server Push First Time User Profile: Error\nTokenId blank" );
							responseMessage = "TokenId blank";
							success = false;
						}
						else
						{
							// Successfully logged in, write token ID
							m_Token = new AuthToken(SPSharedData.Instance.UserProfile.Email, authResp.Data.TokenId);
							WriteTokenToDisk();

							Console.WriteLine("Server Push First Time User Profile: Success\nTokenID: " + m_Token.EncodedToken);

							success = true;
						}
					}

					if( callback != null )
					{
						callback(success, responseMessage);
					}
				});
		}

		// Reset Password Command
		public void ServerResetPassword(string oldPassword, string newPassword, string confirmPassword, Action<bool, string>callback)
		{
			if( m_Token == null || string.IsNullOrEmpty(m_Token.EncodedToken) )
			{
				Console.WriteLine("Server Reset Password: Error\nEmpty Token" );

				if( callback != null )
				{
					callback(false, "Token is not valid");
				}
			}
			else
			{
				RESTServerResetPassword(oldPassword, newPassword, confirmPassword, callback);
			}
		}

		private void RESTServerResetPassword(string oldPassword, string newPassword, string confirmPassword, Action<bool, string>callback)
		{
			Console.WriteLine ("Server Reset Password: Attempt");

			var request = new RestRequest("MobileResetPassword", Method.POST);

			request.AddParameter("oldPassword", oldPassword);
			request.AddParameter("newPassword", newPassword);
			request.AddParameter("confirmPassword", confirmPassword);

			request.AddHeader("Authorization", m_Token.EncodedToken);
			request.AddHeader("Accept", _appjson);

			ExecuteAsync(request, Clients.Api, (response) =>
				{
					Console.WriteLine ("Server Reset Password: Response Received");

					if( RESTResponseServerErrorCheck(response) )
					{
						return;
					}

					bool success;
					string responseMessage = "";
					AuthenticateResponse authResp = null;
					PushStringResponse stringResp = null;

					try
					{
						authResp = m_JsonDeserializer.Deserialize<AuthenticateResponse>(response);
					}
					catch(Exception e)
					{
						try
						{
							stringResp = m_JsonDeserializer.Deserialize<PushStringResponse>(response);
						}
						catch
						{
							Console.WriteLine("Exception when deserialising response data: " + e.Message + " : " + response.Content);
							if( callback != null )
							{
								callback(false, "");
							}
							return;
						}
					}

					if( authResp == null && stringResp == null )
					{
						Console.WriteLine("Server Reset Password: Error" );
						success = false;
					}
					else
					{
						if( stringResp != null && !string.IsNullOrEmpty(stringResp.Data) )
						{
							Console.WriteLine("Server Reset Password: Error" );
							responseMessage = stringResp.Data;
							success = false;
						}
						else if( authResp != null && string.IsNullOrEmpty(authResp.Data.TokenId))
						{
							Console.WriteLine("Server Reset Password: Error\nTokenId blank" );
							responseMessage = "TokenId blank";
							success = false;
						}
						else
						{
							// Successfully logged in, write token ID
							m_Token = new AuthToken(SPSharedData.Instance.UserProfile.Email, authResp.Data.TokenId);
							WriteTokenToDisk();

							Console.WriteLine("Server Reset Password: Success\nTokenID: " + m_Token.EncodedToken);

							success = true;
						}
					}

					if( callback != null )
					{
						callback(success, responseMessage);
					}
				});
		}

		public void ServerLogout(Action<bool>callback)
		{
			if( m_Token != null && !string.IsNullOrEmpty(m_Token.EncodedToken) )
			{
				Console.WriteLine("Server Logout: Success" );

				RemoveTokenFromDisk();
				m_Token = null;

				if( callback != null )
				{
					callback(true);
				}
			}
			else
			{
				Console.WriteLine("Server Logout: Failed\nEncoded Token null or blank" );

				if( callback != null )
				{
					callback(false);
				}
			}
		}

		// Search Location Command
		public void ServerSearchLocation(string postCode, int take, Action<bool, List<SearchLocationResponse>>callback)
		{
			RESTServerSearchLocation(postCode, take, callback);
		}

		private void RESTServerSearchLocation(string postCode, int take, Action<bool, List<SearchLocationResponse>>callback)
		{
			Console.WriteLine ("Server Search Location: Attempt");

			var request = new RestRequest("kiosklocation", Method.GET);

			if( !string.IsNullOrEmpty(postCode) )
			{
				request.AddParameter("postCode", postCode);

				if( take <= 0 )
				{
					take = 5;
				}

				request.AddParameter("take", take);
			}

			request.AddHeader("Accept", _appjson);

			ExecuteAsync(request, Clients.SecurePlugins, (response) =>
				{
					Console.WriteLine ("Server Search Location: Response Received");

					bool success;
					List<SearchLocationResponse> resp;

					try
					{
						resp = m_JsonDeserializer.Deserialize<List<SearchLocationResponse>>(response);
					}
					catch(Exception e)
					{
						Console.WriteLine("Exception when deserialising response data: " + e.Message + " : " + response.Content);
						if( callback != null )
						{
							callback(false, null);
						}
						return;
					}

					if( resp == null || resp.Count <= 0 )
					{
						Console.WriteLine("Server Search Location: Error" );
						success = false;
					}
					else
					{
						if( string.IsNullOrEmpty(resp[0].KioskId) )
						{
							Console.WriteLine("Server Search Location: Error\nKioskId empty" );
							success = false;
						}
						else
						{
							Console.WriteLine("Server Search Location: Success\nCount: " + resp.Count);

							success = true;
						}
					}

					if( callback != null )
					{
						callback(success, resp);
					}
				});
		}

		// Get EMerchants Command
		public void ServerGetEMerchants(Action<bool>callback)
		{
			RESTServerGetEMerchants(callback);
		}

		private void RESTServerGetEMerchants(Action<bool>callback)
		{
			Console.WriteLine ("Server Get EMerchant: Attempt");

			var request = new RestRequest("GetEmerchants", Method.GET);

			request.AddHeader("Accept", _appjson);

			ExecuteAsync(request, Clients.Api, (response) =>
				{
					Console.WriteLine ("Server Get EMerchant: Response Received");

					bool success;
					EMerchantResponse resp;

					try
					{
						resp = m_JsonDeserializer.Deserialize<EMerchantResponse>(response);
					}
					catch(Exception e)
					{
						Console.WriteLine("Exception when deserialising response data: " + e.Message + " : " + response.Content);
						if( callback != null )
						{
							callback(false);
						}
						return;
					}

					if( resp == null || resp.Data == null || resp.Data[0] == null )
					{
						Console.WriteLine("Server Get EMerchant: Error" );
						success = false;
					}
					else
					{
						if( string.IsNullOrEmpty(resp.Data[0].Name ) )
						{
							Console.WriteLine("Server Get EMerchant: Error\nName empty" );
							success = false;
						}
						else
						{
							Console.WriteLine("Server Get EMerchant: Success\nCount: " + resp.Data.Count + " First Name: " + resp.Data[0].Name);

							SPSharedData.Instance.EMerchantList = resp.Data;

							success = true;
						}
					}

					if( callback != null )
					{
						callback(success);
					}
				});
		}

		// Get EMerchant Banners Command
		public void ServerGetBanners(string category, Action<bool>callback)
		{
			RESTServerGetBanners(category, callback);
		}

		private void RESTServerGetBanners(string category, Action<bool>callback)
		{
			Console.WriteLine ("Server Get Banner: Attempt");

			var request = new RestRequest("GetBanners", Method.GET);

			request.AddParameter("category", category);

			request.AddHeader("Accept", _appjson);

			ExecuteAsync(request, Clients.Api, (response) =>
				{
					Console.WriteLine ("Server Get Banner: Response Received");

					bool success;
					EMerchantBannerResponse resp;

					try
					{
						resp = m_JsonDeserializer.Deserialize<EMerchantBannerResponse>(response);
					}
					catch(Exception e)
					{
						Console.WriteLine("Exception when deserialising response data: " + e.Message + " : " + response.Content);
						if( callback != null )
						{
							callback(false);
						}
						return;
					}

					if( resp == null || resp.Data == null || resp.Data[0] == null )
					{
						Console.WriteLine("Server Get Banner: Error" );
						success = false;
					}
					else
					{
//						if( string.IsNullOrEmpty(resp.Data[0].BannerKey ) )
//						{
//							Console.WriteLine("Server Get EMerchant Banner: Error\nBanner Key empty" );
//							success = false;
//						}
//						else
						{
							Console.WriteLine("Server Get Banner: Success\nCount: " + resp.Data.Count + " Banner Key: " + resp.Data[0].BannerKey);

							SPSharedData.Instance.EMerchantBannerList = resp.Data;

							success = true;
						}
					}

					if( callback != null )
					{
						callback(success);
					}
				});
		}

		// Push Feedback Command
		public void ServerPushFeedback(string name, string email, string contactNumber, string message, Action<bool, string>callback)
		{
			RESTServerPushFeedback(name, email, contactNumber, message, callback);
		}

		private void RESTServerPushFeedback(string name, string email, string contactNumber, string message, Action<bool, string>callback)
		{
			Console.WriteLine ("Server Push Feedback: Attempt");

			var request = new RestRequest("PushFeedback", Method.POST);

			request.AddParameter("name", name);
			request.AddParameter("email", email);
			request.AddParameter("contactNumber", contactNumber);
			request.AddParameter("message", message);

			request.AddHeader("Accept", _appjson);

			ExecuteAsync(request, Clients.Api, (response) =>
				{
					Console.WriteLine ("Server Push Feedback: Response Received");

					bool success;
					string responseMessage = "";
					PushStringResponse resp;

					try
					{
						resp = m_JsonDeserializer.Deserialize<PushStringResponse>(response);
					}
					catch(Exception e)
					{
						Console.WriteLine("Exception when deserialising response data: " + e.Message + " : " + response.Content);
						if( callback != null )
						{
							callback(false, "");
						}
						return;
					}

					if( resp == null )
					{
						Console.WriteLine("Server Push Feedback: Error" );
						success = false;
					}
					else
					{
						Console.WriteLine("Server Push Feedback: Success");

						responseMessage = resp.Data;

						success = true;
					}

					if( callback != null )
					{
						callback(success, responseMessage);
					}
				});
		}
			
		// Search Postcode Command
		public void RESTTrackAndTraceSearch(string trackingNumber, Action<bool, TrackAndTraceSearchResponse> callback)
		{
			var request = new RestRequest ("TrackAndTraceSearch", Method.GET);

			if(!string.IsNullOrEmpty(trackingNumber))
			{
				request.AddParameter ("trackingNo", trackingNumber);
			}

			request.AddHeader("Accept", _appjson);

			ExecuteAsync (request, Clients.Api, (rawResponse) => {
				TrackAndTraceSearchResponse responseData;

				try
				{
					responseData = m_JsonDeserializer.Deserialize<TrackAndTraceSearchResponse>(rawResponse);
				}
				catch (Exception e)
				{
					Console.WriteLine("Exception when deserialising response data: " + e.Message + " : " + rawResponse.Content);
					if(callback != null)
					{
						callback(false, null);
					}
					return;
				}

				if(!responseData.Success)
				{
					Console.WriteLine("Error in Track and Track response data : " + responseData.Error);
					if(callback != null)
					{
						callback(false, responseData);
					}
				}
				else
				{
					if(callback != null)
					{
						callback(true, responseData);
					}
				}
			});
		}

		public void RESTSearchAddress(string postcode, Action<bool, SearchAddressData> callback)
		{
			var request = new RestRequest ("SearchAddress", Method.GET);

			if( !string.IsNullOrEmpty(postcode) )
			{
				request.AddParameter ("postalCode", postcode);
			}

			request.AddHeader("Accept", _appjson);

			ExecuteAsync( request, Clients.Plugins, (response) =>
				{
					SearchAddressResponse resp;

					try
					{
						resp = m_JsonDeserializer.Deserialize<SearchAddressResponse>(response);
					}
					catch (Exception e)
					{
						Console.WriteLine("Error deserializing Postcode Location Data : " + e.Message + " : " + response.Content);
						if(callback != null)
						{
							callback(false, null);
						}
						return;
					}

					if(callback != null)
					{
						callback(true, resp.Address);
					}
				});
		}

		public void RESTGetPostcodeLocation(string postcode, Action<bool, PostcodeLocationData> callback)
		{
			var request = new RestRequest ("PostCode", Method.GET);

			if(!string.IsNullOrEmpty(postcode))
			{
				request.AddParameter ("id", postcode);
			}

			request.AddHeader("Accept", _appjson);

			ExecuteAsync (request, Clients.Plugins, (response) => {
				PostcodeLocationData locationData;

				try
				{
					locationData = m_JsonDeserializer.Deserialize<PostcodeLocationData>(response);
				}
				catch (Exception e)
				{
					Console.WriteLine("Error deserializing Postcode Location Data : " + e.Message + " : " + response.Content);
					if(callback != null)
					{
						callback(false, null);
					}
					return;
				}

				if(callback != null)
				{
					callback(true, locationData);
				}
			});
		}

		public void RESTGetParcelList(Action<bool, ParcelData[]> callback)
		{
			var request = new RestRequest ("GetParcels", Method.GET);

			request.AddHeader("Accept", _appjson);
			request.AddHeader("Authorization", m_Token.EncodedToken);

			ExecuteAsync (request, Clients.Api, (rawResponse) => {
				Console.WriteLine("Parcels : " + rawResponse.Content);
				GetParcelsResponse parcelsResponse;
				try
				{
					parcelsResponse = m_JsonDeserializer.Deserialize<GetParcelsResponse>(rawResponse);
				}
				catch(Exception e)
				{
					Console.WriteLine("Error deserializing Get Parcl List Data : " + e.Message + " : " + rawResponse.Content);
					if(callback != null)
					{
						callback(false, null);
					}
					return;
				}

				if(!parcelsResponse.Success)
				{
					Console.WriteLine("Error retrieving Parcels.");
					if(callback != null)
					{
						callback(false, parcelsResponse.Data.ToArray());
					}
					return;
				}

				if(callback != null)
				{
					callback(true, parcelsResponse.Data.ToArray());
				}
			});
		}

		// Remote Pickup Commands
		public void ServerRemotePickup(string externalId, Action<bool, string, string> callback)
		{
			if( m_Token == null || string.IsNullOrEmpty(m_Token.EncodedToken) )
			{
				Console.WriteLine("Server Remote Pickup: Error\nEmpty Token" );

				if( callback != null )
				{
					callback(false, "Server Error", "Token is not valid");
				}
			}
			else
			{
				RESTServerRemotePickup(externalId, callback);
			}
		}

		private void RESTServerRemotePickup(string externalId, Action<bool, string, string> callback)
		{
			Console.WriteLine ("Server Remote Pickup: Attempt");

			var request = new RestRequest("RemotePickup", Method.POST);

			request.AddHeader("Accept", _appjson);
			request.AddHeader("Authorization", m_Token.EncodedToken);

			request.AddParameter("externalId", externalId);

			ExecuteAsync(request, Clients.Api, (response) =>
				{
					Console.WriteLine ("Server Remote Pickup: Attempt");

					bool success;
					string responseError = "";
					string responseMessage = "";
					RemotePickupResponse resp;

					try
					{
						resp = m_JsonDeserializer.Deserialize<RemotePickupResponse>(response);
					}
					catch(Exception e)
					{
						Console.WriteLine("Exception when deserialising response data: " + e.Message + " : " + response.Content);
						if( callback != null )
						{
							callback(false, "Error Communicating with Server", "Please try again, or contact support if the problem persists.");
						}
						return;
					}

					if( resp == null || !resp.Success )
					{
						Console.WriteLine("Server Remote Pickup: Error" );

						responseError = resp.Error;
						responseMessage = resp.Data;

						success = false;
					}
					else
					{
						Console.WriteLine("Server Remote Pickup: Success");

						responseError = resp.Data;

						success = true;
					}

					if( callback != null )
					{
						callback(success, responseError, responseMessage);
					}
				});
		}

		// Register Push Notification Commands
		public void ServerRegisterPushNotification(string tokenId, Action<bool, string>callback)
		{
			if( m_Token == null || string.IsNullOrEmpty(m_Token.EncodedToken) )
			{
				Console.WriteLine("Server Register Push Notification: Error\nEmpty Token" );

				if( callback != null )
				{
					callback(false, "Token is not valid");
				}
			}
			else
			{
				RESTServerRegisterPushNotification(tokenId, callback);
			}
		}

		private void RESTServerRegisterPushNotification(string tokenId, Action<bool, string>callback)
		{
			Console.WriteLine ("Server Register Push Notification: Attempt");

			SPSharedData.Instance.PushNotificationsTokenID = tokenId;

			var request = new RestRequest("RegisterPushNotification", Method.POST);

			request.AddParameter("tokenId", tokenId);
			request.AddParameter("deviceType", _deviceType);

			request.AddHeader("Authorization", m_Token.EncodedToken);
			request.AddHeader("Accept", _appjson);

			ExecuteAsync(request, Clients.Api, (response) =>
				{
					Console.WriteLine ("Server Register Push Notification: Response Received");

					bool success;
					string responseMessage = "";
					PushStringResponse resp;

					try
					{
						resp = m_JsonDeserializer.Deserialize<PushStringResponse>(response);
					}
					catch(Exception e)
					{
						Console.WriteLine("Exception when deserialising response data: " + e.Message + " : " + response.Content);
						if( callback != null )
						{
							callback(false, "Invalid Server Response");
						}
						return;
					}

					if( resp == null )
					{
						Console.WriteLine("Server Register Push Notification: Error" );

						responseMessage = resp.Data;

						success = false;
					}
					else
					{
						Console.WriteLine("Server Register Push Notification: Success");

						responseMessage = resp.Data;

						success = true;
					}

					if( callback != null )
					{
						callback(success, responseMessage);
					}
				});
		}

		// Deregister Push Notification Commands
		public void ServerDeregisterPushNotification(string tokenId, Action<bool, string>callback)
		{
			RESTServerDeregisterPushNotification(tokenId, callback);
		}

		private void RESTServerDeregisterPushNotification(string tokenId, Action<bool, string>callback)
		{
			Console.WriteLine ("Server Deregister Push Notification: Attempt");

			SPSharedData.Instance.PushNotificationsTokenID = null;

			var request = new RestRequest("DeregisterPushNotification", Method.POST);

			request.AddParameter("tokenId", tokenId);
			request.AddParameter("deviceType", _deviceType);

			request.AddHeader("Accept", _appjson);

			ExecuteAsync(request, Clients.Api, (response) =>
				{
					Console.WriteLine ("Server Deregister Push Notification: Response Received");

					bool success;
					string responseMessage = "";
					PushStringResponse resp;

					try
					{
						resp = m_JsonDeserializer.Deserialize<PushStringResponse>(response);
					}
					catch(Exception e)
					{
						Console.WriteLine("Exception when deserialising response data: " + e.Message + " : " + response.Content);
						if( callback != null )
						{
							callback(false, "Invalid Server Response");
						}
						return;
					}

					if( resp == null )
					{
						Console.WriteLine("Server Deregister Push Notification: Error" );

						responseMessage = resp.Data;

						success = false;
					}
					else
					{
						Console.WriteLine("Server Deregister Push Notification: Success");

						responseMessage = resp.Data;

						success = true;
					}

					if( callback != null )
					{
						callback(success, responseMessage);
					}
				});
		}
		#endregion Server Commands
	}
}