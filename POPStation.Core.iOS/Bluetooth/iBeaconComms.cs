﻿using System;
using System.Drawing;
#if __IOS__
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreBluetooth;
using MonoTouch.CoreLocation;
using MonoTouch.CoreFoundation;
using MonoTouch.AVFoundation;
using MonoTouch.MultipeerConnectivity;

namespace POPStation.Core.iOS
{
	public partial class iBeaconComms
	{
		public Action<CLRegionEventArgs> RegionEnteredAction { get; set; }
		public Action RegionLeftAction { get; set; }
		public Action<int, int> RangeEnteredAction { get; set; }
		public Action RangeLeftAction { get; set; }

		public static readonly string UUID = "E20A39F4-73F5-4BC4-A12F-17D1AD07A961";
		//static readonly ushort major = 0;
		//static readonly ushort minor = 0;
		public static readonly string BeaconID = "SingPOST";

		private float _DefaultRangeValue;
		public float DefaultRangeValue
		{
			get
			{
				return _DefaultRangeValue;
			}
		}
		private float _RangeValue;
		public float RangeValue
		{
			get
			{
				return _RangeValue;
			}
			set
			{
				_RangeValue = Math.Max(1, value);
			}
		}
		private float DeltaValue;

		private bool IsInRange;

		static bool DeviceIsPhone
		{
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}

		CLLocationManager LocationManager;

		#region Singleton Instance
		private static iBeaconComms m_Instance;
		public static iBeaconComms Instance
		{
			get
			{
				if( m_Instance == null )
				{
					m_Instance = new iBeaconComms(4f, 0.25f);
				}
				return m_Instance;
			}
		}
		#endregion Singeton Instance

		public iBeaconComms(float defaultRangeValue, float deltaValue)
		{
			_DefaultRangeValue = defaultRangeValue;
			RangeValue = DefaultRangeValue;
			DeltaValue = deltaValue;

			var beaconUUID = new NSUuid(UUID);
			var beaconRegion = new CLBeaconRegion(beaconUUID, BeaconID);

			beaconRegion.NotifyEntryStateOnDisplay = true;
			beaconRegion.NotifyOnEntry = true;
			beaconRegion.NotifyOnExit = true;

			LocationManager = new CLLocationManager();

			LocationManager.RegionEntered += (sender, e) =>
			{
				if( RegionEnteredAction != null )
				{
					RegionEnteredAction(e);
				}
			};

			LocationManager.RegionLeft += (sender, e) => 
			{
				if( RegionLeftAction != null )
				{
					RegionLeftAction();
				}
				Console.WriteLine("Region Left");
			};

			LocationManager.DidRangeBeacons += (sender, e) =>
			{
				if( e.Beacons.Length > 0 )
				{
					CLBeacon beacon = e.Beacons[0];

					if( IsInRange )
					{
						if( beacon.Accuracy > 0f && beacon.Accuracy < RangeValue + DeltaValue )
						{
							IsInRange = true;
						}
						else
						{
							IsInRange = false;
						}
					}
					else
					{
						if( beacon.Accuracy > 0f && beacon.Accuracy < RangeValue - DeltaValue )
						{
							IsInRange = true;
						}
						else
						{
							IsInRange = false;
						}
					}

					if( IsInRange )
					{
						if( RangeEnteredAction != null )
						{
							RangeEnteredAction(beacon.Major.UInt16Value, beacon.Minor.UInt16Value);
						}
					}
					else
					{
						if( RangeLeftAction != null )
						{
							RangeLeftAction();
						}
					}
				}
				else
				{
					if( RangeLeftAction != null )
					{
						RangeLeftAction();
					}
				}
			};

			if( UIDevice.CurrentDevice.CheckSystemVersion(8,0) )
			{
				LocationManager.RequestAlwaysAuthorization();
			}

			LocationManager.StartMonitoring(beaconRegion);
			LocationManager.StartRangingBeacons(beaconRegion);
		}
	}
}
#endif
