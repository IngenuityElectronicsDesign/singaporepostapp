﻿using POPStation.Core.iOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Rivets;

namespace POPStationiOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		// class-level declarations
		public override UIWindow Window {
			get;
			set;
		}

		public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
		{
			UIApplication.SharedApplication.Delegate.Window.TintColor = SPShared.Instance.RedTintColor;



			return true;
		}

		public override bool OpenUrl(UIApplication application, NSUrl url, string sourceApplication, NSObject annotation)
		{
			var rurl = new Rivets.AppLinkUrl(url.ToString());

			switch( rurl.InputUrl.Host )
			{
				case "myparcel":
				{
					if( SPShared.Instance != null && SPShared.Instance.NavMenuTableViewController != null )
					{
						SPShared.Instance.NavMenuTableViewController.ShouldTransition(0, true, null);
					}
					break;
				}
				case "locations":
				{
					if( SPShared.Instance != null && SPShared.Instance.NavMenuTableViewController != null )
					{
						SPShared.Instance.NavMenuTableViewController.ShouldTransition(1, true, null);
					}
					break;
				}
				case "emerchants":
				{
					if( SPShared.Instance != null && SPShared.Instance.NavMenuTableViewController != null )
					{
						SPShared.Instance.NavMenuTableViewController.ShouldTransition(2, true, null);
					}
					break;
				}
				case "account":
				{
					if( SPShared.Instance != null && SPShared.Instance.NavMenuTableViewController != null && SPShared.Instance.IsLoggedIn )
					{
						SPShared.Instance.NavMenuTableViewController.ShouldTransition(5, true, null);
					}
					break;
				}
				default:
				{
					break;
				}
			}

			return true;

//			var id = string.Empty;
//
//			if (rurl.InputQueryParameters.ContainsKey("id"))
//				id = rurl.InputQueryParameters ["id"];
//
//			if (rurl.InputUrl.Host.Equals ("products") && !string.IsNullOrEmpty (id)) {
//				var c = new ProductViewController (id);
//				navController.PushViewController (c, true);
//				return true;
//			} else {
//				navController.PopToRootViewController (true);
//				return true;
//			}
		}

		// Push Notifications
		public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
		{
			Console.WriteLine(deviceToken.ToString());

			string str = deviceToken.ToString();
			str = Regex.Replace(str, "[^a-zA-Z0-9 \040 -]", "");
			str = Regex.Replace(str, @"\s+", "");

			Console.WriteLine(str);

//			UIAlertView alert = new UIAlertView("Registered For Remote Notifications Successfully", "Device Token: " + str, null, "OK");
//			alert.Show();

			SPServerData.Instance.ServerRegisterPushNotification(str, (success, message) =>
				{
//					InvokeOnMainThread( () =>
//						{
//							string title = success ? "Success" : "Failed";
//
//							UIAlertView alert2 = new UIAlertView("Register Push Notifications " + title, (message != null ? message : ""), null, "OK");
//							alert2.Show();
//						});
				});
		}

		public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
		{
//			UIAlertView alert = new UIAlertView("Failed to Register For Remote Notifications", (error != null ? error.LocalizedDescription : ""), null, "OK");
//			alert.Show();
		}

		// This method is invoked when the application is about to move from active to inactive state.
		// OpenGL applications should use this method to pause.
		public override void OnResignActivation (UIApplication application)
		{
		}
		// This method should be used to release shared resources and it should store the application state.
		// If your application supports background exection this method is called instead of WillTerminate
		// when the user quits.
		public override void DidEnterBackground (UIApplication application)
		{
		}
		// This method is called as part of the transiton from background to active state.
		public override void WillEnterForeground (UIApplication application)
		{
		}
		// This method is called when the application is about to terminate. Save data, if needed.
		public override void WillTerminate (UIApplication application)
		{
		}

		public override void ReceivedLocalNotification(UIApplication application, UILocalNotification notification)
		{
			//new UIAlertView("POPStation", notification.AlertBody, null, "OK", null).Show();
		}

		public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
		{
			if( UIDevice.CurrentDevice.CheckSystemVersion(8,0) )
			{
				if( UIApplication.SharedApplication.ApplicationState == UIApplicationState.Inactive )
				{
					if( SPShared.Instance != null && SPShared.Instance.NavMenuTableViewController != null )
					{
						SPShared.Instance.NavMenuTableViewController.ShouldTransition(0, true, null);
					}
				}
			}
			else
			{
				if( SPShared.Instance != null && SPShared.Instance.NavMenuTableViewController != null )
				{
					SPShared.Instance.NavMenuTableViewController.ShouldTransition(0, true, null);
				}
			}
		}
	}
}
