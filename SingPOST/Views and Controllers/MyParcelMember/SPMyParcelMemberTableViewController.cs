using System;
using System.Drawing;
using System.Collections.Generic;
using System.Timers;
using MonoTouch.Foundation;
using MonoTouch.CoreFoundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using POPStation.Core.iOS;
using System.Linq;

namespace POPStationiOS
{
	public partial class SPMyParcelMemberTableViewController : UITableViewController
	{
		SPMyParcelMemberHeader[] HeaderViewsArray = new SPMyParcelMemberHeader[] { new SPMyParcelMemberHeader("Parcels To Be Collected", true), new SPMyParcelMemberHeader("Collected Parcels", true) };

		List<ParcelData> NotCollectedParcels = new List<ParcelData> ();
		List<ParcelData> CollectedParcels = new List<ParcelData> ();

		public UIBarButtonItem TrackAndTraceButton { get; private set; }
		private SPTrackAndTrackPopoutBar TrackAndTracePopoutBar;

		public SPMyParcelMemberTableViewController(IntPtr handle) : base(handle)
		{
		}

		private int MaxParcelsInSection()
		{ 
			int maxRows = (int)((UIScreen.MainScreen.ApplicationFrame.Height - 44f - 55f) / 50f);

			if(maxRows < 2)
			{
				return 2;
			}
			else
			{
				return maxRows - 2;
			}
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			RefreshControl = new UIRefreshControl();
			RefreshControl.ValueChanged += (sender, e) => 
			{
				if( RefreshControl.Refreshing )
				{
					RefreshControl.BeginRefreshing();
					SPServerData.Instance.RESTGetParcelList(GetParcelsCallback);
				}
			};
				
			SPServerData.Instance.RESTGetParcelList(GetParcelsCallback);

			TableView.RegisterClassForCellReuse( typeof(SPMyParcelMemberCell), new NSString("MyParcelMemberCell") );

			NavigationItem.TitleView = new UIImageView(UIImage.FromFile("Bar/logo-pops.png"));

			// Setup Track and Trace Button
			TrackAndTraceButton = new UIBarButtonItem(UIImage.FromFile("Bar/trackntrace.png"), UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
				{
					if( TrackAndTracePopoutBar.IsVisible )	// Currently Showing
					{
						TrackAndTracePopoutBar.Hide(true, () =>
							{
								((SPNavBar)TrackAndTracePopoutBar.ParentBar).SetBorderLineShouldHide(false, false);
								TrackAndTraceButton.Image = UIImage.FromFile("Bar/trackntrace.png");
								if( NotCollectedParcels.Count > 0 )
								{
									TableView.ScrollToRow( NSIndexPath.FromRowSection(0,0), UITableViewScrollPosition.Top, true );
								}
								TableView.ScrollRectToVisible( new RectangleF( 0f, 0f, 0f, 0f ), true );
							});
					}
					else 							// Current Hidden
					{
						((SPNavBar)TrackAndTracePopoutBar.ParentBar).SetBorderLineShouldHide(true, false);
						TrackAndTracePopoutBar.Show(true, () =>
							{
								TrackAndTraceButton.Image = UIImage.FromFile("Bar/trackntrace-active.png");
							});
					}
				});
			NavigationItem.RightBarButtonItem = TrackAndTraceButton;

			// Setup Track and Trace Popout Bar
			TrackAndTracePopoutBar = new SPTrackAndTrackPopoutBar(NavigationController.NavigationBar, TableView);
		}

		private void GetParcelsCallback(bool success, ParcelData[] parcels)
		{
			if( success && parcels != null && parcels.Length > 0 )
			{
				InvokeOnMainThread( () => 
					{
						UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

						CollectedParcels.Clear();
						NotCollectedParcels.Clear();

						int addIndex = 0;

						for(int i = 0; i < parcels.Length; i++)
						{
							if( parcels[i].ParcelStatus == ParcelStatus.Collected || parcels[i].ParcelStatus == ParcelStatus.FinalDelivery )
							{
								CollectedParcels.Add(parcels[i]);
							}
							else
							{
								if( parcels[i].ParcelStatus == ParcelStatus.DeliveredToPopStation )
								{
									NotCollectedParcels.Insert(addIndex, parcels[i]);
									addIndex++;
								}
								else
								{
									NotCollectedParcels.Add(parcels[i]);
								}
							}
						}

						if( RefreshControl != null && RefreshControl.Refreshing )
						{
							RefreshControl.EndRefreshing();
						}
						TableView.ReloadData();
					});
			}
			else
			{
				UIAlertView alert = new UIAlertView("Error Retrieving Parcels", "Please try again, or contact support if the problem persists.", null, "OK");
				alert.Show();
			}
		}

		bool first = true;

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			if( first )
			{
				TableView.ContentOffset = new PointF(0, TableView.ContentOffset.Y-RefreshControl.Frame.Height);
				RefreshControl.BeginRefreshing();
				first = false;
			}
			else
			{
				foreach( var parcel in NotCollectedParcels.ToList() )
				{
					if( parcel.ParcelStatus == ParcelStatus.Collected || parcel.ParcelStatus == ParcelStatus.FinalDelivery )
					{
						CollectedParcels.Insert(0, parcel);
						NotCollectedParcels.Remove(parcel);
					}
				}
				TableView.ReloadData();
			}

			// Ensure the Search Button Action is reset when we segue back
			TrackAndTracePopoutBar.SearchAction = () =>
			{
				if( string.IsNullOrEmpty(TrackAndTracePopoutBar.SearchField.Text) )
				{
					return;
				}

				if( !TrackAndTracePopoutBar.IsVisible )
				{
					((SPNavBar)TrackAndTracePopoutBar.ParentBar).SetBorderLineShouldHide(true, false);
					TrackAndTracePopoutBar.Show(true, () =>
						{
							TrackAndTraceButton.Image = UIImage.FromFile("Bar/trackntrace-active.png");

							SPTrackAndTraceTableViewController toPush = (SPTrackAndTraceTableViewController)Storyboard.InstantiateViewController("TrackAndTraceTVC");
							toPush.Setup(TrackAndTracePopoutBar);
							NavigationController.PushViewController(toPush, true);
						});
				}
				else
				{
					TrackAndTracePopoutBar.Hide(true, () =>
						{
							((SPNavBar)TrackAndTracePopoutBar.ParentBar).SetBorderLineShouldHide(false, false);
							TrackAndTraceButton.Image = UIImage.FromFile("Bar/trackntrace.png");
						});
					SPTrackAndTraceTableViewController toPush = (SPTrackAndTraceTableViewController)Storyboard.InstantiateViewController("TrackAndTraceTVC");
					toPush.Setup(TrackAndTracePopoutBar);
					NavigationController.PushViewController(toPush, true);
				}
			};
		}

		public override void ViewDidDisappear(bool animated)
		{
			if( RefreshControl != null && RefreshControl.Refreshing )
			{
				RefreshControl.EndRefreshing();
			}

			base.ViewDidDisappear(animated);
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return HeaderViewsArray.Length;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			if( HeaderViewsArray[section].Expanded )
			{
				return Math.Min(MaxParcelsInSection () + 1, ( section == 0 ) ? NotCollectedParcels.Count : CollectedParcels.Count );
			}
			return 0;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			SPMyParcelMemberCell cell = (SPMyParcelMemberCell)TableView.DequeueReusableCell(new NSString("MyParcelMemberCell"), indexPath);

			if( indexPath.Row >= MaxParcelsInSection() )
			{
				cell.Setup("Parcels");
			}
			else
			{
				cell.Setup( ( indexPath.Section == 0 ) ? NotCollectedParcels[indexPath.Row] : CollectedParcels[indexPath.Row] );
			}

			return cell;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			TrackAndTracePopoutBar.Hide(false, () =>
				{
					if( indexPath.Row >= MaxParcelsInSection() )
					{
						SPMyParcelMemberListTableViewController toPush = (SPMyParcelMemberListTableViewController)Storyboard.InstantiateViewController("MyParcelMemberListTVC");
						toPush.Setup(((SPMyParcelMemberCell)tableView.CellAt(indexPath)).SeeAllTitle, new List<ParcelData>((indexPath.Section==0)?NotCollectedParcels:CollectedParcels), indexPath.Section!=0);
						NavigationController.PushViewController(toPush, true);
					}
					else
					{
						SPMyParcelMemberDetailTableViewController toPush = (SPMyParcelMemberDetailTableViewController)Storyboard.InstantiateViewController("MyParcelMemberDetailTVC");
						toPush.Setup((indexPath.Section==0)?NotCollectedParcels[indexPath.Row]:CollectedParcels[indexPath.Row]);
						NavigationController.PushViewController(toPush, true);
					}
				});
		}

		public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return 65f;
		}

		public override float GetHeightForHeader(UITableView tableView, int section)
		{
			return 40f;
		}

		public override UIView GetViewForHeader(UITableView tableView, int section)
		{
			SPMyParcelMemberHeader headerView = HeaderViewsArray[section];
			headerView.ButtonAction = () =>
			{
				tableView.ReloadSections(new NSMutableIndexSet((uint)section), UITableViewRowAnimation.Fade);
			};
			headerView.Count = section == 0 ? NotCollectedParcels.Count : CollectedParcels.Count;
			return headerView;
		}
		#endregion Table View Logic
	}
}
