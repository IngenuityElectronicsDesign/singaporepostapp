﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using POPStation.Core.iOS;

namespace POPStationiOS
{
	public class SPMyParcelMemberCell : UITableViewCell
	{
		public ParcelData ParcelData { get; private set; }
		public string SeeAllTitle { get; private set; }

		private UIImage ActiveBackgroundImage = UIImage.FromFile("Body/item.png").CreateResizableImage(new UIEdgeInsets(20f, 20f, 20f, 20f));
		private UIImage CollectedBackgroundImage = UIImage.FromFile("Body/item-collected.png").CreateResizableImage(new UIEdgeInsets(20f, 20f, 20f, 20f));
		private UIImage SeeAllBackgroundImage = UIImage.FromFile("Body/See-more.png").CreateResizableImage(new UIEdgeInsets(20f, 20f, 20f, 20f));

		private UIImage ActiveArrowImage = UIImage.FromFile("Body/parcellistarrow-default.png");
		private UIImage CollectedArrowImage = UIImage.FromFile("Body/parcellistarrow-collected.png");

		private UIImageView BackgroundImageView;
		private UILabel MerchantNameLabel;
		private UILabel ParcelNumberLabel;
		private UILabel TrackingStatusLabel;
		private UILabel SeeAllTitleLabel;
		private UIImageView ParcelIconView;
		private UIImageView ArrowIconView;

		public SPMyParcelMemberCell(IntPtr handle) : base(handle)
		{
			SelectionStyle = UITableViewCellSelectionStyle.None;
			BackgroundColor = UIColor.Clear;

			BackgroundImageView = new UIImageView(new RectangleF(20f, 5f, 280f, 55f));
			AddSubview(BackgroundImageView);

			MerchantNameLabel = new UILabel(new RectangleF(40f, 2f, 200f, 22f));
			MerchantNameLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
			BackgroundImageView.AddSubview(MerchantNameLabel);

			ParcelNumberLabel = new UILabel(new RectangleF(140f, 2f, 100f, 22f));
			ParcelNumberLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 14f);
			ParcelNumberLabel.TextColor = UIColor.FromWhiteAlpha(0.33f, 1f);
			BackgroundImageView.AddSubview(ParcelNumberLabel);

			TrackingStatusLabel = new UILabel(new RectangleF(40f, 22f, 200f, 32f));
			TrackingStatusLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 12f);
			TrackingStatusLabel.TextColor = UIColor.Black;
			TrackingStatusLabel.Lines = 2;
			//TrackingStatusLabel.AdjustsFontSizeToFitWidth = true;
			//TrackingStatusLabel.MinimumScaleFactor = 0.9f;
			//TrackingStatusLabel.SizeToFit();
			BackgroundImageView.AddSubview(TrackingStatusLabel);

			SeeAllTitleLabel = new UILabel(new RectangleF(20f, 0f, 220f, 55f));
			SeeAllTitleLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
			SeeAllTitleLabel.TextColor = UIColor.White;
			BackgroundImageView.AddSubview(SeeAllTitleLabel);

			ParcelIconView = new UIImageView(new RectangleF(5f, 12f, 30f, 30f));
			BackgroundImageView.AddSubview(ParcelIconView);

			ArrowIconView = new UIImageView(new RectangleF(245f, 12f, 30f, 30f));
			BackgroundImageView.AddSubview(ArrowIconView);
		}

		public void Setup(ParcelData parcelData )
		{
			ParcelData = parcelData;

			bool isCollected = (ParcelData.ParcelStatus == ParcelStatus.Collected || ParcelData.ParcelStatus == ParcelStatus.FinalDelivery);

			MerchantNameLabel.Text = parcelData.Merchant != null ? parcelData.Merchant : "";
			ParcelNumberLabel.Text = "| " + parcelData.TrackingNo != null ? parcelData.TrackingNo : "";
			TrackingStatusLabel.Text = parcelData.StatusString != null ? parcelData.StatusString : "";

			TrackingStatusLabel.Frame = new RectangleF(40f, 22f, 200f, 32f);
			TrackingStatusLabel.SizeToFit();

			MerchantNameLabel.TextColor = (isCollected ? UIColor.FromWhiteAlpha(0.33f, 1f) : SPShared.Instance.BlueTintColor);

			BackgroundImageView.Image = (isCollected ? CollectedBackgroundImage : ActiveBackgroundImage);

			ParcelIconView.Image = UIImage.FromFile( "ParcelList/" + ParcelData.GetIconFileNameForStatus() );
			ArrowIconView.Image = (isCollected ? CollectedArrowImage : ActiveArrowImage);

			SizeF MerchantNameSize = new NSString(MerchantNameLabel.Text).StringSize(MerchantNameLabel.Font, new SizeF(9999f, MerchantNameLabel.Frame.Height), MerchantNameLabel.LineBreakMode);
			float X = (MerchantNameLabel.Frame.X+MerchantNameSize.Width+10f);
			ParcelNumberLabel.Frame = new RectangleF(X, ParcelNumberLabel.Frame.Y, 240f-X, ParcelNumberLabel.Frame.Height);

			ShowSeeAll(false);
		}

		public void Setup(string seeAllTitle)
		{
			SeeAllTitle = seeAllTitle;

			SeeAllTitleLabel.Text = "See all " + SeeAllTitle;

			BackgroundImageView.Image = SeeAllBackgroundImage;
			ArrowIconView.Image = CollectedArrowImage;

			ShowSeeAll(true);
		}

		public void Setup()
		{
			BackgroundImageView.Hidden = true;
		}

		void ShowSeeAll(bool seeAll)
		{
			SeeAllTitleLabel.Hidden = !seeAll;

			ParcelIconView.Hidden = seeAll;
			MerchantNameLabel.Hidden = seeAll;
			ParcelNumberLabel.Hidden = seeAll;
			TrackingStatusLabel.Hidden = seeAll;
		}

		public override void SetSelected(bool selected, bool animated)
		{
			base.SetSelected(false, false);
		}
	}
}

