using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using POPStation.Core.iOS;
using System.Linq;

namespace POPStationiOS
{
	public partial class SPMyParcelMemberListTableViewController : UITableViewController
	{
		List<ParcelData> Parcels;
		bool IsCollectedList;

		int PageIndex;

		SPNextPrevButton PrevButton;
		SPNextPrevButton NextButton;

		UIScreenEdgePanGestureRecognizer BackGesture;

		public SPMyParcelMemberListTableViewController(IntPtr handle) : base(handle)
		{
		}

		public void Setup(string title, List<ParcelData> parcels, bool isCollectedList)
		{
			NavigationItem.Title = title;
			Parcels = parcels;
			IsCollectedList = isCollectedList;
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse( typeof(SPMyParcelMemberCell), new NSString("MyParcelMemberCell") );
			TableView.RegisterClassForCellReuse( typeof(SPMyParcelMemberCell), new NSString("MyParcelMemberBlankCell") );

			// Set the nav items of the new view
			UIBarButtonItem negativeSpacer = new UIBarButtonItem(UIBarButtonSystemItem.FixedSpace);
			negativeSpacer.Width = -12f; // -16 is the left edge

			UIBarButtonItem backButton = new UIBarButtonItem(UIImage.FromFile("Bar/arrowleft.png"), UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
				{
					NavigationController.PopViewControllerAnimated(true);
				});
			NavigationItem.LeftBarButtonItems = new UIBarButtonItem[] {negativeSpacer, backButton};

			PageIndex = 0;
		}

		bool first = true;

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			if( first )
			{
				first = false;
			}
			else if( !IsCollectedList )
			{
				foreach( var parcel in Parcels.ToList() )
				{
					if( parcel.ParcelStatus == ParcelStatus.Collected || parcel.ParcelStatus == ParcelStatus.FinalDelivery )
					{
						Parcels.Remove(parcel);
					}
				}
				TableView.ReloadData();
			}

			BackGesture = new UIScreenEdgePanGestureRecognizer();
			BackGesture.Edges = UIRectEdge.Left;
			BackGesture.AddTarget( () =>
				{
					NavigationController.PopViewControllerAnimated(true);
				});

			View.AddGestureRecognizer(BackGesture);
		}

		public override void ViewWillDisappear(bool animated)
		{
			View.RemoveGestureRecognizer(BackGesture);
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 1;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			return GetNumOfRowsWhichFit();
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			int index = indexPath.Row + (GetNumOfRowsWhichFit() * PageIndex);
			SPMyParcelMemberCell cell;

			if( index < Parcels.Count )
			{
				cell = (SPMyParcelMemberCell)TableView.DequeueReusableCell(new NSString("MyParcelMemberCell"), indexPath);
				cell.Setup(Parcels[index]);
			}
			else
			{
				cell = (SPMyParcelMemberCell)TableView.DequeueReusableCell(new NSString("MyParcelMemberBlankCell"), indexPath);
				cell.Setup();
			}

			return cell;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			int index = indexPath.Row + (GetNumOfRowsWhichFit() * PageIndex);

			if( index < Parcels.Count )
			{
				SPMyParcelMemberDetailTableViewController toPush = (SPMyParcelMemberDetailTableViewController)Storyboard.InstantiateViewController("MyParcelMemberDetailTVC");
				toPush.Setup(Parcels[index]);
				NavigationController.PushViewController(toPush, true);
			}
		}

		public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return 65f;
		}

		public override float GetHeightForHeader(UITableView tableView, int section)
		{
			return 5f;
		}

		public override UIView GetViewForHeader(UITableView tableView, int section)
		{
			return new UIView();
		}

		public override float GetHeightForFooter(UITableView tableView, int section)
		{
			return 40f;
		}

		public override UIView GetViewForFooter(UITableView tableView, int section)
		{
			UIView FooterView = new UIView(new RectangleF(0f, 0f, 320f, 50f));
			FooterView.BackgroundColor = UIColor.GroupTableViewBackgroundColor;

			PrevButton = new SPNextPrevButton(new PointF(20f, 5f), false);
			PrevButton.Button.TouchUpInside += (sender, e) => 
			{
				PageIndex = Math.Max(0, PageIndex-1);
				TableView.ReloadSections(new NSMutableIndexSet(0), UITableViewRowAnimation.Right);
			};
			PrevButton.Button.Enabled = PageIndex > 0;
			FooterView.AddSubview(PrevButton);

			NextButton = new SPNextPrevButton(new PointF(220f, 5f), true);
			NextButton.Button.TouchUpInside += (sender, e) => 
			{
				PageIndex = Math.Min(GetNumOfPages()-1, PageIndex+1);
				TableView.ReloadSections(new NSMutableIndexSet(0), UITableViewRowAnimation.Left);
			};
			NextButton.Button.Enabled = PageIndex < GetNumOfPages()-1;
			FooterView.AddSubview(NextButton);

			UILabel PageLabel = new UILabel(new RectangleF(100f, 5f, 120f, 30f));
			PageLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);
			PageLabel.Text = "Page " + (PageIndex+1).ToString() + "/" + GetNumOfPages().ToString();
			PageLabel.TextColor = UIColor.FromWhiteAlpha(0.33f, 1f);
			PageLabel.TextAlignment = UITextAlignment.Center;
			FooterView.AddSubview(PageLabel);

			return FooterView;
		}
		#endregion Table View Logic

		int GetNumOfRowsWhichFit()
		{
			return (int)((UIScreen.MainScreen.ApplicationFrame.Height - 44f - 40f) / 65f);
		}

		int GetNumOfPages()
		{
			// We always want at least one page, and we always round up with a remainder.
			return Math.Max((int)Math.Ceiling((float)Parcels.Count/GetNumOfRowsWhichFit()), 1);
		}
	}
}
