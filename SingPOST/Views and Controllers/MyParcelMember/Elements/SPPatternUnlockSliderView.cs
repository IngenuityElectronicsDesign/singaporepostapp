﻿using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public class SPPatternUnlockSliderView : UIControl, ISPPatternUnlockNubViewDelegate
	{
		bool Unlocked = false;

		UIImage BackgroundUnlockedImage;

		UIImageView BackgroundView;
		SPPatternUnlockNubView NubView;

		Action Completion;
		Action Timeout;

		List<PointF> Points;

		public SPPatternUnlockSliderView(RectangleF frame, SPPatternUnlockPattern container, Action completion, Action timeout) : base(frame)
		{
			Points = container.Points;

			BackgroundUnlockedImage = container.BlueImage;
			Completion = completion;
			Timeout = timeout;

			BackgroundView = new UIImageView(new RectangleF(new PointF(), Frame.Size));
			BackgroundView.Image = container.RedImage;
			AddSubview(BackgroundView);

			NubView = new SPPatternUnlockNubView(Points[0]);
			NubView.Delegate = this;
			NubView.SetPathBounds(Points[0], Points[1], 1);
			AddSubview(NubView);
		}

		#region ISPPatternUnlockNubViewDelegate implementation
		public void ReachedEndPoint(int pointIndex, Action finalCompletion)
		{
			if( pointIndex == Points.Count-1 )
			{
				NubView.SetPathBounds(Points[pointIndex], Points[pointIndex], pointIndex);
				BackgroundView.Image = BackgroundUnlockedImage;

				if( Completion != null && !Unlocked )
				{
					Unlocked = true;
					Completion();

					if( finalCompletion != null )
					{
						finalCompletion();
					}
				}
			}
			else
			{
				NubView.SetPathBounds(Points[pointIndex], Points[pointIndex+1], pointIndex+1);
			}
		}

		public void FingerLiftedTimeout()
		{
			if( Timeout != null )
			{
				Timeout();
			}
		}
		#endregion ISPPatternUnlockNubViewDelegate implementation
	}

	public class SPPatternUnlockPattern
	{
		public UIImage RedImage { get; private set; }
		public UIImage BlueImage { get; private set; }
		public List<PointF> Points { get; private set; }

		public SPPatternUnlockPattern(UIImage redImage, UIImage blueImage, List<PointF> points)
		{
			RedImage = redImage;
			BlueImage = blueImage;
			Points = points;
		}
	}
}
