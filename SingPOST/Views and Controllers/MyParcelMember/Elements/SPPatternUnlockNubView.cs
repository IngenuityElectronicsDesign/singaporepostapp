﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Timers;
using MonoTouch.Foundation;
using MonoTouch.CoreFoundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public interface ISPPatternUnlockNubViewDelegate
	{
		void ReachedEndPoint(int pointIndex, Action finalCompletion);
		void FingerLiftedTimeout();
	} 

	public class SPPatternUnlockNubView : UIControl
	{
		public ISPPatternUnlockNubViewDelegate Delegate { get; set; }
		int PointIndex;
		bool DirectionIsDown;
		bool IsDiagonal;

		UIImageView BackgroundView;
		UIPanGestureRecognizer PanGestureRecogniser;

		public PointF MinBounds { get; private set; }
		public PointF MaxBounds { get; private set; }

		static float Radius = 20f;
		static SizeF Size = new SizeF(40f, 40f);

		Timer FingerLiftedTimer;
		bool TimeoutAllowed;

		public SPPatternUnlockNubView(PointF location) : base(new RectangleF(location, Size))
		{
			TimeoutAllowed = true;

			BackgroundView = new UIImageView(new RectangleF(3f, 3f, 34f, 34f));
			BackgroundView.Image = UIImage.FromFile("AirUnlock/slider.png");
			AddSubview(BackgroundView);

			PanGestureRecogniser = new UIPanGestureRecognizer( () =>
				{
					PointF translation = PanGestureRecogniser.TranslationInView(Superview);

					float X = Center.X + translation.X;
					float Y = Center.Y + translation.Y;

					X = Math.Min(Math.Max(X, MinBounds.X+Radius), MaxBounds.X-Radius);
					Y = Math.Min(Math.Max(Y, MinBounds.Y+Radius), MaxBounds.Y-Radius);

					if( IsDiagonal )
					{
						float ratio = ( ((MaxBounds.Y-Radius*2)-MinBounds.Y) / ((MaxBounds.X-Radius*2)-MinBounds.X) );
						Y = (DirectionIsDown ? 1 : -1) * ( ratio*(X-(MinBounds.X+Radius))) + (DirectionIsDown ? Radius : (MaxBounds.Y-Radius));
					}

					Center = new PointF((float)Math.Round(X), (float)Math.Round(Y));

					if( Center.X >= MaxBounds.X-(Radius+3f) && (DirectionIsDown ? Center.Y >= MaxBounds.Y-(Radius+3f) : Center.Y <= MinBounds.Y+(Radius+3f)) )
					{
						Delegate.ReachedEndPoint(PointIndex, () =>
							{
								SetFingerLiftedTimer(false);
								TimeoutAllowed = false;
							});
					}
//					else if( Center == new PointF(MinBounds.X+Radius, (DirectionIsDown ? MinBounds.Y+Radius : MaxBounds.Y-Radius)) )
//					{
//						Console.WriteLine("Start");
//					}

					PanGestureRecogniser.SetTranslation(new PointF(), Superview);

					if( PanGestureRecogniser.NumberOfTouches > 0 )
					{
						SetFingerLiftedTimer(false);
						PointF locationInNib = PanGestureRecogniser.LocationOfTouch(0, this);

						if( locationInNib.X < -Radius*0.5f || locationInNib.X > Frame.Width+Radius*0.5f || locationInNib.Y < -Radius*0.5f || locationInNib.Y > Frame.Height+Radius*0.5f )
						{
							PanGestureRecogniser.Enabled = false;
							PanGestureRecogniser.Enabled = true;
						}
					}
					else
					{
						SetFingerLiftedTimer(true);
					}
				});

			PanGestureRecogniser.MaximumNumberOfTouches = 1;
			PanGestureRecogniser.MinimumNumberOfTouches = 1;
			PanGestureRecogniser.CancelsTouchesInView = false;

			AddGestureRecognizer(PanGestureRecogniser);
		}

		public void SetPathBounds(PointF startPoint, PointF endPoint, int pointIndex)
		{
			PointIndex = pointIndex;
			DirectionIsDown = (startPoint.Y <= endPoint.Y);
			IsDiagonal = (startPoint.X != endPoint.X && startPoint.Y != endPoint.Y);

			float X = Math.Min(startPoint.X, endPoint.X);
			float Y = Math.Min(startPoint.Y, endPoint.Y);

			MinBounds = new PointF(X, Y);

			X = Math.Max(startPoint.X+2*Radius, endPoint.X+2*Radius);
			Y = Math.Max(startPoint.Y+2*Radius, endPoint.Y+2*Radius);

			MaxBounds = new PointF(X, Y);
		}

		private void SetFingerLiftedTimer(bool start)
		{
			if( start && FingerLiftedTimer == null && TimeoutAllowed )
			{
				FingerLiftedTimer = new Timer(2000); // 2 Second Timeout
				FingerLiftedTimer.Elapsed += (sender, e) => 
				{
					DispatchQueue.MainQueue.DispatchAsync( () =>
						{
							Delegate.FingerLiftedTimeout();
						});
					FingerLiftedTimer.Stop();
					FingerLiftedTimer = null;
				};
				FingerLiftedTimer.Start();
			}
			else if( !start && FingerLiftedTimer != null )
			{
				FingerLiftedTimer.Stop();
				FingerLiftedTimer = null;
			}
		}
	}
}
