﻿using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public class SPMyParcelMemberHeader : SPCollapsableSectionHeader
	{
		public int Count
		{
			set
			{
				SetTitle(value + " " + Title);
			}
		}
		string Title;

		public SPMyParcelMemberHeader(string title) : this(title, false) { }

		public SPMyParcelMemberHeader(string title, bool isExpanded) : base(title, isExpanded)
		{
			Title = title;
			SetTitle(Title);
		}
	}
}
