using POPStation.Core.iOS;
using System;
using System.Drawing;
using System.Timers;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MonoTouch.CoreLocation;
using MonoTouch.CoreFoundation;

namespace POPStationiOS
{
	public partial class SPMyParcelMemberDetailTableViewController : UITableViewController
	{
		ParcelData ParcelData;
		bool IsAirUnlockVisible;

		SPButton AirUnlockButton;
		UIButton PinCoverButton;

		UIScreenEdgePanGestureRecognizer BackGesture;

		UIImage QRCodeImage = null;

		List<SPPatternUnlockPattern> Patterns = new List<SPPatternUnlockPattern> // Fill the list
		{
			new SPPatternUnlockPattern(UIImage.FromFile("AirUnlock/pattern-1red.png"), UIImage.FromFile("AirUnlock/pattern-1blue.png"), new List<PointF> (new PointF[] {new PointF(0f, 0f), new PointF(110f, 0f), new PointF(110f, 40f), new PointF(170f, 40f), new PointF(170f, 0f), new PointF(240f, 0f)})),
			new SPPatternUnlockPattern(UIImage.FromFile("AirUnlock/pattern-2red.png"), UIImage.FromFile("AirUnlock/pattern-2blue.png"), new List<PointF> (new PointF[] {new PointF(0f, 0f), new PointF(110f, 0f), new PointF(110f, 40f), new PointF(130f, 40f), new PointF(170f, 0f), new PointF(240f, 0f)})),
			new SPPatternUnlockPattern(UIImage.FromFile("AirUnlock/pattern-3red.png"), UIImage.FromFile("AirUnlock/pattern-3blue.png"), new List<PointF> (new PointF[] {new PointF(0f, 0f), new PointF(23f, 0f), new PointF(23f, 40f), new PointF(175f, 40f), new PointF(175f, 0f), new PointF(240f, 0f)})),
			new SPPatternUnlockPattern(UIImage.FromFile("AirUnlock/pattern-4red.png"), UIImage.FromFile("AirUnlock/pattern-4blue.png"), new List<PointF> (new PointF[] {new PointF(0f, 0f), new PointF(70f, 0f), new PointF(110f, 40f), new PointF(170f, 40f), new PointF(170f, 0f), new PointF(240f, 0f)})),
			new SPPatternUnlockPattern(UIImage.FromFile("AirUnlock/pattern-5red.png"), UIImage.FromFile("AirUnlock/pattern-5blue.png"), new List<PointF> (new PointF[] {new PointF(0f, 0f), new PointF(40f, 0f), new PointF(40f, 40f), new PointF(130f, 40f), new PointF(170f, 0f), new PointF(240f, 0f)}))
		};

		int PatternIndex;

		private TrackingDetailData[] m_TrackingData;

		private int CurrentLayoutType
		{
			get
			{
				if (ParcelData == null)
					return 1;
				switch(ParcelData.ParcelStatus)
				{
					case ParcelStatus.DeliveredToPopStation:
						return 2;
					case ParcelStatus.Collected:
					case ParcelStatus.DeliveredToPostOffice:
						return 3;
					default:
						// All other possibilities just return 1
						return 1;
				}
			}
		}

		public SPMyParcelMemberDetailTableViewController(IntPtr handle) : base(handle)
		{
		}

		public void Setup(ParcelData parcelData)
		{
			ParcelData = parcelData;

			GetTrackingData();

			Random rand = new Random();
			PatternIndex = rand.Next(5);
		}

		public void GetTrackingData()
		{
			SPServerData.Instance.RESTTrackAndTraceSearch(ParcelData.TrackingNo, (success, response) =>
				{
					if(success)
					{
						m_TrackingData = response.Data.TrackingDetails.ToArray();
						InvokeOnMainThread( () =>
							{
								TableView.ReloadData();
							});
					}
					else
					{
						UIAlertView alert = new UIAlertView("Error Getting Parcel List", "Please try again, or contact support if the problem persists.", null, "OK", null);
						alert.Show();
					}
				});
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse( typeof(SPMyParcelMemberDetailCell), new NSString("TrackAndTraceCell") );

			NavigationItem.Title = ParcelData.TrackingNo;

			// Set the nav items of the new view
			UIBarButtonItem negativeSpacer = new UIBarButtonItem(UIBarButtonSystemItem.FixedSpace);
			negativeSpacer.Width = -12f; // -16 is the left edge

			UIBarButtonItem backButton = new UIBarButtonItem(UIImage.FromFile("Bar/arrowleft.png"), UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
				{
					NavigationController.PopViewControllerAnimated(true);
				});
			NavigationItem.LeftBarButtonItems = new UIBarButtonItem[] {negativeSpacer, backButton};
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			IsAirUnlockVisible = false;

			BackGesture = new UIScreenEdgePanGestureRecognizer();
			BackGesture.Edges = UIRectEdge.Left;
			BackGesture.AddTarget( () =>
				{
					NavigationController.PopViewControllerAnimated(true);
				});

			View.AddGestureRecognizer(BackGesture);

			if( CLLocationManager.IsMonitoringAvailable(typeof(CLBeaconRegion)) )
			{
				iBeaconComms.Instance.RangeValue = ParcelData.PopStation.Range;

				iBeaconComms.Instance.RangeEnteredAction = (int major, int minor) =>
				{
					if( CurrentLayoutType == 2 && ParcelData.PopStation != null )
					{
						if( ParcelData.IsPaymentRequired )
						{
							if( IsAirUnlockVisible )
							{
								IsAirUnlockVisible = false;
								TableView.ReloadSections(new NSMutableIndexSet(0), UITableViewRowAnimation.Fade);
							}
						}
						else if( ParcelData.PopStation.GetMajor() != -1 && ParcelData.PopStation.GetMinor() != -1 && ParcelData.PopStation.GetMajor() == major && ParcelData.PopStation.GetMinor() == minor )
						{
							if( !IsAirUnlockVisible )
							{
								IsAirUnlockVisible = true;
								TableView.ReloadSections(new NSMutableIndexSet(0), UITableViewRowAnimation.Fade);
							}
						}
						else
						{
							if( IsAirUnlockVisible )
							{
								IsAirUnlockVisible = false;
								TableView.ReloadSections(new NSMutableIndexSet(0), UITableViewRowAnimation.Fade);
							}
						}
					}

				};
				iBeaconComms.Instance.RangeLeftAction = () =>
				{
					if( IsAirUnlockVisible )
					{
						IsAirUnlockVisible = false;
						TableView.ReloadSections(new NSMutableIndexSet(0), UITableViewRowAnimation.Fade);
					}
				};
			}
		}

		public override void ViewWillDisappear(bool animated)
		{
			View.RemoveGestureRecognizer(BackGesture);

			iBeaconComms.Instance.RangeValue = iBeaconComms.Instance.DefaultRangeValue;

			iBeaconComms.Instance.RangeEnteredAction = null;
			iBeaconComms.Instance.RangeLeftAction = null;

			base.ViewWillDisappear(animated);
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 1;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			if(m_TrackingData == null)
			{
				return 0;
			}

			return m_TrackingData.Length;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			SPMyParcelMemberDetailCell cell = (SPMyParcelMemberDetailCell)TableView.DequeueReusableCell(new NSString("TrackAndTraceCell"), indexPath);
			cell.Setup(m_TrackingData[indexPath.Row].Date.ToString("yyyy-MM-dd hh:mm"), m_TrackingData[indexPath.Row].StatusString);
			return cell;
		}

		public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			SizeF size = SizeF.Empty;
			if( !string.IsNullOrEmpty(m_TrackingData[indexPath.Row].StatusString) )
			{
				size = new NSString(m_TrackingData[indexPath.Row].StatusString).StringSize(UIFont.FromName("AvenirNextCondensed-Regular", 15f), new SizeF(SPMyParcelMemberDetailCell.LabelWidth, 9999f), UILineBreakMode.WordWrap);
			}
			return Math.Max(size.Height + 10f, 30f);
		}

		public override float GetHeightForHeader(UITableView tableView, int section)
		{
			return GetHeightOfPanel() + 78f;
		}

		public override UIView GetViewForHeader(UITableView tableView, int section)
		{
			UIView HeaderView = new UIView(new RectangleF(0f, 0f, 320f, GetHeightOfPanel() + 20f));

			UIImageView BackgroundView = new UIImageView(new RectangleF(10f, 10f, 300f, GetHeightOfPanel()));
			BackgroundView.Image = UIImage.FromFile("Body/Backing-View-Panel.png").CreateResizableImage(new UIEdgeInsets(15f, 15f, 15f, 15f));
			HeaderView.AddSubview(BackgroundView);

			TableView.ScrollEnabled = true;

			// Parcel Details Section

			UIImageView ParcelIconView = new UIImageView(new RectangleF(15f, 15f, 40f, 40f));
			ParcelIconView.Image = UIImage.FromFile ("StatusDetail/" + ParcelData.GetIconFileNameForStatus());
			BackgroundView.AddSubview(ParcelIconView);

			UILabel ParcelDetailsLabel = new UILabel(new RectangleF(80f, 8f, 200f, 18f));
			ParcelDetailsLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 15f);
			ParcelDetailsLabel.Text = "Parcel details";
			ParcelDetailsLabel.TextColor = UIColor.Black;
			BackgroundView.AddSubview(ParcelDetailsLabel);

			UILabel MerchantNameLabel = new UILabel(new RectangleF(80f, 20f, 200f, 30f));
			MerchantNameLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 20f);
			MerchantNameLabel.Text = ParcelData.Merchant;
			MerchantNameLabel.TextColor = SPShared.Instance.RedTintColor;
			BackgroundView.AddSubview(MerchantNameLabel);

			UILabel ParcelNumberLabel = new UILabel(new RectangleF(80f, 43f, 200f, 18f));
			ParcelNumberLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 15f);
			ParcelNumberLabel.Text = ParcelData.TrackingNo;
			ParcelNumberLabel.TextColor = SPShared.Instance.BlueTintColor;
			BackgroundView.AddSubview(ParcelNumberLabel);

			UIView ParcelDetailsLine = new UIView(new RectangleF(0f, 70f, 300f, 1f));
			ParcelDetailsLine.BackgroundColor = UIColor.GroupTableViewBackgroundColor;
			BackgroundView.AddSubview(ParcelDetailsLine);

			// Status Expiry Section

			UILabel StatusTitleLabel = new UILabel(new RectangleF(10f, 73f, 60f, 21f));
			StatusTitleLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 15f);
			StatusTitleLabel.Text = "Status";
			StatusTitleLabel.TextColor = UIColor.Black;
			BackgroundView.AddSubview(StatusTitleLabel);

			UILabel StatusLabel = new UILabel(new RectangleF(80f, 73f, 200f, 41f));
			StatusLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 15f);
			StatusLabel.Text = ParcelData.StatusString;
			StatusLabel.TextColor = SPShared.Instance.BlueTintColor;
			StatusLabel.Lines = 2;
			StatusLabel.AdjustsFontSizeToFitWidth = true;
			StatusLabel.MinimumScaleFactor = 0.9f;
			StatusLabel.SizeToFit();
			BackgroundView.AddSubview(StatusLabel);

			// This only appears in Layout 2
			if (CurrentLayoutType == 2)
			{
				UILabel ExpiresTitleLabel = new UILabel (new RectangleF (10f, 113f, 60f, 18f));
				ExpiresTitleLabel.Font = UIFont.FromName ("AvenirNextCondensed-Regular", 15f);
				ExpiresTitleLabel.Text = "Expires";
				ExpiresTitleLabel.TextColor = UIColor.Black;
				BackgroundView.AddSubview (ExpiresTitleLabel);

				UILabel ExpiresLabel = new UILabel (new RectangleF (80f, 113f, 200f, 18f));
				ExpiresLabel.Font = UIFont.FromName ("AvenirNextCondensed-Regular", 15f);
				ExpiresLabel.Text = ParcelData.PopStation.ExpiryDate.ToString("dd MMMM yyyy");
				ExpiresLabel.TextColor = SPShared.Instance.BlueTintColor;
				BackgroundView.AddSubview (ExpiresLabel);
			}

			float height;

			// Location Section only shown in 2 and 3
			if (CurrentLayoutType != 1)
			{
				height = CurrentLayoutType == 2 ? 136f : 119f;
				UIView StatusExpiryLine = new UIView(new RectangleF(0f, height, 300f, 1f));
				StatusExpiryLine.BackgroundColor = UIColor.GroupTableViewBackgroundColor;
				BackgroundView.AddSubview(StatusExpiryLine);

				height = CurrentLayoutType == 2 ? 141f : 124f;
				UILabel LocationTitleLabel = new UILabel (new RectangleF (10f, height, 60f, 18f));
				LocationTitleLabel.Font = UIFont.FromName ("AvenirNextCondensed-Regular", 15f);
				LocationTitleLabel.Text = "Location";
				LocationTitleLabel.TextColor = UIColor.Black;
				BackgroundView.AddSubview (LocationTitleLabel);

				UILabel LocationLabel = new UILabel (new RectangleF (80f, height, 180f, 18f));
				LocationLabel.Font = UIFont.FromName ("AvenirNextCondensed-Regular", 15f);
				LocationLabel.Text = ParcelData.Location.Name;
				LocationLabel.TextColor = SPShared.Instance.BlueTintColor;
				BackgroundView.AddSubview (LocationLabel);

				height = CurrentLayoutType == 2 ? 158f : 141f;
				UILabel AddressLabel = new UILabel (new RectangleF (80f, height, 180f, 18f));
				AddressLabel.Font = UIFont.FromName ("AvenirNextCondensed-Regular", 15f);
				AddressLabel.Text = ParcelData.Location.Address;
				AddressLabel.TextColor = UIColor.FromWhiteAlpha (0.33f, 1f);
				AddressLabel.AdjustsFontSizeToFitWidth = true;
				AddressLabel.MinimumScaleFactor = 0.75f;
				BackgroundView.AddSubview(AddressLabel);

				// Sometimes there isn't a post code sent from the API, so don't show this button if there isn't one.
				if( !string.IsNullOrEmpty (ParcelData.Location.PostCode) )
				{
					height = CurrentLayoutType == 2 ? 146f : 129f;
					SPButton LocationButton = new SPButton (new PointF (90f, height), null, "", null, false);
					LocationButton.Frame = new RectangleF (90f, height, 220f, 45f);
					LocationButton.Button.TouchUpInside += (sender, e) =>
					{
						SPLocationsViewController toPush = (SPLocationsViewController)Storyboard.InstantiateViewController ("LocationsVC");
						toPush.Setup(ParcelData.Location.Name, ParcelData.Location.PostCode);
						NavigationController.PushViewController (toPush, true);

						//SPShared.Instance.NavMenuTableViewController.ShouldTransition(1, true, null);
					};
					HeaderView.AddSubview (LocationButton);
				

					height = CurrentLayoutType == 2 ? 143f : 126f;
					UIImageView LocationIconView = new UIImageView (new RectangleF (265f, height, 30f, 30f));
					LocationIconView.Image = UIImage.FromFile ("Body/location.png");
					BackgroundView.AddSubview (LocationIconView);
				}
			}

			if( CurrentLayoutType == 2 )
			{
				UIView LocationLine = new UIView(new RectangleF(0f, 181f, 300f, 1f));
				LocationLine.BackgroundColor = UIColor.GroupTableViewBackgroundColor;
				BackgroundView.AddSubview(LocationLine);

				// Pin Locker QR Section

				UILabel PinTitleLabel = new UILabel(new RectangleF(10f, 186f, 60f, 18f));
				PinTitleLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 15f);
				PinTitleLabel.Text = "PIN";
				PinTitleLabel.TextColor = UIColor.Black;
				BackgroundView.AddSubview(PinTitleLabel);

				UILabel PinLabel = new UILabel(new RectangleF(10f, 221f, 60f, 18f));
				PinLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 15f);
				PinLabel.Text = ParcelData.PopStation.CollectionPin;
				PinLabel.TextColor = SPShared.Instance.BlueTintColor;
				PinLabel.TextAlignment = UITextAlignment.Center;
				BackgroundView.AddSubview(PinLabel);

				PinCoverButton = new UIButton(new RectangleF (18f, 211f, 64f, 60f));
				PinCoverButton.SetTitle ("TAP TO\nSHOW", UIControlState.Normal);
				PinCoverButton.SetTitleColor(SPShared.Instance.BlueTintColor, UIControlState.Normal);
				PinCoverButton.TitleLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 15f);

				PinCoverButton.TitleLabel.TextColor = SPShared.Instance.BlueTintColor;
				PinCoverButton.TitleLabel.BackgroundColor = UIColor.White;
				PinCoverButton.TitleLabel.TextAlignment = UITextAlignment.Center;
				PinCoverButton.TitleLabel.Lines = 2;
				PinCoverButton.TitleLabel.LineBreakMode = UILineBreakMode.CharacterWrap;
				PinCoverButton.TouchUpInside += (sender, e) =>
				{
					UIView.AnimateNotify(0.8f, 0f, 1f, 1f, UIViewAnimationOptions.AllowUserInteraction, () =>
					{
						PinCoverButton.Center = new PointF(-70f, PinCoverButton.Center.Y);
						PinCoverButton.Alpha = 0f;
					}, (finished) => 
					{
						PinCoverButton.RemoveFromSuperview();
						PinCoverButton = null;
					});
				};
				// We have to add it to the header view, otherwise the button doesn't work.
				HeaderView.AddSubview(PinCoverButton);

				UIView PinLine = new UIView(new RectangleF(0f, 266f, 75f, 1f));
				PinLine.BackgroundColor = UIColor.GroupTableViewBackgroundColor;
				BackgroundView.AddSubview(PinLine);

				UILabel LockerTitleLabel = new UILabel(new RectangleF(10f, 271f, 60f, 18f));
				LockerTitleLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 15f);
				LockerTitleLabel.Text = "Locker";
				LockerTitleLabel.TextColor = UIColor.Black;
				BackgroundView.AddSubview(LockerTitleLabel);

				UILabel LockerLabel = new UILabel(new RectangleF(10f, 316f, 60f, 18f));
				LockerLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 15f);
				LockerLabel.Text = ParcelData.PopStation.LockerNo;
				LockerLabel.TextColor = SPShared.Instance.BlueTintColor;
				LockerLabel.TextAlignment = UITextAlignment.Center;
				BackgroundView.AddSubview(LockerLabel);

				UIView PinLockerQRLine = new UIView(new RectangleF(0f, 381f, 300f, 1f));
				PinLockerQRLine.BackgroundColor = UIColor.GroupTableViewBackgroundColor;
				BackgroundView.AddSubview(PinLockerQRLine);

				UILabel QRTitleLabel = new UILabel(new RectangleF(80f, 186f, 60f, 18f));
				QRTitleLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 15f);
				QRTitleLabel.Text = "QR code";
				QRTitleLabel.TextColor = UIColor.Black;
				BackgroundView.AddSubview(QRTitleLabel);

				UIImageView QRImageView = new UIImageView(new RectangleF(115f, 221f, 150f, 150f));

				if( QRCodeImage == null )
				{
					QRImageView.Alpha = 0;
					DispatchQueue.GetGlobalQueue(DispatchQueuePriority.Background).DispatchAsync( () =>
						{
							NSData imageData = NSData.FromUrl(NSUrl.FromString(ParcelData.PopStation.QrImageUrl));

							if( imageData != null )
							{
								QRCodeImage = UIImage.LoadFromData(imageData);
							}

							DispatchQueue.MainQueue.DispatchAsync( () =>
								{
									if( QRImageView != null )
									{
										QRImageView.Image = QRCodeImage;
										UIView.Animate(0.25f, () =>
											{
												QRImageView.Alpha = 1f;
											});
									}
								});
						});
				}
				else
				{
					QRImageView.Image = QRCodeImage;
				}

				BackgroundView.AddSubview(QRImageView);

				UIView QRLine = new UIView(new RectangleF(75f, 181, 1f, 200f));
				QRLine.BackgroundColor = UIColor.GroupTableViewBackgroundColor;
				BackgroundView.AddSubview(QRLine);

				// Air Unlock Section

				if(CLLocationManager.IsMonitoringAvailable(typeof(CLBeaconRegion)))
				{
					UILabel AirUnlockTitleLabel = new UILabel (new RectangleF (10f, 391f, 60f, 18f));
					AirUnlockTitleLabel.Font = UIFont.FromName ("AvenirNextCondensed-Regular", 16f);
					AirUnlockTitleLabel.Text = "Air Unlock";
					AirUnlockTitleLabel.TextColor = UIColor.Black;
					BackgroundView.AddSubview(AirUnlockTitleLabel);

					UILabel AirUnlockSubtitleLabel = new UILabel (new RectangleF (75f, 391f, 180f, 18f));
					AirUnlockSubtitleLabel.Font = UIFont.FromName ("AvenirNextCondensed-Regular", 14f);
					AirUnlockSubtitleLabel.Text = "- Swipe to collect your parcel.";
					AirUnlockSubtitleLabel.TextColor = UIColor.Black;
					BackgroundView.AddSubview(AirUnlockSubtitleLabel);

					SPButton HelpButton = new SPButton(new PointF(265f, 381f), UIImage.FromFile("Body/help.png"), "", null, false);
					HelpButton.Button.Frame = new RectangleF(0f, 0f, 50f, 50f); // Size and Location 15px padded out each side as the icon was too small to tap
					HelpButton.Button.TouchUpInside += (sender, e) =>
					{
						string message = "Please swipe the white circle from left and release it at the right most to unlock the POPStation locker.";

						UIAlertView alert = new UIAlertView("Air Unlock Help", message, null, "OK", null);
						alert.Show();
					};
					HeaderView.AddSubview(HelpButton);
				}

				if(IsAirUnlockVisible)
				{
					SPPatternUnlockSliderView AirUnlockSlider = new SPPatternUnlockSliderView (new RectangleF (20f, 421f, 280f, 80f), Patterns [PatternIndex], () => // Completion
						{
							UIAlertView alert = new UIAlertView ("Air Unlock your locker?", "Please confirm you want to open the locker?", null, "Cancel", "Open");
							alert.Clicked += (sender, e) =>
							{
								if( e.ButtonIndex == 1 )
								{
									SPServerData.Instance.ServerRemotePickup(ParcelData.TrackingNo, (success, error, message) =>
										{
											InvokeOnMainThread( () =>
												{
													if( success )
													{
														ParcelData.ParcelStatus = ParcelStatus.Collected;
														ParcelData.StatusString = "Collected at POPStation by customer";
														ParcelData.PopStation = null;

														var timer = new Timer(1000);
														timer.Elapsed += (s, ev) => 
														{
															InvokeOnMainThread( () =>
																{
																	GetTrackingData();
																});
															timer.Stop();
															timer = null;
														};
														timer.Start();

														iBeaconComms.Instance.RangeEnteredAction = null;
														iBeaconComms.Instance.RangeLeftAction = null;
													}
													else
													{
														if( !string.IsNullOrEmpty(error) )
														{
															UIAlertView errorAlert = new UIAlertView(error, message, null, "OK", null);
															errorAlert.Show();
														}
														else
														{
															UIAlertView errorAlert = new UIAlertView("Air Unlock " + message, "Please try again, or contact support if the problem persists.", null, "OK");
															errorAlert.Show();
														}
													}

													TableView.ReloadSections(new NSMutableIndexSet(0), UITableViewRowAnimation.Fade);
												});
										});
								}
								else
								{
									TableView.ReloadSections(new NSMutableIndexSet(0), UITableViewRowAnimation.Fade);
								}
							};
							alert.Show ();
						}, () => // Timeout
						{
							TableView.ReloadSections(new NSMutableIndexSet(0), UITableViewRowAnimation.Fade);
						});

					HeaderView.AddSubview(AirUnlockSlider);
				}
				else
				{
					if(CLLocationManager.IsMonitoringAvailable(typeof(CLBeaconRegion)))
					{
						AirUnlockButton = new SPButton( new PointF(20f, 428f), UIImage.FromFile("Body/See-more.png"), "Not Available", UIImage.FromFile("Body/parcellistarrow-collected.png"), false );
						AirUnlockButton.Button.TouchUpInside += (sender, e) => // Button Pressed
						{
							UIAlertView alert = new UIAlertView("Air Unlock is Not Available", "• You must be within 5 meters of the    \ndesignated POPStation.                \u0000\n• Your Bluetooth and Location               \nServices must be enabled.            \u0000", null, "OK", null); // Spaces as SingPost wanted this left aligned which is not possible on iOS7 default UIAlertView Labels.
//							UILabel label = new UILabel()
//
//							alert.AddSubview()
							alert.Show();
						};
						HeaderView.AddSubview(AirUnlockButton);

						if( ParcelData.IsPaymentRequired )
						{
							UILabel AirUnlockCODLabel = new UILabel (new RectangleF (10f, 466f, 280f, 18f));
							AirUnlockCODLabel.Font = UIFont.FromName ("AvenirNextCondensed-Regular", 13f);
							AirUnlockCODLabel.Text = "Currently not available for items that require payments.";
							AirUnlockCODLabel.TextColor = SPShared.Instance.RedTintColor;
							BackgroundView.AddSubview(AirUnlockCODLabel);
						}
					}
				}

				HeaderView.BringSubviewToFront(PinCoverButton);
			}
			// Audit Trail Section
			UILabel AuditTrailTitleLabel;
			UILabel DateTimeHeaderLabel;
			UILabel StatusHeaderLabel;
			UIView AuditTrailLine;

			AuditTrailTitleLabel = new UILabel (new RectangleF (15f, GetHeightOfPanel() + 25f, 140f, 20f));
			AuditTrailTitleLabel.Font = UIFont.FromName ("AvenirNextCondensed-Regular", 16f);
			AuditTrailTitleLabel.Text = "Tracking Details";
			AuditTrailTitleLabel.TextColor = UIColor.FromWhiteAlpha (0.1f, 1f);
			HeaderView.AddSubview (AuditTrailTitleLabel);

			DateTimeHeaderLabel = new UILabel (new RectangleF (15f, GetHeightOfPanel() + 55f, 140f, 20f));
			DateTimeHeaderLabel.Font = UIFont.FromName ("AvenirNextCondensed-Regular", 16f);
			DateTimeHeaderLabel.Text = "Date Time";
			DateTimeHeaderLabel.TextColor = SPShared.Instance.BlueTintColor;
			HeaderView.AddSubview (DateTimeHeaderLabel);

			StatusHeaderLabel = new UILabel (new RectangleF (160f, GetHeightOfPanel() + 55f, 150f, 20f));
			StatusHeaderLabel.Font = UIFont.FromName ("AvenirNextCondensed-Regular", 16f);
			StatusHeaderLabel.Text = "Status";
			StatusHeaderLabel.TextColor = SPShared.Instance.BlueTintColor;
			HeaderView.AddSubview (StatusHeaderLabel);

			AuditTrailLine = new UIView (new RectangleF (5f, GetHeightOfPanel() + 78f, 310f, 1f));
			AuditTrailLine.BackgroundColor = UIColor.FromWhiteAlpha (0.67f, 1f);
			HeaderView.AddSubview (AuditTrailLine);

			return HeaderView;
		}

		#endregion Table View Logic

		float GetHeightOfPanel()
		{
			switch(CurrentLayoutType)
			{
				case 1:
					return 122f;
				case 2:
					return 498f; //(IsAirUnlockVisible ? 477f : 447f);
				case 3:
					return 174f;
				default:
					return 0f;
			}
		}
	}
}
