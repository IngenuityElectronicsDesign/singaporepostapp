using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public partial class SPNavBar : UINavigationBar
	{
		private static float HeightIncrease = 38f;

		private UIImageView OriginalBorderLine;
		private UIView CustomBorderLine;

		public SPNavBar(IntPtr handle) : base(handle)
		{
			TintColor = UIApplication.SharedApplication.Delegate.Window.TintColor;

			BackIndicatorImage = UIImage.FromFile("Bar/arrowleft.png").ImageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal);
			BackIndicatorTransitionMaskImage = UIImage.FromFile("Bar/arrowleft.png");

			CustomBorderLine = new UIView(new RectangleF(0f, Frame.Height + HeightIncrease, Frame.Width, 1f));
			CustomBorderLine.BackgroundColor = TintColor;
			AddSubview(CustomBorderLine);
		}

		public override void LayoutSubviews()
		{
			base.LayoutSubviews();

			if( (OriginalBorderLine = FindHairlineImageViewUnder(this)) != null )
			{
				OriginalBorderLine.Hidden = true;
			}

			CustomBorderLine.Frame = new RectangleF(0f, Frame.Size.Height, Frame.Size.Width, 1f);

			UIStringAttributes attr = new UIStringAttributes();
			attr.ForegroundColor = TintColor;
			attr.Font = UIFont.FromName("AvenirNextCondensed-Regular", 20f);
			TitleTextAttributes = attr;
		}

		public void SetBorderLineShouldHide(bool shouldHide, bool animated)
		{
			if( animated )
			{
				UIView.Animate(0.3f, () => // Anims
					{
						CustomBorderLine.BackgroundColor = shouldHide ? UIColor.Clear : TintColor;
					});
			}
			else
			{
				CustomBorderLine.BackgroundColor = shouldHide ? UIColor.Clear : TintColor;
			}
		}

		private UIImageView FindHairlineImageViewUnder(UIView view)
		{
			if( view.GetType() == typeof(UIImageView) && view.Bounds.Height <= 1f )
			{
				return ((UIImageView)view);
			}
			foreach( UIView subview in view.Subviews )
			{
				UIImageView imageView = FindHairlineImageViewUnder(subview);
				if( imageView != null )
				{
					return imageView;
				}
			}
			return null;
		}
	}
}