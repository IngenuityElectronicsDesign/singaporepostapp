﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public class SPNavPopoutBar : UIToolbar
	{
		private const float AnimDuration = 0.3f;

		public bool IsVisible { get; set; }
		public UIScrollView ScrollView { get; private set; }

		protected UIView BorderLine { get; private set; }
		public UINavigationBar ParentBar { get; private set; }

		public static RectangleF DefaultFrame = new RectangleF(0f, 0f, 320f, 50f);

		public SPNavPopoutBar(UINavigationBar parentBar, UIScrollView scrollView) : base(DefaultFrame)
		{
			Setup(parentBar, scrollView);
		}

		public SPNavPopoutBar(RectangleF frame, UINavigationBar parentBar, UIScrollView scrollView) : base(frame)
		{
			Setup(parentBar, scrollView);
		}

		void Setup(UINavigationBar parentBar, UIScrollView scrollView)
		{
			TintColor = UIApplication.SharedApplication.Delegate.Window.TintColor;

			ScrollView = scrollView;
			ParentBar = parentBar;

			BorderLine = new UIView(new RectangleF(0f, Frame.Height, Frame.Width, 1f));
			BorderLine.BackgroundColor = TintColor.ColorWithAlpha(0.8f);
			AddSubview(BorderLine);

			IsVisible = false;
		}

		#region Util Funcs
		RectangleF GetFinalFrame(UINavigationBar bar)
		{
			return new RectangleF(bar.Frame.X, bar.Frame.Y + bar.Frame.Height, bar.Frame.Width, Frame.Height);
		}

		RectangleF GetInitialFrame(UINavigationBar bar)
		{
			RectangleF rect = GetFinalFrame(bar);
			rect.Y -= rect.Height;
			return rect;
		}
		#endregion Util Funcs

		#region Popout Logic
		public void Show(bool animated, Action completion)
		{
			if( IsVisible )
			{
				if( completion != null )
				{
					completion();
				}
				return;
			}

			// Position the popout bar behind the navigation bar
			ParentBar.Superview.InsertSubviewBelow(this, ParentBar);
			if( animated )
			{
				this.Frame = GetInitialFrame(ParentBar);
			}

			float height = 0f;
			float visible = 0f;
			float diff = 0f;
			float fix = 0f;

			if( ScrollView != null )
			{

				height = Frame.Height;
				visible = ScrollView.Bounds.Height - ScrollView.ContentInset.Top - ScrollView.ContentInset.Bottom;
				diff = visible - ScrollView.ContentSize.Height;
				fix = Math.Max(0f, Math.Min(height, diff));

				// Increase the top inset of the scroll view, so the first cell is not covered by the popout bar
				UIEdgeInsets insets = ScrollView.ContentInset;
				insets.Top += height;
				ScrollView.ContentInset = insets;
				ScrollView.ScrollIndicatorInsets = insets;
				ScrollView.ContentOffset = new PointF(ScrollView.ContentOffset.X, ScrollView.ContentOffset.Y + fix);
			}


			if( animated )
			{
				UIView.Animate(AnimDuration, () => // Anims
					{
						Frame = GetFinalFrame(ParentBar);
						if( ScrollView != null )
						{
							ScrollView.ContentOffset = new PointF(ScrollView.ContentOffset.X, ScrollView.ContentOffset.Y - height);
						}
					}, () => // Completion
					{
						IsVisible = true;

						if( completion != null )
						{
							completion();
						}
					});
			}
			else
			{
				Frame = GetFinalFrame(ParentBar);
				if( ScrollView != null )
				{
					ScrollView.ContentOffset = new PointF(ScrollView.ContentOffset.X, ScrollView.ContentOffset.Y - height);
				}
				IsVisible = true;

				if( completion != null )
				{
					completion();
				}
			}
		}

		public void Hide(bool animated, Action completion)
		{
			if( !IsVisible )
			{
				if( completion != null )
				{
					completion();
				}
				return;
			}

			if( ParentBar == null )
			{
				Console.WriteLine("Should hide popout bar before releasing nav bar");
				return;
			}

			float height = 0f;
			float visible = 0f;
			float fix = 0f;

			if( ScrollView != null )
			{
				height = Frame.Height;
				visible = ScrollView.Bounds.Height - ScrollView.ContentInset.Top - ScrollView.ContentInset.Bottom;
				fix = height;

				if( visible <= ScrollView.ContentSize.Height )
				{
					float bottom = -ScrollView.ContentOffset.Y + ScrollView.ContentSize.Height;
					float diff = bottom - ScrollView.Bounds.Height + ScrollView.ContentInset.Bottom;
					fix = Math.Max(0f, Math.Min(height, diff));
				}
				float offset = height - (ScrollView.ContentOffset.Y + ScrollView.ContentInset.Top);
				float topFix = Math.Max(0f, Math.Min(height, offset));

				// Reverse the change to the inset of the affected scroll view
				UIEdgeInsets insets = ScrollView.ContentInset;
				insets.Top -= height;
				ScrollView.ContentInset = insets;
				ScrollView.ScrollIndicatorInsets = insets;
				ScrollView.ContentOffset = new PointF(ScrollView.ContentOffset.X, ScrollView.ContentOffset.Y - topFix);
			}

			if( animated )
			{
				UIView.Animate(AnimDuration, () => // Anims
					{
						Frame = GetInitialFrame(ParentBar);
						if( ScrollView != null )
						{
							ScrollView.ContentOffset = new PointF(ScrollView.ContentOffset.X, ScrollView.ContentOffset.Y + fix);
						}
					}, () => // Completion
					{
						IsVisible = false;

						if( completion != null )
						{
							completion();
						}
					});
			}
			else
			{
				Frame = GetInitialFrame(ParentBar);
				if( ScrollView != null )
				{
					ScrollView.ContentOffset = new PointF(ScrollView.ContentOffset.X, ScrollView.ContentOffset.Y + fix);
				}
				IsVisible = false;

				if( completion != null )
				{
					completion();
				}
			}
		}
		#endregion Popout Logic
	}
}

