﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public class SPTrackAndTrackPopoutBar : SPNavPopoutBar
	{
		UIImageView ViewContainer;
		public UITextField SearchField { get; set; }
		UIButton SearchButton;

		public Action SearchAction { get; set; }

		public SPTrackAndTrackPopoutBar(UINavigationBar parentBar, UIScrollView scrollView) : base(parentBar, scrollView)
		{
			Setup();
		}

		public void Setup()
		{
			ViewContainer = new UIImageView(new RectangleF(10f, 0f, 300f, 36f));
			ViewContainer.Image = UIImage.FromFile("Body/Tracking-Number-Search-Bar.png");

			SearchField = new UITextField(new RectangleF(10f, 0f, 264f, 36f));
			SearchField.Font = UIFont.FromName("AvenirNextCondensed-Italic", 16f);
			SearchField.TextColor = SPShared.Instance.BlueTintColor;
			SearchField.Placeholder = "Please enter tracking number";
			SearchField.ReturnKeyType = UIReturnKeyType.Search;
			SearchField.KeyboardType = UIKeyboardType.NumbersAndPunctuation;
			SearchField.EditingDidBegin += (sender, e) => 
			{
				SearchField.Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);
			};
			SearchField.ShouldReturn += (sender) =>
			{
				SearchField.ResignFirstResponder();
				SearchField.Font = UIFont.FromName("AvenirNextCondensed-Italic", 16f);

				if( SearchAction != null )
				{
					SearchAction();
				}

				return true;
			};
			ViewContainer.AddSubview(SearchField);

			SearchButton = new UIButton(UIButtonType.Custom);
			SearchButton.SetImage(UIImage.FromFile("Body/fieldsearcharrow.png"), UIControlState.Normal);
			SearchButton.Frame = new RectangleF(267f, 3f, 30f, 30f);
			SearchButton.TouchUpInside += (sender, e) => // Pressed
			{
				SearchField.Font = UIFont.FromName("AvenirNextCondensed-Italic", 16f);
				SearchField.EndEditing(false);

				if( SearchAction != null )
				{
					SearchAction();
				}
			};
			ViewContainer.AddSubview(SearchButton);

			UIBarButtonItem negativeSpacer = new UIBarButtonItem(UIBarButtonSystemItem.FixedSpace);
			negativeSpacer.Width = -6f; // -16 is the left edge
			UIBarButtonItem item = new UIBarButtonItem(ViewContainer);

			base.SetItems(new UIBarButtonItem[] {negativeSpacer, item}, false);

			SearchAction = null;
		}
	}
}

