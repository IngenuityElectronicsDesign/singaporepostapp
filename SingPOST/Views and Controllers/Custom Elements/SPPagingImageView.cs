﻿using POPStation.Core.iOS;
using System;
using System.Drawing;
using System.Timers;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.CoreFoundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public class SPPagingImageView : UIView
	{
		UIScrollView PagingView;
		UIPageControl PageControl;
		List<UIButton> ImageButtons;
		UITextView TextView;

		EMerchantBannerData[] DataArray;

		public int PageCount { get; private set; }


		public SPPagingImageView(RectangleF frame) : base(frame)
		{
		}

		public void Setup(EMerchantBannerData[] dataArray, Action[] buttonPressActions, double animDuration)
		{
			if( dataArray.Length != buttonPressActions.Length )
			{
				Console.Write("Merchant Banners Warning: Not all banners have a button press action assigned");
			}

			DataArray = dataArray;
			PageCount = DataArray.Length;

			// Setup ScrollView
			PagingView = new UIScrollView(new RectangleF(Frame.X, Frame.Y, Frame.Width, Frame.Height - 40f));
			PagingView.ContentSize = new SizeF(PagingView.Bounds.Width*PageCount, PagingView.Bounds.Height-20f);
			PagingView.PagingEnabled = true;
			PagingView.Bounces = true;
			PagingView.ShowsHorizontalScrollIndicator = false;
			PagingView.Scrolled += (sender, e) => 
			{
				// Get Page
				float pageWidth = Bounds.Width;
				int pageIndex = (int)Math.Floor((PagingView.ContentOffset.X - pageWidth / 2 ) / pageWidth) + 1;

				PageControl.CurrentPage = pageIndex;
			};
			AddSubview(PagingView);

			// Setup PageControl
			PageControl = new UIPageControl(new RectangleF(20f, Bounds.Height - 40f, Bounds.Width-40f, 20f));
			PageControl.Enabled = false;
			PageControl.Pages = PageCount;
			PageControl.CurrentPageIndicatorTintColor = SPShared.Instance.RedTintColor;
			PageControl.PageIndicatorTintColor = UIColor.FromWhiteAlpha(0.2f, 1f);
			AddSubview(PageControl);

			TextView = new UITextView (new RectangleF (5f, Bounds.Height - 20f, Bounds.Width, 30f));
			TextView.Text = "Merchants who ship to POPStation";
			TextView.Font = UIFont.FromName("AvenirNextCondensed-Regular", 14f);
			TextView.TextColor = UIColor.FromWhiteAlpha(0.24f, 1f);
			TextView.BackgroundColor = UIColor.Clear;
			AddSubview (TextView);

			// Setup Image Buttons
			ImageButtons = new List<UIButton>();

			for( int i = 0; i < DataArray.Length; i++ )
			{
				EMerchantBannerData data = DataArray[i];

				UIButton button = new UIButton(UIButtonType.Custom);
				button.Frame = new RectangleF(i*PagingView.Bounds.Width, 0f, PagingView.Bounds.Width, PagingView.Bounds.Height);
				button.Alpha = 0f;
				button.Tag = i;
				button.TouchUpInside += (sender, e) => 
				{
					if( buttonPressActions[Math.Min(button.Tag, buttonPressActions.Length-1)] != null )
					{
						buttonPressActions[Math.Min(button.Tag, buttonPressActions.Length-1)]();
					}
				};

				DispatchQueue.GetGlobalQueue(DispatchQueuePriority.Background).DispatchAsync( () =>
					{
						UIImage loadingImage = UIImage.LoadFromData(NSData.FromUrl(NSUrl.FromString(data.ImageUrl)));

						DispatchQueue.MainQueue.DispatchAsync( () =>
							{
								if( button != null )
								{
									button.SetImage(loadingImage, UIControlState.Normal);
									UIView.Animate(0.25f, () =>
										{
											button.Alpha = 1f;
										});
								}
							});
					});

				ImageButtons.Add(button);
				PagingView.AddSubview(button);
			}

			if( animDuration > 0 )
			{
				var timer = new Timer();
				timer.Interval = (animDuration*1000);
				timer.Elapsed += (sender, e) => 
				{
					DispatchQueue.MainQueue.DispatchAsync( () =>
						{
							PagingView.ScrollRectToVisible(new RectangleF(( PageControl.CurrentPage < PageCount-1 ? PageControl.CurrentPage+1 : 0 )*PagingView.Bounds.Width, 0f, PagingView.Bounds.Width, PagingView.Bounds.Height), true);
						});

//					if( true )
//					{
//						timer.Stop();
//						timer = null;
//					}
				};
				timer.Start();
			}
		}
	}
}
