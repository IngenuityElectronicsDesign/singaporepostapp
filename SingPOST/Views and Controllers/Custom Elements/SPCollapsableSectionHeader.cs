﻿using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public class SPCollapsableSectionHeader : UIView
	{
		public bool CanBeExpanded { get; set; }

		private bool IsExpanded;
		public bool Expanded
		{
			get
			{
				return IsExpanded;
			}
			set
			{
				if( CanBeExpanded )
				{
					IsExpanded = value;

					Button.SetImage((IsExpanded ? BackgroundExpanded : BackgroundCollapsed), UIControlState.Normal);
					IconView.Image = (IsExpanded ? ArrowExpanded : ArrowCollapsed);
				}
			}
		}

		public Action ButtonAction;

		public UIButton Button { get; private set; }
		public UILabel TitleLabel { get; private set; }
		public UIImageView IconView { get; private set; }

		UIImage BackgroundExpanded = UIImage.FromFile("Body/table-section-header-expand.png");
		UIImage BackgroundCollapsed = UIImage.FromFile("Body/table-section-header-collapse.png");
		UIImage ArrowExpanded = UIImage.FromFile("Body/accordionarrow-down.png");
		UIImage ArrowCollapsed = UIImage.FromFile("Body/accordionarrow-up.png");

		private static RectangleF Rect = new RectangleF(0f, 0f, 320f, 40f);

		public SPCollapsableSectionHeader(string title) : this(title, false) { }

		public SPCollapsableSectionHeader(string title, bool isExpanded) : base(Rect)
		{
			CanBeExpanded = isExpanded;
			IsExpanded = isExpanded;

			BackgroundColor = UIColor.GroupTableViewBackgroundColor.ColorWithAlpha(0.95f);

			Button = new UIButton(UIButtonType.Custom);
			Button.SetImage((IsExpanded ? BackgroundExpanded : BackgroundCollapsed), UIControlState.Normal);
			Button.Frame = new RectangleF(0f, 0f, 320f, 40f);
			Button.TouchUpInside += (sender, e) => 
			{
				Expanded = !Expanded;

				if( ButtonAction != null )
				{
					ButtonAction();
				}
			};
			AddSubview(Button);

			TitleLabel = new UILabel(new RectangleF(20f, 15f, 260f, 20f));
			TitleLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);
			TitleLabel.Text = title;
			TitleLabel.TextColor = UIColor.White;
			TitleLabel.TextAlignment = UITextAlignment.Left;
			AddSubview(TitleLabel);

			IconView = new UIImageView(new RectangleF(285f, 15f, 20f, 20f));
			IconView.Image = (IsExpanded ? ArrowExpanded : ArrowCollapsed);
			AddSubview(IconView);
		}

		public void SetTitle(string title)
		{
			TitleLabel.Text = title;
		}
	}
}

