﻿using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public class SPNextPrevButton : UIView
	{
		public UIButton Button { get; private set; }
		public UILabel TitleLabel { get; private set; }
		public UIImageView IconView { get; private set; }

		private static SizeF Size = new SizeF(80f, 30f);

		public SPNextPrevButton(PointF location, bool isNext) : base(new RectangleF(location, Size))
		{
			Button = new UIButton(UIButtonType.Custom);
			Button.SetImage(UIImage.FromFile("Body/next-prev.png").CreateResizableImage(new UIEdgeInsets(15f, 15f, 15f, 15f)), UIControlState.Normal);
			Button.Frame = new RectangleF(new PointF(), Size);
			AddSubview(Button);

			TitleLabel = new UILabel(new RectangleF(isNext ? 0f : 30f, 0f, 50f, 30f));
			TitleLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
			TitleLabel.Text = isNext ? "Next" : "Prev";
			TitleLabel.TextColor = UIColor.White;
			TitleLabel.TextAlignment = UITextAlignment.Center;
			AddSubview(TitleLabel);

			UIImage Arrow = UIImage.FromFile("Body/parcellistarrow-collected.png");

			IconView = new UIImageView(new RectangleF(isNext ? 55f : 5f, 5f, 20f, 20f));
			IconView.Image = UIImage.FromImage(Arrow.CGImage, 1f, isNext ? UIImageOrientation.Up : UIImageOrientation.UpMirrored);
			AddSubview(IconView);
		}
	}
}

