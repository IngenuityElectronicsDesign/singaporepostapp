﻿using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public class SPButton : UIView
	{
		public UIButton Button { get; private set; }
		public UILabel TitleLabel { get; private set; }
		public UIImageView IconView { get; private set; }
		private UIActivityIndicatorView ActivityView;

		public bool IsActive
		{
			get	{ return ActivityView.IsAnimating; }
			set 
			{
				if( value )
				{
					IconView.Hidden = true;
					Button.Enabled = false;
					ActivityView.StartAnimating();
				}
				else
				{
					IconView.Hidden = false;
					Button.Enabled = true;
					ActivityView.StopAnimating();
				}
			}
		}

		private static SizeF Size = new SizeF(280f, 40f);

		public SPButton(PointF location, UIImage backgroundImage, string title, UIImage iconView, bool activity) : base(new RectangleF(location, Size))
		{
			Button = new UIButton(UIButtonType.Custom);
			Button.SetImage(backgroundImage, UIControlState.Normal);
			Button.SetImage(backgroundImage, UIControlState.Disabled);
			Button.Frame = new RectangleF(new PointF(), Size);
			AddSubview(Button);

			TitleLabel = new UILabel(new RectangleF(20f, 5f, 220f, 30f));
			TitleLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
			TitleLabel.Text = title;
			TitleLabel.TextColor = UIColor.White;
			TitleLabel.TextAlignment = UITextAlignment.Left;
			TitleLabel.AdjustsFontSizeToFitWidth = true;
			AddSubview(TitleLabel);

			IconView = new UIImageView(new RectangleF(245f, 5f, 30f, 30f));
			IconView.Image = iconView;
			AddSubview(IconView);

			ActivityView = new UIActivityIndicatorView(new RectangleF(245f, 5f, 30f, 30f));
			ActivityView.HidesWhenStopped = true;
			AddSubview(ActivityView);
		}
	}
}

