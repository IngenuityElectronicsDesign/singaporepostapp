using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using POPStation.Core.iOS;
using Rivets;

namespace POPStationiOS
{
	public partial class SPFAQViewController : UIViewController
	{
		UIActivityIndicatorView LoadingIndicatorView;
		UIBarButtonItem BackButton;

		public SPFAQViewController(IntPtr handle) : base(handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			UITextAttributes attr = new UITextAttributes();
			attr.TextColor = SPShared.Instance.BlueTintColor;
			attr.Font = UIFont.FromName("AvenirNextCondensed-Regular", 18f);

			BackButton = new UIBarButtonItem("", UIBarButtonItemStyle.Plain, (sender, e) =>
				{
					ContentView.GoBack();
					//ContentView.LoadRequest(request);
				});
			BackButton.SetTitleTextAttributes(attr, UIControlState.Normal);
			NavigationItem.SetRightBarButtonItem(BackButton, false);

			LoadingIndicatorView = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray);
			LoadingIndicatorView.Frame = new RectangleF(120f, 120f, 80f, 80f);
			LoadingIndicatorView.HidesWhenStopped = true;
			LoadingIndicatorView.StartAnimating();
			View.AddSubview(LoadingIndicatorView);

			NSUrl url = new NSUrl(SPSharedData.PopstationURLMain + "faqs?mobile=true");
			NSUrlRequest request = new NSUrlRequest(url);
			ContentView.Alpha = 0f;
			ContentView.LoadRequest(request);
			ContentView.ShouldStartLoad = (w, urlRequest, navigationType) =>
			{
				CheckAppLinks(urlRequest.Url.AbsoluteString);
				return true;
			};
			ContentView.LoadStarted += (sender, e) => 
			{
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
				//UpdateBackButton();
			};
			ContentView.LoadFinished += (sender, e) => 
			{
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;

				if( LoadingIndicatorView.IsAnimating )
				{
					LoadingIndicatorView.StopAnimating();
					UIView.Animate(0.4f,  () =>
						{
							ContentView.Alpha = 1f;
						});
				}

				UpdateBackButton();
			};
			ContentView.LoadError += (sender, e) => 
			{
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
				UpdateBackButton();
			};

//			ContentView.ShouldStartLoad = (w, urlRequest, navigationType) =>
//			{
//				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
//
//				if( urlRequest.Url.ToString() != request.Url.ToString() )
//				{
//
//					NavigationItem.SetRightBarButtonItem(BackButton, true);
//				}
//				else
//				{
//					NavigationItem.SetRightBarButtonItem(null, true);
//				}
//
//
//
//				return true;
//			};
		}

		void UpdateBackButton()
		{
			BackButton.Title = (ContentView.CanGoBack ? "Back" : "");

			if( BackButton.Title == "Back" && ContentView.Request.Url.AbsoluteString.Contains(SPSharedData.PopstationURLMain + "faqs") )
			{
				BackButton.Title = "";
			}
		}

		public async void CheckAppLinks(string url)
		{
			if( url.StartsWith("http") && !url.StartsWith(SPSharedData.PopstationURLMain + "faqs"))
			{
				var result = await Rivets.AppLinks.Navigator.Navigate(url);

				if( result == NavigationResult.App )
				{
					ContentView.StopLoading();
				}
			}
			return;
		}
	}
}
