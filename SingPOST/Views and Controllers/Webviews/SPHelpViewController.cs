using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using POPStation.Core.iOS;
using Rivets;

namespace POPStationiOS
{
	public partial class SPHelpViewController : UIViewController
	{
		UIActivityIndicatorView LoadingIndicatorView;

		public SPHelpViewController(IntPtr handle) : base(handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			LoadingIndicatorView = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray);
			LoadingIndicatorView.Frame = new RectangleF(120f, 120f, 80f, 80f);
			LoadingIndicatorView.HidesWhenStopped = true;
			LoadingIndicatorView.StartAnimating();
			View.AddSubview(LoadingIndicatorView);

			NSUrl url = new NSUrl(SPSharedData.PopstationURLMain + "mobile-help?mobile=true");
			NSUrlRequest request = new NSUrlRequest(url);
			ContentView.Alpha = 0f;
			ContentView.LoadRequest(request);
			ContentView.LoadFinished += (sender, e) => 
			{
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;

				LoadingIndicatorView.StopAnimating();
				UIView.Animate(0.4f,  () =>
					{
						ContentView.Alpha = 1f;
					});
			};

			ContentView.ShouldStartLoad = (w, urlRequest, navigationType) =>
			{
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;

				CheckAppLinks(urlRequest.Url.AbsoluteString);

				if( urlRequest.Url.ToString() != request.Url.ToString() )
				{
					UITextAttributes attr = new UITextAttributes();
					attr.TextColor = SPShared.Instance.BlueTintColor;
					attr.Font = UIFont.FromName("AvenirNextCondensed-Regular", 18f);

					UIBarButtonItem item = new UIBarButtonItem("Back", UIBarButtonItemStyle.Plain, (sender, e) =>
						{
							ContentView.LoadRequest(request);
						});
					item.SetTitleTextAttributes(attr, UIControlState.Normal);
					NavigationItem.SetRightBarButtonItem(item, true);
				}
				else
				{
					NavigationItem.SetRightBarButtonItem(null, true);
				}

				return true;
			};
		}

		public async void CheckAppLinks(string url)
		{
			if( url.StartsWith("http") )
			{
				var result = await Rivets.AppLinks.Navigator.Navigate(url);

				if( result == NavigationResult.App )
				{
					ContentView.StopLoading();
				}
			}
			return;
		}
	}
}
