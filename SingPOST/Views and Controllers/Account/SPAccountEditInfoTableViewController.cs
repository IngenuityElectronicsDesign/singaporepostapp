using POPStation.Core.iOS;
using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.CoreFoundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MonoTouch.CoreText;

namespace POPStationiOS
{
	public partial class SPAccountEditInfoTableViewController : UITableViewController
	{
		private string[] Titles = { "First Name", "Last Name", "Phone Number", "Post Code", "Building Name", "Street Name", "Unit Number" };
		private SPTextFieldCell[] TextFieldCells = new SPTextFieldCell[7];
		private UserProfileData m_EditingData;
		private string PostCode;

		public SPAccountEditInfoTableViewController(IntPtr handle) : base(handle)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse( typeof(SPTextFieldCell), new NSString("TextFieldCell") );

			Title = "Edit Info";

			// Set the nav items of the new view
			UITextAttributes attr = new UITextAttributes();
			attr.TextColor = SPShared.Instance.BlueTintColor;
			attr.Font = UIFont.FromName("AvenirNextCondensed-Regular", 18f);

			UIBarButtonItem cancelButton = new UIBarButtonItem("Cancel", UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
				{
					for(int i = 0; i < TextFieldCells.Length; i++)
					{
						if(TextFieldCells[i] != null)
						{
							if(TextFieldCells[i].TextField.CanResignFirstResponder)
								TextFieldCells[i].TextField.ResignFirstResponder();
						}
					}
					NavigationController.PopViewControllerAnimated(true);
				});
			cancelButton.SetTitleTextAttributes(attr, UIControlState.Normal);
			NavigationItem.LeftBarButtonItem = cancelButton;

			UIBarButtonItem doneButton = new UIBarButtonItem("Done", UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
				{
					for(int i = 0; i < TextFieldCells.Length; i++)
					{
						if(TextFieldCells[i] != null)
						{
							if(TextFieldCells[i].TextField.CanResignFirstResponder)
								TextFieldCells[i].TextField.ResignFirstResponder();
						}
					}
					UpdateProfile();
				});
			doneButton.SetTitleTextAttributes(attr, UIControlState.Normal);
			NavigationItem.RightBarButtonItem = doneButton;
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);

			m_EditingData = null;
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 1;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			return 7;
		}

//		public override string TitleForFooter(UITableView tableView, int section)
//		{
//			return "All fields are mandatory";
//		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			SPTextFieldCell cell = (SPTextFieldCell)TableView.DequeueReusableCell(new NSString("TextFieldCell"), indexPath);
			TextFieldCells[indexPath.Row] = cell;

			cell.SelectionStyle = UITableViewCellSelectionStyle.None;

			if(m_EditingData == null)
			{
				m_EditingData = new UserProfileData (SPSharedData.Instance.UserProfile);
				PostCode = m_EditingData.PostCode;
			}

			Action shouldReturn = null;
			string currentData = "";
			EventHandler endedHandler = null;

			switch( indexPath.Row )
			{
			case 0:

				currentData = m_EditingData.FirstName;
				shouldReturn = () =>
				{
					TextFieldCells[1].TextField.BecomeFirstResponder();
				};
				endedHandler = (sender, e) => {
					m_EditingData.FirstName = TextFieldCells[0].TextField.Text;
				};
				break;
			case 1:
				currentData = m_EditingData.LastName;
				shouldReturn = () =>
				{
					TextFieldCells[2].TextField.BecomeFirstResponder();
				};
				endedHandler = (sender, e) => {
					m_EditingData.LastName = TextFieldCells[1].TextField.Text;
				};
				break;
			case 2:
				currentData = m_EditingData.ContactNumber;
				shouldReturn = () =>
				{
					TextFieldCells[3].TextField.BecomeFirstResponder();
				};
				endedHandler = (sender, e) => {
					m_EditingData.ContactNumber = TextFieldCells[2].TextField.Text;
				};
				break;
			case 3:
				currentData = m_EditingData.PostCode;
				shouldReturn = () =>
				{
					TextFieldCells[6].TextField.BecomeFirstResponder();
				};
				endedHandler = (sender, e) =>
				{
					if( !string.IsNullOrEmpty(TextFieldCells[3].TextField.Text) && TextFieldCells[3].TextField.Text != PostCode )
					{
						UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
						SPServerData.Instance.RESTSearchAddress( TextFieldCells[3].TextField.Text, (success, address) =>
							{
								InvokeOnMainThread( () =>
									{
										UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
										if( success && address != null )
										{
											PostCode = TextFieldCells[3].TextField.Text;

											TextFieldCells[4].TextField.Text = address.BuildingName;
											TextFieldCells[5].TextField.Text = address.StreetName;

											m_EditingData.PostCode = TextFieldCells[3].TextField.Text;
											m_EditingData.BuildingName = address.BuildingName;
											m_EditingData.StreetName = address.StreetName;
										}
										else
										{
											TextFieldCells[3].TextField.BecomeFirstResponder();
											UIAlertView alert = new UIAlertView("Error Retrieving Address", "Please try again, or contact support if the problem persists.", null, "OK");
											alert.Show();
										}
									});
							});
					}
				};
				break;
			case 4:
				currentData = m_EditingData.BuildingName;
				shouldReturn = () =>
				{
					TextFieldCells[6].TextField.BecomeFirstResponder();
				};
				endedHandler = (sender, e) => {
					m_EditingData.BuildingName = TextFieldCells[4].TextField.Text;
				};
				break;
			case 5:
				currentData = m_EditingData.StreetName;
				shouldReturn = () =>
				{
				};
				endedHandler = (sender, e) => {
					m_EditingData.StreetName = TextFieldCells[5].TextField.Text;
				};
				break;
			case 6:
				currentData = m_EditingData.UnitNumber;
				shouldReturn = () =>
				{
					UpdateProfile();
				};
				endedHandler = (sender, e) => {
					m_EditingData.UnitNumber = TextFieldCells[6].TextField.Text;
				};
				break;
			default:
				break;
			}

			if( indexPath.Row == 5 )
			{
				cell.Setup(Titles[indexPath.Row], 90f, currentData, shouldReturn, endedHandler, null, true);
			}
			else
			{
				cell.Setup(Titles[indexPath.Row], 90f, currentData, shouldReturn, endedHandler, null);
			}

			if(indexPath.Row == 0)
				cell.TextField.BecomeFirstResponder();

			if(indexPath.Row == 2 || indexPath.Row == 5 || indexPath.Row == 6)
				cell.TextField.KeyboardType = UIKeyboardType.NumbersAndPunctuation;
			cell.TextField.ReturnKeyType = (indexPath.Row < 6 ? UIReturnKeyType.Next : UIReturnKeyType.Done);


			return cell;
		}  

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			((SPTextFieldCell)tableView.CellAt(indexPath)).TextField.BecomeFirstResponder();
		}
		#endregion Table View Logic

		public void UpdateProfile()
		{
			//if( string.IsNullOrEmpty(TextFieldCells[0].TextField.Text) || string.IsNullOrEmpty(TextFieldCells[1].TextField.Text) || string.IsNullOrEmpty(TextFieldCells[2].TextField.Text) || string.IsNullOrEmpty(TextFieldCells[3].TextField.Text) || string.IsNullOrEmpty(TextFieldCells[4].TextField.Text) || string.IsNullOrEmpty(TextFieldCells[5].TextField.Text) || string.IsNullOrEmpty(TextFieldCells[6].TextField.Text) )
			if(!m_EditingData.ValidateData())
			{
				UIAlertView alert = new UIAlertView("Warning", "Some fields aren't filled correctly, please check them and try again.", null, "OK", null);
				alert.Show();
				return;
			}

			//SPServerData.Instance.ServerPushProfile( TextFieldCells[0].TextField.Text, TextFieldCells[1].TextField.Text, TextFieldCells[2].TextField.Text, TextFieldCells[3].TextField.Text, TextFieldCells[4].TextField.Text, TextFieldCells[5].TextField.Text, TextFieldCells[6].TextField.Text, (updateSuccess, message) =>
			SPServerData.Instance.ServerPushProfile(m_EditingData, (updateSuccess, message) =>
				{
					DispatchQueue.MainQueue.DispatchAsync( () =>
						{
							if( updateSuccess )
							{
								SPSharedData.Instance.UserProfile = m_EditingData;
								NavigationController.PopViewControllerAnimated(true);
							}
							else
							{
								UIAlertView alert = new UIAlertView("Error", message, null, "OK", null);
								alert.Show();
							}
						});
				});
		}
	}
}
