// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace POPStationiOS
{
	[Register ("SPAccountTableViewController")]
	partial class SPAccountTableViewController
	{
		[Outlet]
		MonoTouch.UIKit.UISwitch PushNotificationSwitch { get; set; }

		[Action ("PushNotificationSwitchValueChanged:")]
		partial void PushNotificationSwitchValueChanged (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (PushNotificationSwitch != null) {
				PushNotificationSwitch.Dispose ();
				PushNotificationSwitch = null;
			}
		}
	}
}
