﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public class SPTextFieldCell : UITableViewCell
	{
		public UILabel TitleLabel { get; private set; }
		public UITextField TextField { get; private set; }

		private UIImageView ConfirmationIconView;
		private UIImage SuccessImage;
		private UIImage FailImage;

		public bool HasSecondField { get; private set; }

		private EventHandler m_EndedEventHandler;

		public SPTextFieldCell(IntPtr handle) : base(handle)
		{
			ConfirmationIconView = new UIImageView(new RectangleF(280f, 7f, 30f, 30f));
			SuccessImage = UIImage.FromFile("Body/checked-fillgreen.png");
			FailImage = UIImage.FromFile("Body/fail-fillred.png");
			AddSubview(ConfirmationIconView);
		}

		public void Setup(string title, float titleWidth, string existingData, Action shouldReturnAction, EventHandler endedEventHandler, Action editingChangedAction, bool isStatic)
		{
			Setup(title, titleWidth, existingData, shouldReturnAction, endedEventHandler, editingChangedAction);
			TextField.Enabled = !isStatic;
		}

		public void Setup(string title, float titleWidth, string existingData, Action shouldReturnAction, EventHandler endedEventHandler, Action editingChangedAction)
		{
			if( TitleLabel == null )
			{
				TitleLabel = new UILabel (new RectangleF (10f, 9f, titleWidth, 25f));
				TitleLabel.Font = UIFont.FromName ("AvenirNextCondensed-Regular", 16f);
				AddSubview(TitleLabel);
			}
			TitleLabel.Text = title;

			if( TextField == null )
			{
				TextField = new UITextField(new RectangleF(titleWidth+20f, 10f, 320f-50f-20f-titleWidth, 25f));
				TextField.AutocapitalizationType = UITextAutocapitalizationType.None;
				TextField.AutocorrectionType = UITextAutocorrectionType.No;
				TextField.Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
				TextField.TextColor = SPShared.Instance.BlueTintColor;
				AddSubview(TextField);
			}

			TextField.Placeholder = title;
			TextField.Text = existingData;
			TextField.EditingChanged += (sender, e) =>
			{
				if(editingChangedAction != null)
				{
					editingChangedAction();
				}
			};
			TextField.ShouldReturn = (sender) =>
			{
				TextField.ResignFirstResponder();
				if(shouldReturnAction != null)
				{
					shouldReturnAction();
				}
				return true;
			};
			if(endedEventHandler != null)
			{
				TextField.Ended += endedEventHandler;
				m_EndedEventHandler = endedEventHandler;
			}
		}

		public override void PrepareForReuse()
		{
			base.PrepareForReuse();

			if( m_EndedEventHandler != null )
			{
				TextField.Ended -= m_EndedEventHandler;
				TextField.ShouldReturn = null;
			}
		}

		public void SetConfirmation(bool success)
		{
			ConfirmationIconView.Image = success ? SuccessImage : FailImage;
		}

		public void ClearConfirmation()
		{
			ConfirmationIconView.Image = null;
		}

		public void RemoveEndedEventHandler()
		{
			if( m_EndedEventHandler != null )
			{
				TextField.Ended -= m_EndedEventHandler;
				TextField.ShouldReturn = null;
			}
		}
	}
}

