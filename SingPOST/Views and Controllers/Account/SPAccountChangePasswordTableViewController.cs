using POPStation.Core.iOS;
using System;
using System.Drawing;
using System.Timers;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.CoreFoundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MonoTouch.CoreText;

namespace POPStationiOS
{
	public partial class SPAccountChangePasswordTableViewController : UITableViewController
	{
		private string[] Titles = { "Old Password", "New Password", "Confirm New Password" };
		private SPTextFieldCell[] PasswordCells = new SPTextFieldCell[3];

		public SPAccountChangePasswordTableViewController(IntPtr handle) : base(handle)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse( typeof(SPTextFieldCell), new NSString("TextFieldCell") );

			Title = "Change Password";

			// Set the nav items of the new view
			UITextAttributes attr = new UITextAttributes();
			attr.TextColor = SPShared.Instance.BlueTintColor;
			attr.Font = UIFont.FromName("AvenirNextCondensed-Regular", 18f);

			UIBarButtonItem cancelButton = new UIBarButtonItem("Cancel", UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
				{
					foreach( SPTextFieldCell cell in PasswordCells )
					{
						cell.RemoveEndedEventHandler();
					}
					NavigationController.PopViewControllerAnimated(true);
				});
			cancelButton.SetTitleTextAttributes(attr, UIControlState.Normal);
			NavigationItem.LeftBarButtonItem = cancelButton;

			UIBarButtonItem doneButton = new UIBarButtonItem("Done", UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
				{
					ResetPassword();
				});
			doneButton.SetTitleTextAttributes(attr, UIControlState.Normal);
			NavigationItem.RightBarButtonItem = doneButton;
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 1;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			return 3;
		}

//		public override string TitleForFooter (UITableView tableView, int section)
//		{
//			return "All fields are mandatory";
//		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			SPTextFieldCell cell = (SPTextFieldCell)TableView.DequeueReusableCell(new NSString("TextFieldCell"), indexPath);
			PasswordCells[indexPath.Row] = cell;

			cell.SelectionStyle = UITableViewCellSelectionStyle.None;

			Action editingAction = () =>
			{
				switch( indexPath.Row )
				{
					case 0:
					{
						PasswordCells[0].ClearConfirmation();
						break;
					}
					case 1:
					{
						if( string.IsNullOrEmpty(PasswordCells[1].TextField.Text) )
						{
							PasswordCells[1].SetConfirmation( false );
						}
						else if( PasswordCells[1].TextField.Text.Length >= 7 )
						{
							PasswordCells[1].SetConfirmation( true );
						}
						else
						{
							PasswordCells[1].ClearConfirmation();
						}
						break;
					}
					case 2:
					{
						if( string.IsNullOrEmpty(PasswordCells[2].TextField.Text) )
						{
							PasswordCells[2].SetConfirmation( false );
						}
						else if( PasswordCells[1].TextField.Text == PasswordCells[2].TextField.Text )
						{
							PasswordCells[2].SetConfirmation( true );
						}
						else
						{
							PasswordCells[2].ClearConfirmation();
						}
						break;
					}
					default:
					{
						break;
					}
				}
			};

			Action returnAction = () =>
			{
				switch( indexPath.Row )
				{
					case 0:
					{
						PasswordCells[1].TextField.BecomeFirstResponder();
						break;
					}
					case 1:
					{
						PasswordCells[2].TextField.BecomeFirstResponder();
						break;
					}
					case 2:
					{
						ResetPassword();
						break;
					}
					default:
					{
						break;
					}
				}
			};

			// TODO : set up the delegates here properly
			cell.Setup(Titles[indexPath.Row], 130f, "", returnAction, null, editingAction);

			cell.TextField.SecureTextEntry = true;
			cell.TextField.ReturnKeyType = (indexPath.Row < 2 ? UIReturnKeyType.Next : UIReturnKeyType.Done);

			if( indexPath.Row == 0 )
			{
				cell.TextField.BecomeFirstResponder();
			}

			return cell;
		}  

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			((SPTextFieldCell)tableView.CellAt(indexPath)).TextField.BecomeFirstResponder();
		}
		#endregion Table View Logic

		public void ResetPassword()
		{
			if( string.IsNullOrEmpty(PasswordCells[0].TextField.Text) || string.IsNullOrEmpty(PasswordCells[1].TextField.Text) || string.IsNullOrEmpty(PasswordCells[2].TextField.Text) )
			{
				UIAlertView alert = new UIAlertView("Entry Error", "Some fields aren't filled correctly, please check them and try again.", null, "OK", null);
				alert.Show();
				return;
			}

			SPServerData.Instance.ServerResetPassword( PasswordCells[0].TextField.Text, PasswordCells[1].TextField.Text, PasswordCells[2].TextField.Text, (updateSuccess, message) =>
				{
					DispatchQueue.MainQueue.DispatchAsync( () =>
						{
							if( updateSuccess )
							{
								var timer = new Timer(500);
								timer.Elapsed += (s, ev) => 
								{
									InvokeOnMainThread( () =>
										{
											NavigationController.PopViewControllerAnimated(true);
										});
									timer.Stop();
									timer = null;
								};
								timer.Start();

								PasswordCells[0].SetConfirmation(true);
							}
							else
							{
								if( message == "Incorrect password" )
								{
									PasswordCells[0].SetConfirmation(false);
								}

								UIAlertView alert = new UIAlertView("Password Reset Failed", message, null, "OK", null);
								alert.Show();
							}
						});
				});
		}
	}
}
