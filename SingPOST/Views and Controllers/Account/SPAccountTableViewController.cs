using POPStation.Core.iOS;
using System;
using System.Drawing;
using System.Timers;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.CoreFoundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MonoTouch.CoreText;

namespace POPStationiOS
{
	public partial class SPAccountTableViewController : UITableViewController
	{
		UIView HeaderView;
		UILabel TitleLabel;
		UILabel AccountLabel;
		UIView FooterView;
		SPButton LogoutButton;

		public SPAccountTableViewController(IntPtr handle) : base(handle)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			// Setup header and footer
			HeaderView = new UIView(new RectangleF(0f, 0f, 320f, 72f));
			HeaderView.ContentMode = UIViewContentMode.Top;

			TitleLabel = new UILabel(new RectangleF(10f, 12f, 300f, 20f));
			TitleLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 15f);
			TitleLabel.Text = "Your Account";
			TitleLabel.TextColor = UIColor.FromWhiteAlpha(0.1f, 1f);
			HeaderView.AddSubview(TitleLabel);

			AccountLabel = new UILabel(new RectangleF(10f, 30f, 300f, 24f));
			AccountLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
			AccountLabel.Text = SPSharedData.Instance.UserProfile.Email;
			AccountLabel.TextColor = SPShared.Instance.BlueTintColor;
			HeaderView.AddSubview(AccountLabel);

			FooterView = new UIView(new RectangleF(0f, 0f, 320f, 90f));
			FooterView.ContentMode = UIViewContentMode.Bottom;

			LogoutButton = new SPButton(new PointF(20f, 50f), UIImage.FromFile("Body/See-more.png"), "Log out", UIImage.FromFile("Body/parcellistarrow-collected.png"), false);
			LogoutButton.Button.TouchUpInside += (sender, e) =>
			{
				LogoutButton.IsActive = true;

				var timer = new Timer(500);
				timer.Elapsed += (s, ev) => 
				{
					SPServerData.Instance.ServerLogout( (logoutSuccess) =>
						{
							DispatchQueue.MainQueue.DispatchAsync( () =>
								{
									LogoutButton.IsActive = false;

									if( logoutSuccess )
									{
										SPShared.Instance.SetIsLoggedIn(false, true, null);
										SPServerData.Instance.ServerDeregisterPushNotification(SPSharedData.Instance.PushNotificationsTokenID, (success, message) =>
											{
												Console.WriteLine("Server Deregister Push : " + success + " " + message);
											});
									}
									else
									{
										LogoutButton.TitleLabel.Text = "Error: Couldn't Log Out";
									}
								});
						});
					timer.Stop();
					timer = null;
				};
				timer.Start();
			};
			FooterView.AddSubview(LogoutButton);

			// Setup UI elements
			PushNotificationSwitch.On = SPShared.Instance.PushNotificationsEnabled;
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			if( indexPath.Row < 2 )
			{
				// Instantiate the view from the Storyboard
				UIViewController toPush = (UIViewController)Storyboard.InstantiateViewController( (indexPath.Row == 0 ? "AccountEditInfoTVC" : "AccountChangePasswordTVC") );
				NavigationController.PushViewController(toPush, true);
			}
		}

		public override float GetHeightForHeader (UITableView tableView, int section)
		{
			return 72f;
		}

		public override UIView GetViewForHeader(UITableView tableView, int section)
		{
			return HeaderView;
		}

		public override float GetHeightForFooter (UITableView tableView, int section)
		{
			return 90f;
		}

		public override UIView GetViewForFooter (UITableView tableView, int section)
		{
			return FooterView;
		}
		#endregion Table View Logic

		#region UI Elements Logic
		partial void PushNotificationSwitchValueChanged(MonoTouch.Foundation.NSObject sender)
		{
			SPShared.Instance.PushNotificationsEnabled = ((UISwitch)sender).On;
		}
		#endregion UI Elements Logic
	}
}
