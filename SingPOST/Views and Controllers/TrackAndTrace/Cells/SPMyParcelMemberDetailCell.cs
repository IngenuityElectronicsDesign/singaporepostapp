﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public class SPMyParcelMemberDetailCell : UITableViewCell
	{
		public const float LabelWidth = 150f;

		public UILabel DateTimeLabel { get; private set; }
		public UILabel StatusLabel { get; private set; }

		public SPMyParcelMemberDetailCell(IntPtr handle) : base(handle)
		{
			BackgroundColor = UIColor.Clear;

			DateTimeLabel = new UILabel(new RectangleF(15f, 10f, 140, 15f));
			DateTimeLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 15f);
			DateTimeLabel.TextColor = UIColor.FromWhiteAlpha(0.1f, 1f);
			AddSubview(DateTimeLabel);

			StatusLabel = new UILabel(new RectangleF(160f, 5f, LabelWidth, 45f));
			StatusLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 15f);
			StatusLabel.TextColor = UIColor.FromWhiteAlpha(0.1f, 1f);
			StatusLabel.LineBreakMode = UILineBreakMode.WordWrap;
			StatusLabel.Lines = 0;
			AddSubview(StatusLabel);
		}

		public void Setup(string dateTime, string status)
		{
			DateTimeLabel.Text = dateTime;
			StatusLabel.Text = status;

			SizeF statusSize = SizeF.Empty;
			if( !string.IsNullOrEmpty(status) )
			{
				statusSize = new NSString(status).StringSize(StatusLabel.Font, new SizeF(LabelWidth, 9999f), StatusLabel.LineBreakMode);
			}
			StatusLabel.Frame = new RectangleF(StatusLabel.Frame.Location, statusSize);
		}
	}
}

