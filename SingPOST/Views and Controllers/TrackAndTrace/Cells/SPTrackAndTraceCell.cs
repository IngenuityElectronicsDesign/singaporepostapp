﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public class SPTrackAndTraceCell : UITableViewCell
	{
		public const float LabelBoarderWidth = 15f;
		public const float DateLabelWidth = 70f;
		public const float StatusLabelWidth = 150f;
		public const float LocationLabelWidth = 70f;

		public UILabel DateLabel { get; private set; }
		public UILabel StatusLabel { get; private set; }
		public UILabel LocationLabel { get; private set; }

		public SPTrackAndTraceCell(IntPtr handle) : base(handle)
		{
		}

		public void Setup(string dateTime, string status, string location)
		{
			BackgroundColor = UIColor.Clear;

			DateLabel = new UILabel(new RectangleF(LabelBoarderWidth, 10f, DateLabelWidth, 15f));
			DateLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 14f);
			DateLabel.Text = dateTime;
			DateLabel.TextColor = UIColor.FromWhiteAlpha(0.1f, 1f);
			AddSubview(DateLabel);

			StatusLabel = new UILabel(new RectangleF(LabelBoarderWidth+DateLabelWidth, 5f, StatusLabelWidth, 45f));
			StatusLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 14f);
			StatusLabel.Text = status;
			StatusLabel.TextColor = UIColor.FromWhiteAlpha(0.1f, 1f);
			StatusLabel.LineBreakMode = UILineBreakMode.WordWrap;
			StatusLabel.Lines = 0;
			AddSubview(StatusLabel);

			SizeF statusSize = new NSString(status).StringSize(StatusLabel.Font, new SizeF(StatusLabelWidth, 9999f), StatusLabel.LineBreakMode);
			StatusLabel.Frame = new RectangleF(StatusLabel.Frame.Location, statusSize);

			LocationLabel = new UILabel(new RectangleF(LabelBoarderWidth+DateLabelWidth+StatusLabelWidth, 5f, LocationLabelWidth, 45f));
			LocationLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 14f);
			LocationLabel.Text = location;
			LocationLabel.TextColor = UIColor.FromWhiteAlpha(0.1f, 1f);
			LocationLabel.LineBreakMode = UILineBreakMode.WordWrap;
			LocationLabel.Lines = 0;
			AddSubview(LocationLabel);

			SizeF locationSize = new NSString(location).StringSize(LocationLabel.Font, new SizeF(LocationLabelWidth, 9999f), LocationLabel.LineBreakMode);
			LocationLabel.Frame = new RectangleF(LocationLabel.Frame.Location, locationSize);
		}
	}
}

