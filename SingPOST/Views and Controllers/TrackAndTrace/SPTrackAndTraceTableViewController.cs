using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MonoTouch.CoreText;
using POPStation.Core.iOS;
using MonoTouch.CoreFoundation;

namespace POPStationiOS
{
	public partial class SPTrackAndTraceTableViewController : UITableViewController
	{
		private bool IsShowingErrorHeader;
		public const float HeaderViewHeight = 130f;
		public const float LabelViewHeight = 28f;
		public const float HeaderLabelsStart = 20f;
		public const float HeaderLabelValuesStart = 170f;

		UIScreenEdgePanGestureRecognizer BackGesture;

		private string _TrackingNumber;
		public string TrackingNumber
		{
			get
			{
				return _TrackingNumber;
			}
			set
			{
				_TrackingNumber = value;
				if (TrackingNumberLabel != null) {
					TrackingNumberLabel.Text = _TrackingNumber;
				}
			}

		}

		public string _OriginValue;
		public string OriginValue
		{
			get
			{
				return _OriginValue;
			}
			set
			{
				_OriginValue = value;
				if (OriginValueLabel != null) {
					OriginValueLabel.Text = _OriginValue;
				}
			}

		}

		public string _DestinationValue;
		public string DestinationValue
		{
			get
			{
				return _DestinationValue;
			}
			set
			{
				_DestinationValue = value;
				if (DestinationValueLabel != null) {
					DestinationValueLabel.Text = _DestinationValue;
				}
			}

		}
		private TrackAndTraceSearchData m_SearchData;

		UIView HeaderView;
		UIView LabelView;
		UILabel TrackingNumberLabel;
		UILabel TrackingNumberValueLabel;
		UILabel OriginLabel;
		UILabel OriginValueLabel;
		UILabel DestinationLabel;
		UILabel DestinationValueLabel;
		UILabel DateHeaderLabel;
		UILabel StatusHeaderLabel;
		UILabel LocationHeaderLabel;
		UIView HeaderLine;
		UIView LabelLine;
		UILabel ErrorLine1Label;
		UILabel ErrorLine2Label;

		public SPTrackAndTraceTableViewController (IntPtr handle) : base (handle)
		{
		}

		public void Setup(SPTrackAndTrackPopoutBar bar)
		{
			TrackingNumber = bar.SearchField.Text;
			OriginValue = "";
			DestinationValue = "";

			bar.SearchAction = () =>
			{
				TrackingNumber = bar.SearchField.Text;

				SetShowErrorHeader(TrackingNumber == "XZ");
			};

			SPServerData.Instance.RESTTrackAndTraceSearch (TrackingNumber, (success, response) => {
				if(success) {
					if(response != null && response.Data != null && response.Data.TrackingDetails != null)
					{
						m_SearchData = response.Data;
						DispatchQueue.MainQueue.DispatchAsync( () => {
							OriginValue = m_SearchData.OriginCountry;
							DestinationValue = m_SearchData.DestinationCountry;
							TableView.ReloadData();
						});
						return;
					}
				} 
				DispatchQueue.MainQueue.DispatchAsync( () => {
					SetShowErrorHeader(true);
				});
				Console.WriteLine("Error getting Track and Trace info");
			});
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse( typeof(SPTrackAndTraceCell), new NSString("TrackAndTraceCell") );
			TableView.BackgroundColor = UIColor.FromWhiteAlpha(0.91f, 1f);

			Title = "PARCEL INFORMATION";

			// Set the nav items of the new view
			UITextAttributes attr = new UITextAttributes();
			attr.TextColor = SPShared.Instance.BlueTintColor;
			attr.Font = UIFont.FromName("AvenirNextCondensed-Regular", 18f);

			UIBarButtonItem negativeSpacer = new UIBarButtonItem(UIBarButtonSystemItem.FixedSpace);
			negativeSpacer.Width = -12f; // -16 is the left edge

			UIBarButtonItem backButton = new UIBarButtonItem(UIImage.FromFile("Bar/arrowleft.png"), UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
				{
					NavigationController.PopViewControllerAnimated(true);
				});
			NavigationItem.LeftBarButtonItems = new UIBarButtonItem[] {negativeSpacer, backButton};

			UIBarButtonItem doneButton = new UIBarButtonItem("Done", UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
				{
					NavigationController.PopViewControllerAnimated(true);
				});
			doneButton.SetTitleTextAttributes(attr, UIControlState.Normal);
			NavigationItem.RightBarButtonItem = doneButton;

			// Setup header and footer
			HeaderView = new UIView(new RectangleF(0f, 0f, 320f, HeaderViewHeight));
			HeaderView.ContentMode = UIViewContentMode.Top;
			HeaderView.BackgroundColor = UIColor.FromWhiteAlpha(0.96f, 1f);

			TrackingNumberLabel = new UILabel(new RectangleF(HeaderLabelsStart, 15f, 140f, 20f));
			TrackingNumberLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 18f);
			TrackingNumberLabel.Text = "Tracking No.";
			TrackingNumberLabel.TextColor = UIColor.FromWhiteAlpha(0.67f, 1f);
			HeaderView.AddSubview(TrackingNumberLabel);

			TrackingNumberValueLabel = new UILabel(new RectangleF(HeaderLabelValuesStart, 15f, 140f, 20f));
			TrackingNumberValueLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 18f);
			TrackingNumberValueLabel.Text = TrackingNumber;
			TrackingNumberValueLabel.TextColor = SPShared.Instance.BlueTintColor;
			HeaderView.AddSubview(TrackingNumberValueLabel);

			OriginLabel = new UILabel(new RectangleF(HeaderLabelsStart, 40f, 140f, 20f));
			OriginLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 18f);
			OriginLabel.Text = "Origin";
			OriginLabel.TextColor = UIColor.FromWhiteAlpha(0.67f, 1f);
			HeaderView.AddSubview(OriginLabel);

			OriginValueLabel = new UILabel(new RectangleF(HeaderLabelValuesStart, 40f, 140f, 20f));
			OriginValueLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 18f);
			OriginValueLabel.Text = OriginValue;
			OriginValueLabel.TextColor = UIColor.FromWhiteAlpha(0.1f, 1f);
			HeaderView.AddSubview(OriginValueLabel);

			DestinationLabel = new UILabel(new RectangleF(HeaderLabelsStart, 65f, 140f, 20f));
			DestinationLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 18f);
			DestinationLabel.Text = "Destination";
			DestinationLabel.TextColor = UIColor.FromWhiteAlpha(0.67f, 1f);
			HeaderView.AddSubview(DestinationLabel);

			DestinationValueLabel = new UILabel(new RectangleF(HeaderLabelValuesStart, 65f, 140f, 20f));
			DestinationValueLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 18f);
			DestinationValueLabel.Text = DestinationValue;
			DestinationValueLabel.TextColor = UIColor.FromWhiteAlpha(0.1f, 1f);
			HeaderView.AddSubview(DestinationValueLabel);


			HeaderLine = new UIView(new RectangleF(5f, HeaderViewHeight, 310f, 1f));
			HeaderLine.BackgroundColor = UIColor.FromWhiteAlpha(0.67f, 1f);
			HeaderView.AddSubview(HeaderLine);

			// Label Sub View
			LabelView = new UIView(new RectangleF(0f, HeaderViewHeight-LabelViewHeight, 320f, LabelViewHeight));
			LabelView.ContentMode = UIViewContentMode.Top;
			LabelView.BackgroundColor = UIColor.FromWhiteAlpha(0.91f, 1f);
			HeaderView.AddSubview (LabelView);

			LabelLine = new UIView(new RectangleF(0f, 0f, 320f, 1f));
			LabelLine.BackgroundColor = UIColor.FromWhiteAlpha(0.67f, 1f);
			LabelView.AddSubview(LabelLine);

			DateHeaderLabel = new UILabel(new RectangleF(SPTrackAndTraceCell.LabelBoarderWidth, 4f, SPTrackAndTraceCell.DateLabelWidth, 20f));
			DateHeaderLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 14f);
			DateHeaderLabel.Text = "Date";
			DateHeaderLabel.TextColor = SPShared.Instance.BlueTintColor;
			LabelView.AddSubview(DateHeaderLabel);

			StatusHeaderLabel = new UILabel(new RectangleF(SPTrackAndTraceCell.LabelBoarderWidth + SPTrackAndTraceCell.DateLabelWidth, 4f, SPTrackAndTraceCell.StatusLabelWidth, 20f));
			StatusHeaderLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 14f);
			StatusHeaderLabel.Text = "Status";
			StatusHeaderLabel.TextColor = SPShared.Instance.BlueTintColor;
			LabelView.AddSubview(StatusHeaderLabel);

			LocationHeaderLabel = new UILabel(new RectangleF(SPTrackAndTraceCell.LabelBoarderWidth + SPTrackAndTraceCell.DateLabelWidth + SPTrackAndTraceCell.StatusLabelWidth, 4f, SPTrackAndTraceCell.LocationLabelWidth, 20f));
			LocationHeaderLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 14f);
			LocationHeaderLabel.Text = "Location";
			LocationHeaderLabel.TextColor = SPShared.Instance.BlueTintColor;
			LabelView.AddSubview(LocationHeaderLabel);

			// Error labels
			ErrorLine1Label = new UILabel(new RectangleF(15f, 15f, 290f, 20f));
			ErrorLine1Label.Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);
			ErrorLine1Label.Text = "The specified parcel could not be found.";
			ErrorLine1Label.TextColor = UIColor.FromWhiteAlpha(0.1f, 1f);
			HeaderView.AddSubview(ErrorLine1Label);

			ErrorLine2Label = new UILabel(new RectangleF(15f, 45f, 290f, 20f));
			ErrorLine2Label.Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);
			ErrorLine2Label.Text = "Please try again.";
			ErrorLine2Label.TextColor = UIColor.FromWhiteAlpha(0.1f, 1f);
			HeaderView.AddSubview(ErrorLine2Label);

			SetShowErrorHeader(false);
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			BackGesture = new UIScreenEdgePanGestureRecognizer();
			BackGesture.Edges = UIRectEdge.Left;
			BackGesture.AddTarget( () =>
				{
					NavigationController.PopViewControllerAnimated(true);
				});

			View.AddGestureRecognizer(BackGesture);
		}

		public override void ViewWillDisappear(bool animated)
		{
			View.RemoveGestureRecognizer(BackGesture);
		}

		public override void ViewDidLayoutSubviews()
		{
			base.ViewDidLayoutSubviews();

			//TableView.ContentSize = new SizeF(TableView.Frame.Width, TableView.Bounds.Height - TableView.ContentInset.Top - TableView.ContentInset.Bottom);
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 1;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			if( IsShowingErrorHeader )
			{
				return 0;
			}
			if (m_SearchData == null)
				return 0;
			return m_SearchData.TrackingDetails.Count;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			SPTrackAndTraceCell cell = (SPTrackAndTraceCell)TableView.DequeueReusableCell(new NSString("TrackAndTraceCell"), indexPath);
			cell.Setup (m_SearchData.TrackingDetails [indexPath.Row].Date.ToString ("dd-MM-yy"), m_SearchData.TrackingDetails[indexPath.Row].StatusString, m_SearchData.TrackingDetails[indexPath.Row].Location);
			return cell;
		}

		public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			SizeF statusLabelSize = new NSString(m_SearchData.TrackingDetails[indexPath.Row].StatusString).StringSize(UIFont.FromName("AvenirNextCondensed-Regular", 14f), new SizeF( SPTrackAndTraceCell.StatusLabelWidth, 9999f), UILineBreakMode.WordWrap);
			SizeF locationLabelSize = new NSString(m_SearchData.TrackingDetails[indexPath.Row].Location).StringSize(UIFont.FromName("AvenirNextCondensed-Regular", 14f), new SizeF( SPTrackAndTraceCell.LocationLabelWidth, 9999f), UILineBreakMode.WordWrap);
			return Math.Max(statusLabelSize.Height,locationLabelSize.Height) + 10f;
		}

		public override float GetHeightForHeader(UITableView tableView, int section)
		{
			return HeaderViewHeight;
		}

		public override UIView GetViewForHeader(UITableView tableView, int section)
		{
			return HeaderView;
		}
		#endregion Table View Logic

		public void SetShowErrorHeader(bool show)
		{
			TrackingNumberLabel.Hidden = show;
			TrackingNumberValueLabel.Hidden = show;
			DateHeaderLabel.Hidden = show;
			StatusHeaderLabel.Hidden = show;
			LocationHeaderLabel.Hidden = show;
			HeaderLine.Hidden = show;
			ErrorLine1Label.Hidden = !show;
			ErrorLine2Label.Hidden = !show;
			OriginLabel.Hidden = show;
			DestinationLabel.Hidden = show;

			IsShowingErrorHeader = show;

			TableView.ReloadData();
		}
	}
}
