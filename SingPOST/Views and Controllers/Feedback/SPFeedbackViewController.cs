using POPStation.Core.iOS;
using System;
using System.Timers;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.CoreFoundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public partial class SPFeedbackViewController : SPAnimatedStateViewController
	{
		private enum ScreenState
		{
			Initial,
			MessageHidden,
			ThankYou
		}
		private ScreenState CurrentState;

		// Top Views
		UIView TopBackgroundView;
		UILabel TopLabel;

		// Form Views
		UIView FormBackgroundView;
		UILabel FormTitleLabel;
		UILabel FormSubtitleLabel;
		UITextField NameField;
		UITextField NumberField;
		UITextField EmailField;
		UILabel CommentsTitleLabel;
		UIImageView CommentsBackgroundView;
		UITextView CommentsTextView;
		SPButton SubmitButton;

		// Thank You Views
		UILabel ThankYouLabel;

		Timer DismissMessageTimer;

		public SPFeedbackViewController(IntPtr handle) : base(handle)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			// Automatically adjust scroll view content insets to compensate for keyboard
			NSNotificationCenter.DefaultCenter.AddObserver("UIKeyboardWillShowNotification", (NSNotification notification) =>
				{
					UIEdgeInsets contentInsets = new UIEdgeInsets(ContentView.ContentInset.Top, ContentView.ContentInset.Left, 216f, ContentView.ContentInset.Right);
					ContentView.ContentInset = contentInsets;
					ContentView.ScrollIndicatorInsets = contentInsets;

					if( SPShared.Instance.IsLoggedIn )
					{
						ContentView.ScrollRectToVisible(new RectangleF(0f, 400f, 320f, 40f), true);
					}
				});

			NSNotificationCenter.DefaultCenter.AddObserver("UIKeyboardWillHideNotification", (NSNotification notification) =>
				{
					UIEdgeInsets contentInsets = new UIEdgeInsets(ContentView.ContentInset.Top, ContentView.ContentInset.Left, 0f, ContentView.ContentInset.Right);
					ContentView.ContentInset = contentInsets;
					ContentView.ScrollIndicatorInsets = contentInsets;
				});

			// Setup Current Screen State
			CurrentState = ScreenState.Initial;

			// Top Views
			TopBackgroundView = new UIView(new RectangleF(0f, 0f, 320f, 100f));
			TopBackgroundView.BackgroundColor = UIColor.GroupTableViewBackgroundColor;

			ViewList.Add(TopBackgroundView);
			PointsList.Add(new PointF[] { new PointF(0f, 0f), new PointF(0f, -100f), new PointF(0f, -100f) });
			VisibilityList.Add(new bool[] { true, false, false });

			TopLabel = new UILabel(new RectangleF(20f, 0f, 280f, 100f));
			TopLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);
			TopLabel.Text = "At SingPost, we are always looking to improve\nyour customer experience. If you have any\nfeedback or ideas for us, we would love to hear\nfrom you.";
			TopLabel.Lines = 4;
			TopBackgroundView.AddSubview(TopLabel);

			// Form Views
			FormBackgroundView = new UIView(new RectangleF(0f, 100f, 320f, 440f));

			ViewList.Add(FormBackgroundView);
			PointsList.Add(new PointF[] { new PointF(0f, 100f), new PointF(0f, 0f), new PointF(0f, -440f) });
			VisibilityList.Add(new bool[] { true, true, false });

			FormTitleLabel = new UILabel(new RectangleF(20f, 20f, 280f, 20f));
			FormTitleLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);
			FormTitleLabel.Text = "Personal information";
			FormTitleLabel.TextColor = SPShared.Instance.BlueTintColor;
			FormBackgroundView.AddSubview(FormTitleLabel);

			FormSubtitleLabel = new UILabel(new RectangleF(20f, 40f, 280f, 20f));
			FormSubtitleLabel.Font = UIFont.FromName("AvenirNextCondensed-Italic", 14f);
			FormSubtitleLabel.Text = ( SPShared.Instance.IsLoggedIn ? "Fields are auto filled with your account information" : "All fields are mandatory" );
			FormSubtitleLabel.TextColor = UIColor.FromWhiteAlpha(0.66f, 1f);
			FormBackgroundView.AddSubview(FormSubtitleLabel);

			NameField = new UITextField(new RectangleF(20f, 70f, 280f, 45f));
			NameField.Background = UIImage.FromFile("Body/Text-Field-Backgrounds.png");
			NameField.BorderStyle = UITextBorderStyle.Bezel;
			NameField.AutocapitalizationType = UITextAutocapitalizationType.Words;
			NameField.AutocorrectionType = UITextAutocorrectionType.No;
			NameField.ReturnKeyType = UIReturnKeyType.Next;
			NameField.Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
			NameField.TextColor = SPShared.Instance.BlueTintColor;
			NameField.Placeholder = "NAME";
			NameField.Text = ( SPShared.Instance.IsLoggedIn ? SPSharedData.Instance.UserProfile.FirstName + " " + SPSharedData.Instance.UserProfile.LastName : "" );
			NameField.Enabled = !SPShared.Instance.IsLoggedIn;
			NameField.ShouldReturn += (sender) =>
			{
				NameField.ResignFirstResponder();
				NumberField.BecomeFirstResponder();
				return true;
			};
			FormBackgroundView.AddSubview(NameField);

			NumberField = new UITextField(new RectangleF(20f, 120f, 280f, 45f));
			NumberField.Background = UIImage.FromFile("Body/Text-Field-Backgrounds.png");
			NumberField.BorderStyle = UITextBorderStyle.Bezel;
			NumberField.AutocapitalizationType = UITextAutocapitalizationType.None;
			NumberField.AutocorrectionType = UITextAutocorrectionType.No;
			NumberField.KeyboardType = UIKeyboardType.NumbersAndPunctuation;
			NumberField.ReturnKeyType = UIReturnKeyType.Next;
			NumberField.Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
			NumberField.TextColor = SPShared.Instance.BlueTintColor;
			NumberField.Placeholder = "CONTACT NUMBER";
			NumberField.Text = ( SPShared.Instance.IsLoggedIn ? SPSharedData.Instance.UserProfile.ContactNumber : "" );
			NumberField.Enabled = !SPShared.Instance.IsLoggedIn;
			NumberField.ShouldReturn += (sender) =>
			{
				NumberField.ResignFirstResponder();
				EmailField.BecomeFirstResponder();
				return true;
			};
			FormBackgroundView.AddSubview(NumberField);

			EmailField = new UITextField(new RectangleF(20f, 170f, 280f, 45f));
			EmailField.Background = UIImage.FromFile("Body/Text-Field-Backgrounds.png");
			EmailField.BorderStyle = UITextBorderStyle.Bezel;
			EmailField.AutocapitalizationType = UITextAutocapitalizationType.None;
			EmailField.AutocorrectionType = UITextAutocorrectionType.No;
			EmailField.KeyboardType = UIKeyboardType.EmailAddress;
			EmailField.ReturnKeyType = UIReturnKeyType.Next;
			EmailField.Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
			EmailField.TextColor = SPShared.Instance.BlueTintColor;
			EmailField.Placeholder = "EMAIL ADDRESS";
			EmailField.Text = ( SPShared.Instance.IsLoggedIn ? SPSharedData.Instance.UserProfile.Email : "" );
			EmailField.Enabled = !SPShared.Instance.IsLoggedIn;
			EmailField.ShouldReturn += (sender) =>
			{
				EmailField.ResignFirstResponder();
				CommentsTextView.BecomeFirstResponder();
				return true;
			};
			FormBackgroundView.AddSubview(EmailField);

			CommentsTitleLabel = new UILabel(new RectangleF(20f, 240f, 280f, 20f));
			CommentsTitleLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);
			CommentsTitleLabel.Text = "Your comments";
			CommentsTitleLabel.TextColor = SPShared.Instance.BlueTintColor;
			FormBackgroundView.AddSubview(CommentsTitleLabel);

			CommentsBackgroundView = new UIImageView(new RectangleF(20f, 270f, 280f, 100f));
			CommentsBackgroundView.Image = UIImage.FromFile("Body/Text-Field-Backgrounds.png").CreateResizableImage(new UIEdgeInsets(4f, 4f, 4f, 4f));
			FormBackgroundView.AddSubview(CommentsBackgroundView);

			CommentsTextView = new UITextView(new RectangleF(20f, 270f, 280f, 100f));
			CommentsTextView.BackgroundColor = UIColor.Clear;
			CommentsTextView.Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
			CommentsTextView.TextColor = SPShared.Instance.BlueTintColor;
			FormBackgroundView.AddSubview(CommentsTextView);

			SubmitButton = new SPButton(new PointF(20f, 390f), UIImage.FromFile("Body/Sign-In.png"), "Submit Feedback", UIImage.FromFile("Body/parcellistarrow-collected.png"), false);
			SubmitButton.Button.TouchUpInside += (sender, e) => // Button Pressed
			{
				if( string.IsNullOrEmpty(NameField.Text) || string.IsNullOrEmpty(NumberField.Text) || string.IsNullOrEmpty(EmailField.Text) || string.IsNullOrEmpty(CommentsTextView.Text) )
				{
					int errorCounter = 0;
					string errorText = "";

					if( string.IsNullOrEmpty(NameField.Text) )
					{
						errorCounter++;
						errorText = errorText.Insert(errorText.Length, "Name");
					}
					if( string.IsNullOrEmpty(NumberField.Text) )
					{
						errorCounter++;
						if( errorCounter > 1 )
						{
							errorText = errorText.Insert(errorText.Length, ", ");
						}
						errorText = errorText.Insert(errorText.Length, "Number");
					}
					if( string.IsNullOrEmpty(EmailField.Text) )
					{
						errorCounter++;
						if( errorCounter > 1 )
						{
							errorText = errorText.Insert(errorText.Length, ", ");
						}
						errorText = errorText.Insert(errorText.Length, "Email");
					}
					if( string.IsNullOrEmpty(CommentsTextView.Text) )
					{
						errorCounter++;
						if( errorCounter > 1 )
						{
							errorText = errorText.Insert(errorText.Length, ", ");
						}
						errorText = errorText.Insert(errorText.Length, "Comments");
					}

					if( errorCounter > 1 )
					{
						errorText = errorText.Insert(errorText.Length, " Fields ");
					}
					else
					{
						errorText = errorText.Insert(errorText.Length, " Field ");
					}

					SubmitButton.TitleLabel.Text = errorText + "not filled in correctly!";

					// Set timer to dismiss Top View
					var timer = new Timer(3000);
					timer.Elapsed += (s, ev) => 
					{
						CurrentState = ScreenState.MessageHidden;
						DispatchQueue.MainQueue.DispatchAsync( () =>
							{
								SubmitButton.TitleLabel.Text = "Submit Feedback";
							});
						timer.Stop();
						timer = null;
					};
					timer.Start();

					return;
				}

				if( DismissMessageTimer != null )
				{
					DismissMessageTimer.Stop();
					DismissMessageTimer = null;
				}

				SubmitButton.IsActive = true;
				CommentsTextView.ResignFirstResponder();

				SPServerData.Instance.ServerPushFeedback( NameField.Text, EmailField.Text, NumberField.Text, CommentsTextView.Text, (feedbackSuccess, message) =>
					{
						DispatchQueue.MainQueue.DispatchAsync( () =>
							{
								if( feedbackSuccess )
								{
									CurrentState = ScreenState.ThankYou;
									LayoutContentSubviews(CurrentState, true, null);
								}
								else
								{
									SubmitButton.Button.SetTitle(message, UIControlState.Normal);
								}
								SubmitButton.IsActive = false;
							});
					});
			};
			FormBackgroundView.AddSubview(SubmitButton);

			// Thank You Views
			ThankYouLabel = new UILabel(new RectangleF(20f, 560f, 280f, 100f));
			ThankYouLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);
			ThankYouLabel.Text = "Thank you for submitting your feedback.\n\nOur customer service officer may contact you for further details as necessary.";
			//ThankYouLabel.TextAlignment = UITextAlignment.Center;
			ThankYouLabel.Lines = 0;
			TopBackgroundView.AddSubview(ThankYouLabel);

			ViewList.Add(ThankYouLabel);
			PointsList.Add(new PointF[] { new PointF(20f, 560f), new PointF(20f, 560f), new PointF(20f, 20f) });
			VisibilityList.Add(new bool[] { false, false, true });

			ContentView.AddSubviews(ViewList.ToArray());

//			if( SPShared.Instance.IsLoggedIn )
//			{
//				CommentsTextView.BecomeFirstResponder();
//			}
//			else
//			{
//				NameField.BecomeFirstResponder();
//			}

			LayoutContentSubviews(CurrentState, false, null);

			// Set timer to dismiss Top View
			DismissMessageTimer = new Timer(6000);
			DismissMessageTimer.Elapsed += (sender, e) => 
			{
				CurrentState = ScreenState.MessageHidden;
				DispatchQueue.MainQueue.DispatchAsync( () =>
					{
						LayoutContentSubviews(CurrentState, true, null);
					});
				DismissMessageTimer.Stop();
				DismissMessageTimer = null;
			};
			DismissMessageTimer.Start();
		}

		public override void ViewDidLayoutSubviews()
		{
			base.ViewDidLayoutSubviews ();

			float height = 0f;

			switch( CurrentState )
			{
			case ScreenState.Initial:
				height = 540f;
				break;
			case ScreenState.MessageHidden:
				height = 440f;
				break;
			case ScreenState.ThankYou:
				height = 60f;
				break;
			}

			ContentView.ContentSize = new SizeF(ContentView.Frame.Width, height);
		}
		#endregion View Controller Logic

		#region AnimatedState Logic
		private void LayoutContentSubviews(ScreenState state, bool animated, Action completion)
		{
			base.LayoutContentSubviews((int)state, animated, completion);

//			switch( CurrentState )
//			{
//			case ScreenState.Initial:
//				{
//					TopIconView.Image = UIImage.FromFile("Body/icon-parcel-home-grey.png");
//					SignInButton.Button.SetImage(UIImage.FromFile("Body/Sign-In.png"), UIControlState.Normal);
//					SignInButton.IconView.Image = UIImage.FromImage(UIImage.FromFile("Body/parcellistarrow-collected.png").CGImage, 1f, UIImageOrientation.Right);
//					SignInButton.TitleLabel.Text = "Sign in";
//					break;
//				}
//			case ScreenState.SignInFields:
//				{
//					TopIconView.Image = UIImage.FromFile("Body/icon-parcel-home-grey.png");
//					SignInButton.Button.SetImage(UIImage.FromFile("Body/Sign-In.png"), UIControlState.Normal);
//					SignInButton.IconView.Image = UIImage.FromFile("Body/parcellistarrow-collected.png");
//					SignInButton.TitleLabel.Text = "Sign in";
//					break;
//				}
//			case ScreenState.FirstTimeFields:
//				{
//					TopIconView.Image = UIImage.FromFile("Body/icon-parcel-home-red.png");
//					SignInButton.Button.SetImage(UIImage.FromFile("Body/Welcome-First-Time-User-Buttons.png"), UIControlState.Normal);
//					SignInButton.TitleLabel.Text = "Welcome, first time sign in";
//					SignInButton.IconView.Image = UIImage.FromFile("Body/checked-linewhite.png");
//
//					TrackAndTracePopoutBar.Hide(true, () =>
//						{
//							((SPNavBar)TrackAndTracePopoutBar.ParentBar).SetBorderLineShouldHide(false, false);
//							TrackAndTraceButton.Image = UIImage.FromFile("Bar/trackntrace.png");
//						});
//
//					break;
//				}
//			}

			ViewDidLayoutSubviews();
		}
		#endregion AnimatedState Logic
	}
}
