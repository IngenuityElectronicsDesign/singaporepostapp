using POPStation.Core.iOS;
using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreAnimation;
using MonoTouch.CoreFoundation;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public partial class SPSlideMenuController : UIViewController
	{
		public enum MenuStates
		{
			Closed,
			Open,
			OpenFully,

			Count
		}

		public MenuStates MenuState;

		private const float MenuRevealWidth = 267f;
		private const float ScreenEdgeThreshold = 24f;
		private const float PositionValidityEpsilon = 2f;
		private float MenuEdgeOffset = 20f;

		public UIViewController BackgroundViewController { get; set; }
		public UIViewController ActiveViewController { get; set; }

		UIView ActiveView;
		UIButton ActiveViewCloseButton;

		private bool ActiveViewSlideOffAnimationEnabled;
		private bool ShouldAlignStatusBarToPaneView;

		public SPSlideMenuController(IntPtr handle) : base(handle)
		{
			ActiveViewSlideOffAnimationEnabled = true;
			ShouldAlignStatusBarToPaneView = true;

			ActiveView = new UIView();
			ActiveView.AutoresizingMask = (UIViewAutoresizing.FlexibleWidth|UIViewAutoresizing.FlexibleHeight);
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			UIApplication.SharedApplication.BeginIgnoringInteractionEvents();

			ActiveView.Frame = View.Bounds;
			View.AddSubview(ActiveView);

			ActiveViewCloseButton = new UIButton(UIButtonType.Custom);
			ActiveViewCloseButton.Frame = new RectangleF(MenuRevealWidth, ActiveView.Frame.Y, ActiveView.Frame.Width-MenuRevealWidth, ActiveView.Frame.Height);
			ActiveViewCloseButton.TouchUpInside += (sender, e) => 
			{
				SPShared.Instance.NavMenuTableViewController.ShouldTransition();
			};
			ActiveViewCloseButton.Hidden = true;
			View.AddSubview(ActiveViewCloseButton);

			BackgroundViewController = (UIViewController)Storyboard.InstantiateViewController("NavMenuTVC");
			((SPNavMenuTableViewController)BackgroundViewController).Setup(this);

			AddViewController(BackgroundViewController, this.View);

			View.BringSubviewToFront(ActiveView);
			View.BringSubviewToFront(ActiveViewCloseButton);

			// Splash / Loading Screen - Might actually have to load some stuff here
			UIImageView SplashScreen = new UIImageView(UIImage.FromFile("Default-568h@2x.png"));
			View.AddSubview(SplashScreen);

			Action completion = () =>
			{
				UIView.Animate(0.25f, () => // Anims
					{
						SplashScreen.Alpha = 0f;
					}, () => // Completion
					{
						SplashScreen.RemoveFromSuperview();
						SplashScreen = null;

						UIApplication.SharedApplication.EndIgnoringInteractionEvents();
					});
			};

			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;

			SPServerData.Instance.ServerGetProfile( (profileSuccess, isFirstTime) =>
				{
					InvokeOnMainThread( () =>
						{
							if( profileSuccess && !isFirstTime )
							{
								SPShared.Instance.SetIsLoggedIn(true, false, completion);
								if( UIDevice.CurrentDevice.CheckSystemVersion(8,0) )
								{
									UIApplication.SharedApplication.RegisterUserNotificationSettings(UIUserNotificationSettings.GetSettingsForTypes(UIUserNotificationType.Alert|UIUserNotificationType.Badge, null));
									UIApplication.SharedApplication.RegisterForRemoteNotifications();
								}
								else
								{
									UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(UIRemoteNotificationType.Alert|UIRemoteNotificationType.Badge);
								}
							}
							else
							{
								SPShared.Instance.SetIsLoggedIn(false, false, completion);
							}

							UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
						});
				});
		}

		public override UIViewController ChildViewControllerForStatusBarStyle()
		{
			return ActiveViewController;
		}

		public override UIViewController ChildViewControllerForStatusBarHidden()
		{
			return ActiveViewController;
		}
		#endregion View Controller Logic

		#region View Controller Containment Logic
		void AddViewController(UIViewController newVC, UIView containerView)
		{
			newVC.BeginAppearanceTransition(true, false);
			newVC.View.Frame = containerView.Bounds;
			this.AddChildViewController(newVC);
			containerView.AddSubview(newVC.View);
			newVC.DidMoveToParentViewController(this);
			newVC.EndAppearanceTransition();
		}
		#endregion View Controller Containment Logic

		#region Active View Controller Logic
		public void SetActiveViewController(UIViewController activeViewController, bool animated, Action completion)
		{
			// If null is passed, close the menu
			if( activeViewController == null )
			{
				SetMenuShowing(MenuStates.Closed, animated, true, completion);
				return;
			}
				
			{
				if( ActiveViewController == null )
				{
					ActiveViewController = new UIViewController();
				}
				ActiveViewController.WillMoveToParentViewController(null);
				ActiveViewController.BeginAppearanceTransition(false, animated);

				Action transitionToNewActiveViewController = ()=> 
				{
					activeViewController.WillMoveToParentViewController(this);
					ActiveViewController.View.RemoveFromSuperview();
					ActiveViewController.RemoveFromParentViewController();
					ActiveViewController.DidMoveToParentViewController(null);
					ActiveViewController.EndAppearanceTransition();
					AddChildViewController(activeViewController);
					activeViewController.View.Frame = ActiveView.Bounds;
					activeViewController.BeginAppearanceTransition(true, animated);
					ActiveView.AddSubview(activeViewController.View);
					ActiveViewController = activeViewController;
					// Force redraw for smoother anim
					ActiveView.SetNeedsDisplay();
					CATransaction.Flush();
					SetNeedsStatusBarAppearanceUpdate();
					// After drawing has finished, set new active view controller view and close
					DispatchGroup.Create().DispatchAsync(DispatchQueue.MainQueue, ()=>
						{
							//WeakReference<SPSlideMenuController> weakSelf = new WeakReference<SPSlideMenuController>(this);
							ActiveViewController = activeViewController;
							SetMenuShowing(MenuStates.Closed, animated, true, ()=>
								{
									activeViewController.DidMoveToParentViewController(this);
									activeViewController.EndAppearanceTransition();
									if( completion != null )
									{
										completion();
									}
								});
						});
				};
				if( ActiveViewSlideOffAnimationEnabled )
				{
					SetMenuShowing(MenuStates.OpenFully, animated, false, transitionToNewActiveViewController);
				}
				else
				{
					transitionToNewActiveViewController();
				}
			}

		}

		// Active View States
		public void SetMenuShowing(MenuStates menuState)
		{
			if( MenuState != menuState )
			{
				MenuState = menuState;

				// Maybe inform objects?
			}

			if( menuState == MenuStates.Open )
			{
				ActiveViewCloseButton.Hidden = false;
			}
			else
			{
				ActiveViewCloseButton.Hidden = true;
			}

			// Update Active Frame
			ActiveView.Frame = new RectangleF(OriginForMenuState(menuState), ActiveViewController.View.Frame.Size);
		}

		public void SetMenuShowing(MenuStates menuState, bool animated, bool slowAnim /*allowsUserInterruption*/, Action completion)
		{
			// If the menu is showing correctly, return
			MenuStates currentMenuState;
			if( MenuIsInValidPosition(out currentMenuState) && currentMenuState == menuState)
			{
				if( completion != null )
				{
					completion();
				}
				return;
			}

			if( animated )
			{
				UIView.AnimateNotify(slowAnim ? 0.4f : 0.3f, 0f, 0.9f, 5f, UIViewAnimationOptions.AllowUserInteraction, () => 
					{
						ActiveView.Frame = new RectangleF(OriginForMenuState(menuState), ActiveView.Frame.Size);
						ActiveViewFrameDidUpdate();
					}, (finished) =>
					{
						SetMenuShowing(menuState);
						if( completion != null )
						{
							completion();
						}
						ActiveViewFrameDidUpdate();
					});
			}
			else
			{
				ActiveView.Frame = new RectangleF(OriginForMenuState(menuState), ActiveView.Frame.Size);
				//ActiveViewFrameDidUpdate();
				SetMenuShowing(menuState);
				if( completion != null )
				{
					completion();
				}
			}
		}

		bool MenuIsInValidPosition(out MenuStates menuState)
		{
			menuState = MenuStates.Count;
			bool validPosition = false;

			for( int state = (int)MenuStates.Closed; state <= (int)MenuStates.OpenFully; state++ )
			{
				PointF menuStateOrigin = OriginForMenuState((MenuStates)state);
				PointF currentOrigin = new PointF(ActiveView.Frame.X, ActiveView.Frame.Y);
				float epsilon = PositionValidityEpsilon;

				if( (Math.Abs(menuStateOrigin.X - currentOrigin.X) < epsilon) && (Math.Abs(menuStateOrigin.Y - currentOrigin.Y) < epsilon) )
				{
					validPosition = true;
					menuState = (MenuStates)state;
					break;
				}
			}

			return validPosition;
		}

		// Return the point the frame should be in a given state
		PointF OriginForMenuState(MenuStates menuState)
		{
			PointF origin = new PointF();

			if( menuState == MenuStates.Open )
			{
				origin.X = MenuRevealWidth;
			}
			else if( menuState == MenuStates.OpenFully )
			{
				origin.X = ActiveView.Frame.Width + MenuEdgeOffset;
			}

			return origin;
		}

		// Update Frame
		void ActiveViewFrameDidUpdate()
		{
			if( ShouldAlignStatusBarToPaneView )
			{
				UIView statusBar = (UIView)UIApplication.SharedApplication.ValueForKey(new NSString("_statusBarWindow"));
				statusBar.Transform = CGAffineTransform.MakeTranslation(ActiveView.Frame.X, ActiveView.Frame.Y);
			}
		}
		#endregion Active View Controller Logic
	}
}
