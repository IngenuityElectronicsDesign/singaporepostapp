﻿using System;
using System.Timers;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.CoreFoundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MonoTouch.CoreText;

namespace POPStationiOS
{
	public partial class SPAnimatedStateViewController : UIViewController
	{
		// Form View Lists
		protected List<UIView> ViewList;
		protected List<PointF[]> PointsList;
		protected List<bool[]> VisibilityList;

		public SPAnimatedStateViewController(IntPtr handle) : base(handle)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			// Form View Lists
			ViewList = new List<UIView>();
			PointsList = new List<PointF[]>();
			VisibilityList = new List<bool[]>();
		}
		#endregion View Controller Logic

		protected void LayoutContentSubviews(int stateIndex, bool animated, Action completion)
		{
			// Apply batched changes
			if( animated )
			{
				UIView.Animate(0.4f, () => // Anims
					{
						for( int i = 0; i < ViewList.Count; i++ )
						{
							UIView view = ViewList[i];

							view.Frame = new RectangleF( PointsList[i][stateIndex], view.Frame.Size );
							view.Alpha = VisibilityList[i][stateIndex] ? 1f : 0f;
						}
					}, () => // Completion
					{
						if( completion != null )
						{
							completion();
						}
					});
			}
			else
			{
				for( int i = 0; i < ViewList.Count; i++ )
				{
					UIView view = ViewList[i];

					view.Frame = new RectangleF( PointsList[i][stateIndex], view.Frame.Size );
					view.Alpha = VisibilityList[i][stateIndex] ? 1f : 0f;
				}
				if( completion != null )
				{
					completion();
				}
			}
		}
	}
}


