﻿using POPStation.Core.iOS;
using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public class SPEMerchantCell : UITableViewCell
	{
		SPEMerchantInfoView LeftView;
		SPEMerchantInfoView RightView;

		public SPEMerchantCell(IntPtr handle) : base (handle)
		{
			SelectionStyle = UITableViewCellSelectionStyle.None;
			BackgroundColor = UIColor.Clear;
		}

		public void Setup(EMerchantData data, Action buttonAction)
		{
			Setup(data, null, buttonAction, null);
		}


		public void Setup(EMerchantData leftData, EMerchantData rightData, Action leftButtonAction, Action rightButtonAction)
		{
			LeftView = new SPEMerchantInfoView(new PointF(10f, 10f), leftData);
			LeftView.ButtonAction = () =>
			{
				if( leftButtonAction != null )
				{
					leftButtonAction();
				}
			};
			AddSubview(LeftView);

			if( rightData != null )
			{
				RightView = new SPEMerchantInfoView(new PointF(165f, 10f), rightData);
				RightView.ButtonAction = () =>
				{
					if( rightButtonAction != null )
					{
						rightButtonAction();
					}
				};
				AddSubview(RightView);
			}
		}

		public override void SetSelected(bool selected, bool animated)
		{
			base.SetSelected(false, false);
		}
	}
}

