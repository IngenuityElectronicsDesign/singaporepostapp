﻿using POPStation.Core.iOS;
using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.CoreFoundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public class SPEMerchantInfoView : UIView
	{
		public EMerchantData Data { get; private set; }

		UIImageView ImageView;
		UILabel MoreInfoLabel;
		UIButton OverlayButton;

		public Action ButtonAction { get; set; }

		private static SizeF Size = new SizeF(145f, 90f);

		public SPEMerchantInfoView(PointF location, EMerchantData data) : base(new RectangleF(location, Size))
		{
			Data = data;

			BackgroundColor = UIColor.White;
			Layer.BorderColor = UIColor.FromWhiteAlpha(0.6f, 1f).CGColor;
			Layer.BorderWidth = 1f;

			ImageView = new UIImageView(new RectangleF(0f, 0f, 145f, 72.5f));
			//ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
			ImageView.Alpha = 0f;
			DispatchQueue.GetGlobalQueue(DispatchQueuePriority.Background).DispatchAsync( () =>
				{
					//Console.WriteLine("IMAGE URL:" + Data.ImageUrl );
					UIImage loadingImage = UIImage.LoadFromData(NSData.FromUrl(NSUrl.FromString(Data.ImageUrl)));

					DispatchQueue.MainQueue.DispatchAsync( () =>
						{
							ImageView.Image = loadingImage;

							ImageView.ContentMode = UIViewContentMode.Center;
							if( ImageView.Bounds.Width <= loadingImage.Size.Width && ImageView.Bounds.Height <= loadingImage.Size.Height )
							{
								ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
							}
								
							UIView.Animate(0.25f, () =>
								{
									ImageView.Alpha = 1f;
								});
						});
				});
			AddSubview(ImageView);

			MoreInfoLabel = new UILabel(new RectangleF(5f, 70f, 135f, 20f));
			MoreInfoLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 14f);
			MoreInfoLabel.TextColor = UIColor.FromWhiteAlpha(0.34f, 1f);
			MoreInfoLabel.Text = "more info »";
			AddSubview(MoreInfoLabel);

			OverlayButton = new UIButton(UIButtonType.Custom);
			OverlayButton.Frame = new RectangleF(new PointF(0f, 0f), Size);
			OverlayButton.TouchUpInside += (sender, e) => 
			{
				if( ButtonAction != null )
				{
					ButtonAction();
				}
			};
			AddSubview(OverlayButton);
		}
	}
}

