using POPStation.Core.iOS;
using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.CoreFoundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public partial class SPEMerchantDetailViewController : UIViewController
	{
		UIImageView ImageView;
		UILabel TitleLabel;
		UILabel DescriptionLabel;
		UILabel WebsiteLabel;
		UIButton WebsiteLinkButton;

		string Name;
		string Desc;
		string WebsiteUrl;
		string LargeImageUrl;

		UIScreenEdgePanGestureRecognizer BackGesture;

		public SPEMerchantDetailViewController(IntPtr handle) : base(handle)
		{
		}

		public void Setup(EMerchantData data)
		{
			Name = data.Name;
			Desc = data.Description;
			WebsiteUrl = data.Url;
			LargeImageUrl = data.LargeImageUrl;
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			Title = Name;

			// Set the nav items of the new view
			UIBarButtonItem negativeSpacer = new UIBarButtonItem(UIBarButtonSystemItem.FixedSpace);
			negativeSpacer.Width = -12f; // -16 is the left edge

			UIBarButtonItem backButton = new UIBarButtonItem(UIImage.FromFile("Bar/arrowleft.png"), UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
				{
					NavigationController.PopViewControllerAnimated(true);
				});
			NavigationItem.LeftBarButtonItems = new UIBarButtonItem[] {negativeSpacer, backButton};

			// Setup view components
			ImageView = new UIImageView(new RectangleF(15f, 0f, 290f, 145f));
			ImageView.Alpha = 0f;
			DispatchQueue.GetGlobalQueue(DispatchQueuePriority.Background).DispatchAsync( () =>
				{
					//Console.WriteLine("IMAGE URL:" + ImageUrl );
					UIImage loadingImage = UIImage.LoadFromData(NSData.FromUrl(NSUrl.FromString(LargeImageUrl)));

					DispatchQueue.MainQueue.DispatchAsync( () =>
						{
							if( ImageView != null )
							{
								ImageView.Image = loadingImage;

								ImageView.ContentMode = UIViewContentMode.Center;
								if( ImageView.Bounds.Width <= loadingImage.Size.Width && ImageView.Bounds.Height <= loadingImage.Size.Height )
								{
									ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
								}

								UIView.Animate(0.25f, () =>
									{
										ImageView.Alpha = 1f;
									});
							}
						});
				});
			ContentView.AddSubview(ImageView);

			TitleLabel = new UILabel(new RectangleF(20f, 155f, 280f, 20f));
			TitleLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 18f);
			TitleLabel.TextColor = UIColor.FromWhiteAlpha(0.6f, 1f);
			TitleLabel.Text = Name;
			ContentView.AddSubview(TitleLabel);

			if( Desc == null )
			{
				Desc = "";
			}

			SizeF size = new NSString(Desc).StringSize(UIFont.FromName("AvenirNextCondensed-Regular", 14f), new SizeF(280f, 9999f), UILineBreakMode.WordWrap);

			DescriptionLabel = new UILabel(new RectangleF(20f, 180f, size.Width, size.Height));
			DescriptionLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 14f);
			DescriptionLabel.TextColor = UIColor.FromWhiteAlpha(0.6f, 1f);
			DescriptionLabel.Text = Desc;
			DescriptionLabel.Lines = 0;
			ContentView.AddSubview(DescriptionLabel);

			WebsiteLabel = new UILabel(new RectangleF(20f, (DescriptionLabel.Frame.Y + DescriptionLabel.Frame.Height + 20f), 280f, 20f));
			WebsiteLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 18f);
			WebsiteLabel.TextColor = UIColor.FromWhiteAlpha(0.6f, 1f);
			WebsiteLabel.Text = "Website";
			ContentView.AddSubview(WebsiteLabel);

			if( WebsiteUrl == null )
			{
				WebsiteUrl = "";
			}

			size = new NSString(WebsiteUrl).StringSize(UIFont.FromName("AvenirNextCondensed-Regular", 14f), new SizeF(9999f, 20f), UILineBreakMode.WordWrap);

			WebsiteLinkButton = new UIButton(UIButtonType.System);
			WebsiteLinkButton.Frame = new RectangleF(20f, (WebsiteLabel.Frame.Y + WebsiteLabel.Frame.Height), size.Width, 20f);
			WebsiteLinkButton.Font = UIFont.FromName("AvenirNextCondensed-Regular", 14f);
			WebsiteLinkButton.SetTitle(WebsiteUrl, UIControlState.Normal);
			WebsiteLinkButton.TintColor = SPShared.Instance.BlueTintColor;
			WebsiteLinkButton.TouchUpInside += (sender, e) => // Button Press
			{
				UIApplication.SharedApplication.OpenUrl(new NSUrl(WebsiteUrl));
			};
			ContentView.AddSubview(WebsiteLinkButton);
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			BackGesture = new UIScreenEdgePanGestureRecognizer();
			BackGesture.Edges = UIRectEdge.Left;
			BackGesture.AddTarget( () =>
				{
					NavigationController.PopViewControllerAnimated(true);
				});

			View.AddGestureRecognizer(BackGesture);
		}

		public override void ViewWillDisappear(bool animated)
		{
			View.RemoveGestureRecognizer(BackGesture);
		}
	}
}
