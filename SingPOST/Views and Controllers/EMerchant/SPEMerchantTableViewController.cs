using POPStation.Core.iOS;
using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.CoreFoundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public partial class SPEMerchantTableViewController : UITableViewController
	{
		List<EMerchantBannerData> MerchantBanners;
		List<Action> MerchantBannerActions;
		List<EMerchantData> Merchants;

		public SPEMerchantTableViewController(IntPtr handle) : base (handle)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse( typeof(SPEMerchantCell), new NSString("EMerchantCell") );

			MerchantBanners = new List<EMerchantBannerData>();
			MerchantBannerActions = new List<Action>();
			Merchants = new List<EMerchantData>();

			SPServerData.Instance.ServerGetBanners( "merchantbanner", (eMerchantSuccess) =>
				{
					DispatchQueue.MainQueue.DispatchAsync( () =>
						{
							if( eMerchantSuccess )
							{
								MerchantBanners.Clear();
								MerchantBannerActions.Clear();

								foreach( EMerchantBannerData merchant in SPSharedData.Instance.EMerchantBannerList )
								{
									MerchantBanners.Add(merchant);
									MerchantBannerActions.Add( () =>
										{
											UIApplication.SharedApplication.OpenUrl(new NSUrl(merchant.Url));
										});
								}
							}

							SPServerData.Instance.ServerGetEMerchants( (eMerchantBannerSuccess) =>
								{
									DispatchQueue.MainQueue.DispatchAsync( () =>
										{
											if( eMerchantBannerSuccess )
											{
												Merchants.Clear();

												foreach( EMerchantData merchant in SPSharedData.Instance.EMerchantList )
												{
													Merchants.Add(merchant);
												}
											}
											TableView.ReloadData();
										});
								});
						});
				});
		}

		public override void ViewDidLayoutSubviews()
		{
			base.ViewDidLayoutSubviews();
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 1;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			if( Merchants != null && Merchants.Count > 0 )
			{
				return (int)((Merchants.Count/2f) + 0.5f);
			}
			return 0;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			SPEMerchantCell cell = (SPEMerchantCell)TableView.DequeueReusableCell(new NSString("EMerchantCell"), indexPath);

			if( indexPath.Row*2f < Merchants.Count-1 )
			{
				cell.Setup(Merchants[(indexPath.Row*2)], Merchants[(indexPath.Row*2)+1], () => // Left Button Action
					{
						SPEMerchantDetailViewController toPush = (SPEMerchantDetailViewController)Storyboard.InstantiateViewController("EMerchantDetailVC");
						toPush.Setup(Merchants[(indexPath.Row*2)]);
						NavigationController.PushViewController(toPush, true);
					}, () => // Right Button Action
					{
						SPEMerchantDetailViewController toPush = (SPEMerchantDetailViewController)Storyboard.InstantiateViewController("EMerchantDetailVC");
						toPush.Setup(Merchants[(indexPath.Row*2)+1]);
						NavigationController.PushViewController(toPush, true);
					});
			}
			else
			{
				cell.Setup(Merchants[(indexPath.Row*2)], () => // Button Action
					{
						SPEMerchantDetailViewController toPush = (SPEMerchantDetailViewController)Storyboard.InstantiateViewController("EMerchantDetailVC");
						toPush.Setup(Merchants[(indexPath.Row*2)]);
						NavigationController.PushViewController(toPush, true);
					});
			}

			return cell;
		}

//		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
//		{
//		}

		public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return 100;
		}

		public override float GetHeightForHeader(UITableView tableView, int section)
		{
			return 145f;
		}

		public override UIView GetViewForHeader(UITableView tableView, int section)
		{
			UIView headerView = new UIView(new RectangleF(0f, 0f, 320f, 145f));
			headerView.BackgroundColor = UIColor.Clear;

			SPPagingImageView pagingImageView = new SPPagingImageView(headerView.Bounds);
			pagingImageView.Setup(MerchantBanners.ToArray(), MerchantBannerActions.ToArray(), 6.0);
			headerView.AddSubview(pagingImageView);

			return headerView;
		}
		#endregion Table View Logic
	}
}
