using POPStation.Core.iOS;
using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.CoreFoundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MonoTouch.MapKit;

namespace POPStationiOS
{
	public partial class SPLocationsListTableViewController : UITableViewController
	{
		SPCollapsableSectionHeader[] HeaderViewsArray = new SPCollapsableSectionHeader[] { new SPCollapsableSectionHeader("Central"), new SPCollapsableSectionHeader("West"), new SPCollapsableSectionHeader("North"), new SPCollapsableSectionHeader("North-East"), new SPCollapsableSectionHeader("East") };

		List<POPStationLocationData>[] Locations;

		SPLocationSearchBar SearchPopoutBar;

		UIScreenEdgePanGestureRecognizer BackGesture;

		private List<POPStationLocationData> SearchResultLocationsList;

//		Action SearchAction = () =>
//		{
//
//		};

		public SPLocationsListTableViewController(IntPtr handle) : base(handle)
		{
		}

		public void Setup(SPLocationSearchBar bar)
		{
			SearchPopoutBar = bar;

			float height = bar.Frame.Height;

			// Increase the top inset of the scroll view, so the first cell is not covered by the popout bar
			UIEdgeInsets insets = TableView.ContentInset;
			insets.Top += height;
			TableView.ContentInset = insets;
			TableView.ScrollIndicatorInsets = insets;
			TableView.ContentOffset = new PointF(TableView.ContentOffset.X, -height);
			TableView.ReloadData();

			bar.SearchAction = () =>
			{
				SearchPopoutBar.IsActive = true;

				if( !string.IsNullOrEmpty(SearchPopoutBar.SearchField.Text) )
				{
					string SearchString = SearchPopoutBar.SearchField.Text;

					SearchResultLocationsList = SPLocationsViewController.SearchWithinLocationList(SPSharedData.Instance.POPStationLocationList, SearchString.Trim() );

					if( SearchResultLocationsList == null || SearchResultLocationsList.Count <= 0 )
					{
						SearchPopoutBar.IsActive = false;

						UIAlertView alert = new UIAlertView("Search Failed", "No matches found for your search.", null, "OK");
						alert.Show();
					}
					else
					{
						Locations = new List<POPStationLocationData>[(int)POPStationLocationData.Regions.Count];

						for( int i = 0; i < (int)POPStationLocationData.Regions.Count; i++ )
						{
							Locations[i] = new List<POPStationLocationData>();
						}

						foreach( POPStationLocationData location in SearchResultLocationsList )
						{
							if( location.Region != POPStationLocationData.Regions.Count )
							{
								Locations[(int)location.Region].Add(location);

								for( int i = 0; i < Locations.Length; i++ )
								{
									if( Locations[i].Count > 0 )
									{
										HeaderViewsArray[i].CanBeExpanded = true;
										HeaderViewsArray[i].Expanded = true;
									}
									else
									{
										HeaderViewsArray[i].Expanded = false;
										HeaderViewsArray[i].CanBeExpanded = false;
									}
								}
							}
							else
							{
								Console.WriteLine("Invalid region passed");
							}
						}

						SearchPopoutBar.IsActive = false;
						TableView.ReloadData();
					}
				}
				else
				{
					// Initial search for all locations
					SearchPopoutBar.IsActive = true;

					SPServerData.Instance.ServerSearchLocation("", 100, (searchSuccess, locationsList) =>
						{
							DispatchQueue.MainQueue.DispatchAsync( () =>
								{
									Locations = new List<POPStationLocationData>[(int)POPStationLocationData.Regions.Count];

									for( int i = 0; i < (int)POPStationLocationData.Regions.Count; i++ )
									{
										Locations[i] = new List<POPStationLocationData>();
									}

									if( searchSuccess )
									{
										SPSharedData.Instance.SetSearchLocationList(locationsList);

										foreach( POPStationLocationData location in SPSharedData.Instance.POPStationLocationList )
										{
											if( location.Region != POPStationLocationData.Regions.Count )
											{
												Locations[(int)location.Region].Add(location);

												for( int i = 0; i < Locations.Length; i++ )
												{
													if( Locations[i].Count > 0 )
													{
														HeaderViewsArray[i].CanBeExpanded = true;
														HeaderViewsArray[i].Expanded = true;
													}
													else
													{
														HeaderViewsArray[i].Expanded = false;
														HeaderViewsArray[i].CanBeExpanded = false;
													}
												}
											}
											else
											{
												Console.WriteLine("Invalid region passed");
											}
										}
									}

									SearchPopoutBar.IsActive = false;
									TableView.ReloadData();
								});
						});
				}

//				SPServerData.Instance.ServerSearchLocation(SearchPopoutBar.SearchField.Text, 5, (searchSuccess, locationsList) =>
//					{
//						DispatchQueue.MainQueue.DispatchAsync( () =>
//							{
//								Locations = new List<POPStationLocationData>[(int)POPStationLocationData.Regions.Count];
//
//								for( int i = 0; i < (int)POPStationLocationData.Regions.Count; i++ )
//								{
//									Locations[i] = new List<POPStationLocationData>();
//								}
//
//								if( searchSuccess )
//								{
//									SPSharedData.Instance.SetSearchLocationList(locationsList);
//
//									foreach( POPStationLocationData location in SPSharedData.Instance.POPStationLocationList )
//									{
//										if( location.Region != POPStationLocationData.Regions.Count )
//										{
//											Locations[(int)location.Region].Add(location);
//
//											for( int i = 0; i < Locations.Length; i++ )
//											{
//												if( Locations[i].Count > 0 )
//												{
//													HeaderViewsArray[i].CanBeExpanded = true;
//													HeaderViewsArray[i].Expanded = true;
//												}
//												else
//												{
//													HeaderViewsArray[i].Expanded = false;
//													HeaderViewsArray[i].CanBeExpanded = false;
//												}
//											}
//										}
//										else
//										{
//											Console.WriteLine("Invalid region passed");
//										}
//									}
//								}
//								else
//								{
//									for( int i = 0; i < Locations.Length; i++ )
//									{
//										HeaderViewsArray[i].Expanded = false;
//										HeaderViewsArray[i].CanBeExpanded = false;
//									}
//
//									UIAlertView alert = new UIAlertView("Search Failed", "Please enter a vaild Post Code.", null, "OK");
//									alert.Show();
//								}
//
//								SearchPopoutBar.IsActive = false;
//								TableView.ReloadData();
//							});
//					});
			};

			// Initial search for all locations
			SearchPopoutBar.IsActive = true;

			SPServerData.Instance.ServerSearchLocation("", 100, (searchSuccess, locationsList) =>
				{
					DispatchQueue.MainQueue.DispatchAsync( () =>
						{
							Locations = new List<POPStationLocationData>[(int)POPStationLocationData.Regions.Count];

							for( int i = 0; i < (int)POPStationLocationData.Regions.Count; i++ )
							{
								Locations[i] = new List<POPStationLocationData>();
							}

							if( searchSuccess )
							{
								SPSharedData.Instance.SetSearchLocationList(locationsList);

								foreach( POPStationLocationData location in SPSharedData.Instance.POPStationLocationList )
								{
									if( location.Region != POPStationLocationData.Regions.Count )
									{
										Locations[(int)location.Region].Add(location);

										for( int i = 0; i < Locations.Length; i++ )
										{
											if( Locations[i].Count > 0 )
											{
												HeaderViewsArray[i].CanBeExpanded = true;
												HeaderViewsArray[i].Expanded = true;
											}
											else
											{
												HeaderViewsArray[i].Expanded = false;
												HeaderViewsArray[i].CanBeExpanded = false;
											}
										}
									}
									else
									{
										Console.WriteLine("Invalid region passed");
									}
								}
							}

							SearchPopoutBar.IsActive = false;
							TableView.ReloadData();
						});
				});
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse( typeof(SPLocationsListCell), new NSString("LocationsListCell") );

			Title = "LIST OF LOCATIONS";

			// Set the nav items of the new view
			UIBarButtonItem negativeSpacer = new UIBarButtonItem(UIBarButtonSystemItem.FixedSpace);
			negativeSpacer.Width = -12f; // -16 is the left edge

			UIBarButtonItem backButton = new UIBarButtonItem(UIImage.FromFile("Bar/arrowleft.png"), UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
				{
					NavigationController.PopViewControllerAnimated(true);
				});
			NavigationItem.LeftBarButtonItems = new UIBarButtonItem[] {negativeSpacer, backButton};
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			SearchPopoutBar.Show(true, null);

			BackGesture = new UIScreenEdgePanGestureRecognizer();
			BackGesture.Edges = UIRectEdge.Left;
			BackGesture.AddTarget( () =>
				{
					NavigationController.PopViewControllerAnimated(true);
				});

			View.AddGestureRecognizer(BackGesture);
		}

		public override void ViewWillDisappear(bool animated)
		{
			View.RemoveGestureRecognizer(BackGesture);
		}

		public override void ViewDidLayoutSubviews()
		{
			base.ViewDidLayoutSubviews();
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override int NumberOfSections(UITableView tableView)
		{
			if( Locations != null && Locations.Length > 0 )
			{
				return HeaderViewsArray.Length;
			}
			return 0;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			if( HeaderViewsArray[section].Expanded )
			{
				return Locations[section].Count;
			}
			return 0;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			SPLocationsListCell cell = (SPLocationsListCell)TableView.DequeueReusableCell(new NSString("LocationsListCell"), indexPath);
			cell.Setup(Locations[indexPath.Section][indexPath.Row]);

			return cell;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			SearchPopoutBar.Hide(false, () =>
				{
					SPLocationsDetailViewController toPush = (SPLocationsDetailViewController)Storyboard.InstantiateViewController("LocationsDetailVC");
					toPush.Setup(((SPLocationsListCell)tableView.CellAt(indexPath)).Data);
					NavigationController.PushViewController(toPush, true);
				});
		}

		public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return 72f;
		}

		public override float GetHeightForHeader(UITableView tableView, int section)
		{
			return 40f;
		}

		public override UIView GetViewForHeader(UITableView tableView, int section)
		{
			SPCollapsableSectionHeader headerView = HeaderViewsArray[section];
			headerView.ButtonAction = () =>
			{
				tableView.ReloadSections(new NSMutableIndexSet((uint)section), UITableViewRowAnimation.Fade);
			};
			return headerView;
		}
		#endregion Table View Logic
	}
}
