using POPStation.Core.iOS;
using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.MapKit;
using MonoTouch.CoreLocation;
using MonoTouch.CoreFoundation;

namespace POPStationiOS
{
	public partial class SPLocationsViewController : UIViewController
	{
		private SPLocationSearchBar SearchPopoutBar;
		public UIBarButtonItem ListButton { get; private set; }

		public UIButton TrackLocationButton { get; private set; }

		private SPAnnotation CalloutAnnotation;

		string PostCode = "";
		int Take = 100;
		bool SingleView = false;
		UIActivityIndicatorView LoadingIndicatorView;

		private PostcodeLocationData m_PostcodeLocationData;
		private bool m_PostcodeSearchCompleted;
		private bool m_LocationSearchCompleted;
		private bool m_LocationSearchSuccess;

		private List<POPStationLocationData> SearchResultLocationsList;

		public SPLocationsViewController(IntPtr handle) : base(handle)
		{
		}

		public void Setup(string name, string postCode)
		{
			Title = name;

			PostCode = postCode;
			Take = 1;
			SingleView = true;

			LoadingIndicatorView = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray);
			LoadingIndicatorView.Frame = new RectangleF(100f, 0f, 40f, 40f);
			LoadingIndicatorView.HidesWhenStopped = true;
			LoadingIndicatorView.StartAnimating();

			// Set the nav items of the new view
			UIBarButtonItem negativeSpacer = new UIBarButtonItem(UIBarButtonSystemItem.FixedSpace);
			negativeSpacer.Width = -12f; // -16 is the left edge

			UIBarButtonItem backButton = new UIBarButtonItem(UIImage.FromFile("Bar/arrowleft.png"), UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
				{
					NavigationController.PopViewControllerAnimated(true);
				});
			NavigationItem.LeftBarButtonItems = new UIBarButtonItem[] {negativeSpacer, backButton};
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			if( !SingleView )
			{
				// Setup List Button
				ListButton = new UIBarButtonItem(UIImage.FromFile("Bar/list.png"), UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
					{
						SPLocationsListTableViewController toPush = (SPLocationsListTableViewController)Storyboard.InstantiateViewController("LocationsListTVC");
						toPush.Setup(SearchPopoutBar);
						NavigationController.PushViewController(toPush, true);
					});
				NavigationItem.RightBarButtonItem = ListButton;
			}
			else
			{
				// Setup Activity Indicator
				ListButton = new UIBarButtonItem(LoadingIndicatorView);
				NavigationItem.RightBarButtonItem = ListButton;
			}

			// Setup Search Popout Bar
			SearchPopoutBar = new SPLocationSearchBar(NavigationController.NavigationBar);

			// Setup Track Location Button
			TrackLocationButton = new UIButton(UIButtonType.Custom);
			TrackLocationButton.Frame = new RectangleF(View.Frame.Width - 40f, View.Frame.Height - 40f, 30f, 30f);
			TrackLocationButton.SetImage(UIImage.FromFile("Body/mylocation.png"), UIControlState.Normal);
			TrackLocationButton.TouchUpInside += (sender, e) =>
			{
				CLLocationManager locManager = new CLLocationManager();
				if( UIDevice.CurrentDevice.CheckSystemVersion(8,0) )
				{
					locManager.RequestAlwaysAuthorization();
				}

				switch( MapView.UserTrackingMode )
				{
					case MKUserTrackingMode.None:
					{
						MapView.SetUserTrackingMode(MKUserTrackingMode.Follow, true);
						break;
					}
					default:
					{
						MapView.SetUserTrackingMode(MKUserTrackingMode.None, true);
						break;
					}
				}
			};
			View.AddSubview(TrackLocationButton);

			CLLocationCoordinate2D coords = new CLLocationCoordinate2D(1.301047789, 103.8358279);
			MKCoordinateRegion viewRegion = new MKCoordinateRegion(coords, new MKCoordinateSpan(0.1, 0.1));
			MapView.SetRegion(viewRegion, true);

			// Initial search for all locations
			SearchPopoutBar.IsActive = true;

			SPServerData.Instance.ServerSearchLocation(PostCode, Take, (searchSuccess, locationsList) =>
				{
					DispatchQueue.MainQueue.DispatchAsync( () =>
						{
							if( CalloutAnnotation != null )
							{
								MapView.DeselectAnnotation(CalloutAnnotation, true);
							}
							MapView.RemoveAnnotations(MapView.Annotations);

							if( searchSuccess )
							{
								SPSharedData.Instance.SetSearchLocationList(locationsList);

								foreach( POPStationLocationData location in SPSharedData.Instance.POPStationLocationList )
								{
									SPAnnotation annot = new SPAnnotation(false, location);
									MapView.AddAnnotation(annot);

									if( SingleView )
									{
										MapView.SelectAnnotation(annot, true);

										coords = new CLLocationCoordinate2D(location.Latitude, location.Longitude);
										viewRegion = new MKCoordinateRegion(coords, new MKCoordinateSpan(0.02, 0.02));
										MapView.SetRegion(viewRegion, true);

										LoadingIndicatorView.StopAnimating();
									}
								}
							}
							else
							{
								UIAlertView alert = new UIAlertView("Search Failed", "Cant find POPStation.", null, "OK");
								alert.Clicked += (sender, e) =>
								{
									if( NavigationController != null )
									{
										NavigationController.PopViewControllerAnimated(true);
									}
								};
								alert.Show();
								LoadingIndicatorView.StopAnimating();
							}
							SearchPopoutBar.IsActive = false;
						});
				});
		}

		public static bool IsDigitsOnly(string str)
		{
			foreach( char c in str.Trim() )
			{
				if( c < '0' || c > '9' )
				{
					return false;
				}
			}
			return true;
		}

		public static List<POPStationLocationData> GetLocationDataFromResponse( List<SearchLocationResponse> respList )
		{
			List<POPStationLocationData> retList = new List<POPStationLocationData>();

			foreach( SearchLocationResponse resp in respList )
			{
				retList.Add(new POPStationLocationData(resp));
			}

			return retList;
		}

		public static List<POPStationLocationData> SearchWithinLocationList( List<POPStationLocationData> locationList, string searchString )
		{
			List<POPStationLocationData> retList = new List<POPStationLocationData>();

			foreach( POPStationLocationData data in locationList )
			{
				if( data.ContainsString(searchString) )
				{
					retList.Add(data);
				}
			}

			return retList;
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			if( !SingleView )
			{
				SearchPopoutBar.Show(true, null);
			}

			SearchPopoutBar.SearchAction = () =>
			{
				SearchPopoutBar.IsActive = true;

				m_PostcodeLocationData = null;
				m_PostcodeSearchCompleted = false;
				m_LocationSearchCompleted = false;

				if( !string.IsNullOrEmpty(SearchPopoutBar.SearchField.Text) )
				{
					string SearchString = SearchPopoutBar.SearchField.Text;

					if( IsDigitsOnly(SearchString) )
					{
						SPServerData.Instance.RESTGetPostcodeLocation(SearchString, (success, locationData) =>
							{
								m_PostcodeSearchCompleted = true;
								if(success)
								{
									m_PostcodeLocationData = locationData;
								}
								CheckSearchResults();
							});

						SPServerData.Instance.ServerSearchLocation(SearchString, 5, (success, locationsList) =>
							{
								m_LocationSearchCompleted = true;
								if(success)
								{
									m_LocationSearchSuccess = success;
									SearchResultLocationsList = GetLocationDataFromResponse(locationsList);
								}
								CheckSearchResults();
							});
					}
					else
					{
						SearchResultLocationsList = SearchWithinLocationList(SPSharedData.Instance.POPStationLocationList, SearchString.Trim() );

						if( SearchResultLocationsList == null || SearchResultLocationsList.Count <= 0 )
						{
							SearchPopoutBar.IsActive = false;

							UIAlertView alert = new UIAlertView("Search Failed", "No matches found for your search.", null, "OK");
							alert.Show();
						}
						else
						{
							DisplaySearchResults();
						}
					}
				}
				else
				{
					SPServerData.Instance.ServerSearchLocation(PostCode, Take, (searchSuccess, locationsList) =>
						{
							DispatchQueue.MainQueue.DispatchAsync( () =>
								{
									if( CalloutAnnotation != null )
									{
										MapView.DeselectAnnotation(CalloutAnnotation, true);
									}
									MapView.RemoveAnnotations(MapView.Annotations);

									if( searchSuccess )
									{
										SPSharedData.Instance.SetSearchLocationList(locationsList);

										foreach( POPStationLocationData location in SPSharedData.Instance.POPStationLocationList )
										{
											SPAnnotation annot = new SPAnnotation(false, location);
											MapView.AddAnnotation(annot);
										}
									}
									SearchPopoutBar.IsActive = false;
								});
						});
				}
			};

			MapView.GetViewForAnnotation += (mapView, annotation) =>
			{
				if( annotation is MKUserLocation )
				{
					return null;
				}

				if( annotation is MKPointAnnotation )
				{
					MKPinAnnotationView pinView = new MKPinAnnotationView(annotation, "postCode");
					pinView.PinColor = MKPinAnnotationColor.Red;
					return pinView;
				}

				if( annotation is SPAnnotation )
				{
					SPAnnotation annot = (SPAnnotation)annotation;

					// Create an anotation view
					MKAnnotationView annotationView = null;

					if( !annot.IsCallout )
					{
						if( annot.LocationData.POPStationName.Contains("POPStation") )
						{
							annotationView = mapView.DequeueReusableAnnotation("PinAnnotation");

							if( annotationView == null )
							{
								annotationView = new MKAnnotationView(annotation, "PinAnnotation");
								annotationView.Image = UIImage.FromFile("Body/mappin-pops.png");
								annotationView.CenterOffset = new PointF(0f, -14f);
								annotationView.CanShowCallout = false; // To add our own custom callout
							}
						}
					}
					else
					{
						annotationView = mapView.DequeueReusableAnnotation("CalloutAnnotation");

						if( annotationView == null )
						{
							annotationView = new SPCalloutAnnotationView(annot, "CalloutView");
							annotationView.CenterOffset = new PointF(0f, -112f);

							((SPCalloutAnnotationView)annotationView).InfoButtonPressAction += () =>
							{
								SearchPopoutBar.Hide(false, () =>
									{
										SPLocationsDetailViewController toPush = (SPLocationsDetailViewController)Storyboard.InstantiateViewController("LocationsDetailVC");
										toPush.Setup(annot.LocationData);
										NavigationController.PushViewController(toPush, true);
									});
							};
						}
					}
					return annotationView;
				}
				return null;
			};

			MapView.DidSelectAnnotationView += (sender, e) =>
			{
				if( e.View.Annotation is SPAnnotation )
				{
					if( !((SPAnnotation)e.View.Annotation).IsCallout )
					{
						CalloutAnnotation = new SPAnnotation(true, (SPAnnotation)e.View.Annotation);
						MapView.AddAnnotation(CalloutAnnotation);
						DispatchQueue.MainQueue.DispatchAsync( () =>
							{
								MapView.SelectAnnotation(CalloutAnnotation, true);
							});
						MKCoordinateRegion viewRegion = new MKCoordinateRegion(((SPAnnotation)e.View.Annotation).Coordinate, MapView.Region.Span);// new MKCoordinateSpan(0.01, 0.01));
						MapView.SetRegion(viewRegion, true);
					}
				}
			};

			MapView.DidDeselectAnnotationView += (sender, e) => 
			{
				if( e.View.Annotation is SPAnnotation && ((SPAnnotation)e.View.Annotation).IsCallout )
				{
					MapView.RemoveAnnotation(e.View.Annotation);
				}
			};
		}

		private void CheckSearchResults()
		{
			// We need to wait for both api calls to complete before executing.
			if( !m_PostcodeSearchCompleted || !m_LocationSearchCompleted )
			{
				return;
			}

			InvokeOnMainThread( () =>
				{
					if( m_PostcodeLocationData != null && m_LocationSearchSuccess )
					{
						DisplaySearchResults();
					}
					else
					{
						SearchPopoutBar.IsActive = false;

						UIAlertView alert = new UIAlertView("Search Failed", "Please enter a vaild Post Code.", null, "OK");
						alert.Show();
					}
				});
		}

		private void DisplaySearchResults()
		{
			if( CalloutAnnotation != null )
			{
				MapView.DeselectAnnotation(CalloutAnnotation, true);
			}
			MapView.RemoveAnnotations(MapView.Annotations);

			POPStationLocationData nearestLoc = null;
			POPStationLocationData furthestLoc = null;

			foreach( POPStationLocationData location in SearchResultLocationsList )
			{
				MapView.AddAnnotation( new SPAnnotation(false, location) );

				if( nearestLoc == null )
				{
					nearestLoc = location;
				}

				furthestLoc = location;
			}

			if( nearestLoc != null && furthestLoc != null )
			{
				CLLocation furthest = new CLLocation(furthestLoc.Latitude, furthestLoc.Longitude);

				if( m_PostcodeLocationData != null )
				{
					CLLocation postcode = new CLLocation(m_PostcodeLocationData.Latitude, m_PostcodeLocationData.Longitude);
					double dist = postcode.DistanceFrom(furthest);

					MKCoordinateRegion viewRegion = MKCoordinateRegion.FromDistance(new CLLocationCoordinate2D(m_PostcodeLocationData.Latitude, m_PostcodeLocationData.Longitude), 2*dist, 2*dist);
					MapView.SetRegion(viewRegion, true);

					MKPointAnnotation postCodeAnnot = new MKPointAnnotation();
					postCodeAnnot.Coordinate = viewRegion.Center;
					postCodeAnnot.Title = SearchPopoutBar.SearchField.Text;
					MapView.AddAnnotation(postCodeAnnot);
				}
				else
				{
					CLLocation nearest = new CLLocation(nearestLoc.Latitude, nearestLoc.Longitude);
					double dist = nearest.DistanceFrom(furthest);

					MKCoordinateRegion viewRegion = MKCoordinateRegion.FromDistance(new CLLocationCoordinate2D(nearestLoc.Latitude, nearestLoc.Longitude), 2*dist, 2*dist);
					MapView.SetRegion(viewRegion, true);
				}
			}

			SearchPopoutBar.IsActive = false;
		}
	}
}
