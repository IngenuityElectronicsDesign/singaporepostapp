﻿using POPStation.Core.iOS;
using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public class SPLocationsListCell : UITableViewCell
	{
		public POPStationLocationData Data { get; private set; }

		private UILabel NameLabel;
		private UILabel AddressLabel;
		private UILabel AccessHours;
		private UIImageView IconView;

		public SPLocationsListCell(IntPtr handle) : base(handle)
		{
			SelectionStyle = UITableViewCellSelectionStyle.None;
			BackgroundColor = UIColor.Clear;

			NameLabel = new UILabel(new RectangleF(20f, 5f, 240f, 20f));
			NameLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);
			NameLabel.TextColor = UIColor.Black;
			AddSubview(NameLabel);

			AddressLabel = new UILabel(new RectangleF(20f, 25f, 240f, 20f));
			AddressLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 14f);
			AddressLabel.TextColor = UIColor.FromWhiteAlpha(0.34f, 1f);
			AddSubview(AddressLabel);

			AccessHours = new UILabel(new RectangleF(20f, 45f, 240f, 20f));
			AccessHours.Font = UIFont.FromName("AvenirNextCondensed-Regular", 14f);
			AccessHours.TextColor = UIColor.FromWhiteAlpha(0.34f, 1f);
			AddSubview(AccessHours);

			IconView = new UIImageView(new RectangleF(270f, 21f, 30f, 30f));
			IconView.Image = UIImage.FromFile("Body/parcellistarrow-default.png");
			AddSubview(IconView);

			UIView line = new UIView(new RectangleF(10f, Bounds.Height-1, 300f, 1f));
			line.BackgroundColor = UIColor.FromWhiteAlpha(0.8f, 1f);
			AddSubview(line);
		}

		public void Setup(POPStationLocationData locationData)
		{
			Data = locationData;

			NameLabel.Text = Data.POPStationName;
			AddressLabel.Text = Data.FullAddress;
			AccessHours.Text = Data.OperationHours;
		}

		public override void SetSelected(bool selected, bool animated)
		{
			base.SetSelected(false, false);
		}
	}
}

