﻿using POPStation.Core.iOS;
using System;
using MonoTouch.Foundation;
using MonoTouch.CoreLocation;
using MonoTouch.MapKit;

namespace POPStationiOS
{
	public class SPAnnotation : MKAnnotation
	{
		public bool IsCallout { get; private set; }
		public POPStationLocationData LocationData { get; private set; }
		private CLLocationCoordinate2D Coord;

		public SPAnnotation(bool isCallout, SPAnnotation annotation) : this(isCallout, annotation.LocationData) { }

		public SPAnnotation(bool isCallout, POPStationLocationData data)
		{
			IsCallout = isCallout;
			LocationData = data;
			Coordinate = new CLLocationCoordinate2D(data.Latitude, data.Longitude);
		}

		#region implemented abstract members of MKAnnotation
		public override CLLocationCoordinate2D Coordinate
		{
			get
			{
				return Coord;
			}
			set
			{
				WillChangeValue("coordinate");
				Coord = value;
				DidChangeValue("coordinate");
			}
		}
		#endregion
	}
}

