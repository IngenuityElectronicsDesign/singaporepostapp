﻿using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.CoreFoundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MonoTouch.MapKit;

namespace POPStationiOS
{
	public class SPCalloutAnnotationView : MKAnnotationView
	{
		private const float AnimDuration = 0.2f;

		public UIImageView BackgroundView { get; private set; }
		public UIView ImageLoadingView { get; private set; }
		public UIImageView ImageView { get; private set; }
		public UIImageView IconView { get; private set; }
		public UILabel Title { get; private set; }
		public UILabel SubTitle { get; private set; }
		public UIButton InfoButton { get; private set; }

		public Action InfoButtonPressAction { get; set; }

		public static RectangleF DefaultFrame = new RectangleF(0f, 0f, 300f, 160f);

		public SPCalloutAnnotationView(NSObject annotation, string reuseIdentifier) : base(annotation, reuseIdentifier)
		{
			Frame = DefaultFrame;

			Setup((SPAnnotation)annotation);
		}

		void Setup(SPAnnotation annotation)
		{
			Alpha = 0f;

			BackgroundView = new UIImageView(new RectangleF(0f, 210f, 300f, 60f));
			BackgroundView.Image = UIImage.FromFile("Body/mapcallout.png");
			AddSubview(BackgroundView);

			ImageLoadingView = new UIView(new RectangleF(0f, 0f, 300f, 110f));
			ImageLoadingView.BackgroundColor = UIColor.FromWhiteAlpha(0.2f, 0.2f);
			AddSubview(ImageLoadingView);

			ImageView = new UIImageView(new RectangleF(0f, 100f, 300f, 110f));
			ImageView.Alpha = 0f;
			DispatchQueue.GetGlobalQueue(DispatchQueuePriority.Background).DispatchAsync( () =>
				{
					NSData imageData = NSData.FromUrl(NSUrl.FromString(annotation.LocationData.ImageFileURL));
					UIImage loadingImage = UIImage.FromFile("NoLocation.png");

					if( imageData != null )
					{
						loadingImage = UIImage.LoadFromData(imageData);
					}

					DispatchQueue.MainQueue.DispatchAsync( () =>
						{
							if( ImageView != null )
							{
								ImageView.Image = loadingImage;

								ImageView.ContentMode = UIViewContentMode.Center;
								if( ImageView.Bounds.Width <= loadingImage.Size.Width || ImageView.Bounds.Height <= loadingImage.Size.Height )
								{
									ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
								}

								UIView.Animate(0.25f, () =>
									{
										ImageView.Alpha = 1f;
									});
							}
						});
				});
			AddSubview(ImageView);

			IconView = new UIImageView(new RectangleF(10f, 10f, 20f, 20f));
			IconView.Image = UIImage.FromFile("Body/mappops.png");
			BackgroundView.AddSubview(IconView);

			Title = new UILabel(new RectangleF(40f, 3f, 220f, 18f));
			Title.Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);
			Title.TextColor = UIColor.White;
			//Title.TextAlignment = UITextAlignment.Center;
			Title.Text = annotation.LocationData.POPStationName;
			BackgroundView.AddSubview(Title);

			SubTitle = new UILabel(new RectangleF(40f, 23f, 220f, 15f));
			SubTitle.Font = UIFont.FromName("AvenirNextCondensed-Regular", 14f);
			SubTitle.TextColor = UIColor.White;
			//SubTitle.TextAlignment = UITextAlignment.Center;
			SubTitle.Text = annotation.LocationData.FullAddress;
			BackgroundView.AddSubview(SubTitle);

			InfoButton = new UIButton(UIButtonType.Custom);
			InfoButton.Frame = new RectangleF(270f, 10f, 20f, 20f);
			InfoButton.SetImage(UIImage.FromFile("Body/mappopsinfo.png"), UIControlState.Normal);
			InfoButton.TintColor = UIColor.White;
			InfoButton.TouchUpInside += (sender, e) => 
			{
				if( InfoButtonPressAction != null )
				{
					InfoButtonPressAction();
				}
			};
			BackgroundView.AddSubview(InfoButton);
		}

		public override UIView HitTest(PointF point, UIEvent uievent)
		{
			UIView hitView = base.HitTest(point, uievent);

			if( hitView != null && Selected )
			{
				PointF pointInAnnotationView = hitView.ConvertPointToView(point, InfoButton);
				hitView = InfoButton.HitTest(pointInAnnotationView, uievent);
			}
			return hitView;
		}

		public override void SetSelected(bool selected, bool animated)
		{
			base.SetSelected(selected, animated);

			if( selected )
			{
				UIView.Animate(AnimDuration, () =>
					{
						Alpha = 1f;
					});
				UIView.AnimateNotify(AnimDuration, 0, 0.75f, 5f, UIViewAnimationOptions.AllowUserInteraction, () =>
					{
						BackgroundView.Frame = new RectangleF(0f, 110f, 300f, 50f);
						ImageView.Frame = new RectangleF(0f, 0f, 300f, 110f);
					}, null);
			}
		}
	}
}

