﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public class SPLocationSearchBar : SPNavPopoutBar
	{
		UIImageView ViewContainer;
		UIImageView SearchIcon;
		UIActivityIndicatorView ActivityView;
		public UITextField SearchField { get; set; }

		public Action SearchAction { get; set; }

		public bool IsActive
		{
			get	{ return ActivityView.IsAnimating; }
			set 
			{
				if( value )
				{
					ActivityView.StartAnimating();
				}
				else
				{
					ActivityView.StopAnimating();
				}
			}
		}

		public SPLocationSearchBar(UINavigationBar parentBar) : base(parentBar, null)
		{
			Setup();
		}

		public void Setup()
		{
			BorderLine.BackgroundColor = UIColor.FromWhiteAlpha(0.67f, 1f);

			ViewContainer = new UIImageView(new RectangleF(10f, 0f, 300f, 36f));
			ViewContainer.Image = UIImage.FromFile("Body/Tracking-Number-Search-Bar.png");

			SearchIcon = new UIImageView(new RectangleF(5f, 3f, 30f, 30f));
			SearchIcon.Image = UIImage.FromFile("Body/icon-search.png");
			ViewContainer.AddSubview(SearchIcon);

			ActivityView = new UIActivityIndicatorView(new RectangleF(270f, 3f, 30f, 30f));
			ActivityView.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray;
			ActivityView.HidesWhenStopped = true;
			ViewContainer.AddSubview(ActivityView);

			SearchField = new UITextField(new RectangleF(40f, 0f, 220f, 36f));
			SearchField.Font = UIFont.FromName("AvenirNextCondensed-Italic", 16f);
			SearchField.TextColor = SPShared.Instance.BlueTintColor;
			SearchField.Placeholder = "Find by Keyword or Post Code";
			SearchField.KeyboardType = UIKeyboardType.NumbersAndPunctuation;
			SearchField.ReturnKeyType = UIReturnKeyType.Search;
			SearchField.EditingDidBegin += (sender, e) => 
			{
				SearchField.Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);
			};
			SearchField.ShouldReturn += (sender) =>
			{
				SearchField.ResignFirstResponder();
				SearchField.Font = UIFont.FromName("AvenirNextCondensed-Italic", 16f);

				if( SearchAction != null && !ActivityView.IsAnimating )
				{
					SearchAction();
				}

				return true;
			};
			ViewContainer.AddSubview(SearchField);

			UIBarButtonItem negativeSpacer = new UIBarButtonItem(UIBarButtonSystemItem.FixedSpace);
			negativeSpacer.Width = -6f; // -16 is the left edge
			UIBarButtonItem item = new UIBarButtonItem(ViewContainer);

			base.SetItems(new UIBarButtonItem[] {negativeSpacer, item}, false);

			SearchAction = null;
		}
	}
}
