using POPStation.Core.iOS;
using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.CoreFoundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MonoTouch.MapKit;
using MonoTouch.CoreLocation;

namespace POPStationiOS
{
	public partial class SPLocationsDetailViewController : UIViewController
	{
		POPStationLocationData LocationData;

		UIImageView ImageView;
		UIView ImageLoadingView;
		UIImageView IconView;
		UILabel AddressLine1Label;
		UILabel AddressLine2Label;
		UILabel AccessHoursLabel;
		UILabel LocationDescriptionLabel;
		SPButton GetDirectionsButton;

		UIScreenEdgePanGestureRecognizer BackGesture;

		public SPLocationsDetailViewController(IntPtr handle) : base(handle)
		{
		}

		public void Setup(POPStationLocationData locationData)
		{
			LocationData = locationData;
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			Title = LocationData.POPStationName;

			// Set the nav items of the new view
			UIBarButtonItem negativeSpacer = new UIBarButtonItem(UIBarButtonSystemItem.FixedSpace);
			negativeSpacer.Width = -12f; // -16 is the left edge

			UIBarButtonItem backButton = new UIBarButtonItem(UIImage.FromFile("Bar/arrowleft.png"), UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
				{
					NavigationController.PopViewControllerAnimated(true);
				});
			NavigationItem.LeftBarButtonItems = new UIBarButtonItem[] {negativeSpacer, backButton};

			// Setup view components
			ImageLoadingView = new UIView(new RectangleF(0f, 0f, 320f, 220f));
			ImageLoadingView.BackgroundColor = UIColor.FromWhiteAlpha(0.8f, 0.2f);
			ContentView.AddSubview(ImageLoadingView);

			ImageView = new UIImageView(new RectangleF(0f, 0f, 320f, 220f));
			ImageView.Alpha = 0f;
			DispatchQueue.GetGlobalQueue(DispatchQueuePriority.Background).DispatchAsync( () =>
				{
					NSData imageData = NSData.FromUrl(NSUrl.FromString(LocationData.LargeImageFileURL));
					UIImage loadingImage = UIImage.FromFile("NoLocation.png");

					if( imageData != null )
					{
						loadingImage = UIImage.LoadFromData(imageData);
					}

					DispatchQueue.MainQueue.DispatchAsync( () =>
						{
							if( ImageView != null )
							{
								ImageView.Image = loadingImage;

								ImageView.ContentMode = UIViewContentMode.Center;
								if( ImageView.Bounds.Width <= loadingImage.Size.Width || ImageView.Bounds.Height <= loadingImage.Size.Height )
								{
									ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
								}

								UIView.Animate(0.25f, () =>
									{
										ImageView.Alpha = 1f;
									});
							}
						});
				});
			ContentView.AddSubview(ImageView);

			IconView = new UIImageView(new RectangleF(15f, 240f, 45f, 45f));
			IconView.Image = UIImage.FromFile("Body/icon-parcel-home-red.png");
			ContentView.AddSubview(IconView);

			string[] AddressLines =	LocationData.FullAddress.Split(",".ToCharArray(), 2);

			AddressLine1Label = new UILabel(new RectangleF(75f, 235f, 200f, 20f));
			AddressLine1Label.Font = UIFont.FromName("AvenirNextCondensed-Regular", 15f);
			AddressLine1Label.TextColor = UIColor.FromWhiteAlpha(0.2f, 1f);
			AddressLine1Label.Text = AddressLines[0];
			ContentView.AddSubview(AddressLine1Label);

			AddressLine2Label = new UILabel(new RectangleF(75f, 255f, 200f, 20f));
			AddressLine2Label.Font = UIFont.FromName("AvenirNextCondensed-Regular", 15f);
			AddressLine2Label.TextColor = UIColor.FromWhiteAlpha(0.2f, 1f);
			AddressLine2Label.Text = AddressLines[1].Trim();
			ContentView.AddSubview(AddressLine2Label);

			AccessHoursLabel = new UILabel(new RectangleF(75f, 275f, 200f, 20f));
			AccessHoursLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 15f);
			AccessHoursLabel.TextColor = UIColor.FromWhiteAlpha(0.2f, 1f);
			AccessHoursLabel.Text = "Hours: " + LocationData.OperationHours;
			ContentView.AddSubview(AccessHoursLabel);

			LocationDescriptionLabel = new UILabel(new RectangleF(75f, 295f, 200f, 20f));
			LocationDescriptionLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 15f);
			LocationDescriptionLabel.TextColor = UIColor.FromWhiteAlpha(0.2f, 1f);
			LocationDescriptionLabel.Text = "Location: " + LocationData.LocationDescription;
			LocationDescriptionLabel.LineBreakMode = UILineBreakMode.WordWrap;
			LocationDescriptionLabel.Lines = 0;
			ContentView.AddSubview(LocationDescriptionLabel);

			SizeF locDescLabelSize = new NSString(LocationDescriptionLabel.Text).StringSize(LocationDescriptionLabel.Font, new SizeF(LocationDescriptionLabel.Frame.Width, 9999f), LocationDescriptionLabel.LineBreakMode);

			LocationDescriptionLabel.Frame = new RectangleF(LocationDescriptionLabel.Frame.Location, locDescLabelSize);

			GetDirectionsButton = new SPButton(new PointF(20f, (295f + LocationDescriptionLabel.Frame.Height + 25f)), UIImage.FromFile("Body/Sign-In.png"), "Get Directions", UIImage.FromFile("Body/parcellistarrow-collected.png"), false);
			GetDirectionsButton.Button.TouchUpInside += (sender, e) => // Button Pressed
			{
				MKLaunchOptions options = new MKLaunchOptions();
				options.DirectionsMode = MKDirectionsMode.Driving;

				MKMapItem location = new MKMapItem(new MKPlacemark(new CLLocationCoordinate2D(LocationData.Latitude, LocationData.Longitude), new NSDictionary()));
				location.Name = LocationData.POPStationName;
				location.OpenInMaps(options);
			};
			ContentView.AddSubview(GetDirectionsButton);
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			BackGesture = new UIScreenEdgePanGestureRecognizer();
			BackGesture.Edges = UIRectEdge.Left;
			BackGesture.AddTarget( () =>
				{
					NavigationController.PopViewControllerAnimated(true);
				});

			View.AddGestureRecognizer(BackGesture);
		}

		public override void ViewWillDisappear(bool animated)
		{
			View.RemoveGestureRecognizer(BackGesture);
		}
	}
}
