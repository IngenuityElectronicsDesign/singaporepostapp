using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace POPStationiOS
{
	public partial class SPNavMenuTableViewController : UITableViewController
	{
		private SPSlideMenuController SlideMenuController;

		public bool WasLoggedIn { get; private set; }
		public int CurrentMenuIndex { get; private set; }

		private int NumOfMenuItems { get { return MenuTitles.Length; } }
		private int NumOfMajorMenuItems { get {	return MenuTitles.Length - 3; } } // There are 3 Minor Menu Items
		private string[] MenuTitles { get { return SPShared.Instance.IsLoggedIn ? UserMenuTitles : GuestMenuTitles; } }
		private string[] StoryboardIDs { get { return SPShared.Instance.IsLoggedIn ? UserStoryboardIDs : GuestStoryboardIDs; } }

		private string[] UserMenuTitles;
		private string[] GuestMenuTitles;

		private string[] UserStoryboardIDs;
		private string[] GuestStoryboardIDs;

		private string[] MenuIcons;
		private string[] MenuIconsActive;

		private UIImageView HeaderView;
		private UIImageView FooterView;

		public SPNavMenuTableViewController(IntPtr handle) : base(handle)
		{
			SPShared.Instance.NavMenuTableViewController = this;
		}

		public void Setup(SPSlideMenuController parent)
		{
			CurrentMenuIndex = -1;

			UserMenuTitles = new string[] { "My Parcel", "Locations", "E-Merchants", "FAQ", "Help", "Account", "Feedback", "Terms of Use", "Rate Our App" };
			GuestMenuTitles = new string[] { "My Parcel", "Locations", "E-Merchants", "FAQ", "Help", "Feedback", "Terms of Use", "Rate Our App" };

			UserStoryboardIDs = new string[] { "MyParcelMemberTVC", "LocationsVC", "EMerchantTVC", "FAQVC", "HelpVC", "AccountTVC", "FeedbackVC", "TermsOfUseVC", "" };
			GuestStoryboardIDs = new string[] { "MyParcelGuestVC", "LocationsVC", "EMerchantTVC", "FAQVC", "HelpVC", "FeedbackVC", "TermsOfUseVC", "" };

			MenuIcons = new string[] { "Navi/navi-myparcels.png", "Navi/navi-location.png", "Navi/navi-merchants.png", "Navi/navi-faq.png", "Navi/navi-help.png", "Navi/navi-account.png"};
			MenuIconsActive = new string[] { "Navi/navi-myparcels-active.png", "Navi/navi-location-active.png", "Navi/navi-merchants-active.png", "Navi/navi-faq-active.png", "Navi/navi-help-active.png", "Navi/navi-account-active.png"};

			if( UserMenuTitles.Length != UserStoryboardIDs.Length || GuestMenuTitles.Length != GuestStoryboardIDs.Length )
			{
				Console.WriteLine("Nav Menu Init Warning: Missing a title or storyboard ID");
			}

			SlideMenuController = parent;
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			TableView.RegisterClassForCellReuse( typeof(SPNavMenuCell), new NSString("NavMenuCell") );

			HeaderView = new UIImageView(new RectangleF(0f, 0f, 320f, 64f));
			HeaderView.Image = UIImage.FromFile("Navi/navi-header.png");
			HeaderView.ContentMode = UIViewContentMode.Top;

			FooterView = new UIImageView(new RectangleF(0f, 0f, 320f, 108f));
			FooterView.Image = UIImage.FromFile("Navi/navi-footer.png");
			FooterView.ContentMode = UIViewContentMode.Bottom;

			Transition(0, false, null);
		}
		#endregion View Controller Logic

		#region Table View Logic
		public override int NumberOfSections(UITableView tableView)
		{
			return 1;
		}

		public override int RowsInSection(UITableView tableview, int section)
		{
			return NumOfMenuItems;
		}

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = base.GetCell(TableView, indexPath);;

			if( indexPath.Row < NumOfMajorMenuItems )	// Major Cells
			{
				cell = TableView.DequeueReusableCell(new NSString("NavMenuCell"), indexPath);
				((SPNavMenuCell)cell).Setup(MenuTitles[indexPath.Row].ToUpper(), UIImage.FromFile(MenuIcons[indexPath.Row]), UIImage.FromFile(MenuIconsActive[indexPath.Row]) );
			}
			else										// Minor Cells
			{
				cell = TableView.DequeueReusableCell(new NSString("NavMenuCell"), indexPath);
				((SPNavMenuCell)cell).Setup(MenuTitles[indexPath.Row].ToUpper());
			}

			((SPNavMenuCell)cell).IsActive = (indexPath.Row == CurrentMenuIndex);

			return cell;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			Transition(indexPath.Row, true, null);

			tableView.ReloadData();
		}

		public override float GetHeightForHeader (UITableView tableView, int section)
		{
			return 64f;
		}

		public override UIView GetViewForHeader(UITableView tableView, int section)
		{
			return HeaderView;
		}

		public override float GetHeightForFooter (UITableView tableView, int section)
		{
			return SPShared.Instance.IsWidescreen ? (SPShared.Instance.IsLoggedIn ? 108f : 152f) : (SPShared.Instance.IsLoggedIn ? 40f : 64f);
		}

		public override UIView GetViewForFooter (UITableView tableView, int section)
		{
			return FooterView;
		}
		#endregion Table View Logic

		#region Nav Menu Logic
		void Transition(int index, bool animated, Action completion)
		{
			if( index >= 0 && string.IsNullOrEmpty(StoryboardIDs[index]) )
			{
				UIApplication.SharedApplication.OpenUrl(new NSUrl("itms-apps://itunes.apple.com/app/id916083708"));

				index = -1;
			}

			if( !(index == 0 && SPShared.Instance.IsLoggedIn == true && WasLoggedIn == true) ) // Special condition so main page will always reload when transitioned to
			{
				// If we're already on the selected view, hide the menu
				if( (index == -1) || (index == CurrentMenuIndex && SPShared.Instance.IsLoggedIn == WasLoggedIn) )
				{
					((UINavigationController)SlideMenuController.ActiveViewController).TopViewController.NavigationItem.LeftBarButtonItems[1].Image = UIImage.FromFile("Bar/mainmenu.png");
					SlideMenuController.SetActiveViewController(null, animated, completion);
					return;
				}
			}

			// Instantiate the view from the Storyboard
			UIViewController activeViewController = (UIViewController)Storyboard.InstantiateViewController(StoryboardIDs[index]);

			// Set the nav items of the new view
			UIBarButtonItem negativeSpacer = new UIBarButtonItem(UIBarButtonSystemItem.FixedSpace);
			negativeSpacer.Width = -6f; // -16 is the left edge

			activeViewController.NavigationItem.Title = MenuTitles[index].ToUpper();
			UIBarButtonItem button = new UIBarButtonItem(UIImage.FromFile("Bar/mainmenu.png"), UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
				{
					((UINavigationController)SlideMenuController.ActiveViewController).TopViewController.View.EndEditing(true);
					((UINavigationController)SlideMenuController.ActiveViewController).TopViewController.NavigationController.View.EndEditing(true);
					((UIBarButtonItem)sender).Image = UIImage.FromFile("Bar/mainmenu-active.png");
					SlideMenuController.SetMenuShowing(SPSlideMenuController.MenuStates.Open, true, true, null);
					TableView.ReloadData();
				});
			activeViewController.NavigationItem.LeftBarButtonItems = new UIBarButtonItem[] {negativeSpacer, button};

			// Create a nav controller with custom nav bar, and attach the view to it
			UINavigationController activeNavigationViewController = new UINavigationController(typeof(SPNavBar), null);
			activeNavigationViewController.PushViewController(activeViewController, false);

			UIScreenEdgePanGestureRecognizer openMenuGesture = new UIScreenEdgePanGestureRecognizer();
			openMenuGesture.Edges = UIRectEdge.Left;
			openMenuGesture.AddTarget( () =>
				{
					((UINavigationController)SlideMenuController.ActiveViewController).TopViewController.View.EndEditing(true);
					((UINavigationController)SlideMenuController.ActiveViewController).TopViewController.NavigationController.View.EndEditing(true);
					((UINavigationController)SlideMenuController.ActiveViewController).TopViewController.NavigationController.NavigationBar.Items[0].LeftBarButtonItems[1].Image = UIImage.FromFile("Bar/mainmenu-active.png");
					SlideMenuController.SetMenuShowing(SPSlideMenuController.MenuStates.Open, true, true, null);
					TableView.ReloadData();
				});

			activeViewController.View.AddGestureRecognizer(openMenuGesture);

			SlideMenuController.SetActiveViewController(activeNavigationViewController, animated, completion);

			WasLoggedIn = SPShared.Instance.IsLoggedIn;
			CurrentMenuIndex = index;
		}

		public void ShouldTransition()
		{
			ShouldTransition(-1, true, null);
		}

		public void ShouldTransition(int index, bool animated, Action completion)
		{
			TableView.ReloadData();
			SlideMenuController.SetMenuShowing(SPSlideMenuController.MenuStates.Open, animated, true, () =>
				{
					Transition(index, animated, completion);
				});
		}
		#endregion Nav Menu Logic
	}
}
