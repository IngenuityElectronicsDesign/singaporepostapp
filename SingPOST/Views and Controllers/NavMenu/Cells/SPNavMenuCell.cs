using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;

namespace POPStationiOS
{
	public partial class SPNavMenuCell : UITableViewCell
	{
		private UILabel TitleLabel;
		private UIImageView IconView;

		private UIImage NormalIcon;
		private UIImage HighlightIcon;

		private UIColor NormalTextColor = UIColor.FromWhiteAlpha(0.62f, 1f);
		private UIColor HighlightTextColor = SPShared.Instance.BlueTintColor;

		private UIColor NormalBackgroundColor = UIColor.White;
		private UIColor HighlightBackgroundColor = UIColor.FromWhiteAlpha(0.94f, 1f);
		private UIColor MinorBackgroundColor = UIColor.FromWhiteAlpha(0.89f, 1f);

		private bool IsMajor;

		// Properties
		private bool Active;
		public bool IsActive
		{
			get
			{
				return Active;
			}
			set
			{
				UIView.Animate(0.1f, () => // Animation
					{
						TitleLabel.TextColor = value ? HighlightTextColor : NormalTextColor;

						if( IsMajor )
						{
							BackgroundColor = value ? HighlightBackgroundColor : NormalBackgroundColor;
							IconView.Image = value ? HighlightIcon : NormalIcon;
						}
					}, () => // Completion
					{
						Active = value;
					});
			}
		}

		public SPNavMenuCell(IntPtr handle) : base(handle)
		{
			TitleLabel = new UILabel(new RectangleF(56f, 6f, 172f, 32f));
			TitleLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
			TitleLabel.TextColor = NormalTextColor;
			AddSubview(TitleLabel);

			IconView = new UIImageView(new RectangleF(12f, 6f, 32f, 32f));
			AddSubview(IconView);

			UIView line = new UIView(new RectangleF(0f, Bounds.Height-1, Bounds.Width, 1f));
			line.BackgroundColor = UIColor.FromWhiteAlpha(0.8f, 1f);
			AddSubview(line);

			SelectionStyle = UITableViewCellSelectionStyle.None;
		}

		public void Setup(string title)
		{
			TitleLabel.Frame = new RectangleF(15f, 6f, 215f, 32f);
			TitleLabel.Text = title;

			IconView.Image = null;

			BackgroundColor = MinorBackgroundColor;

			IsMajor = false;
		}

		public void Setup(string title, UIImage normalIcon, UIImage highlightIcon)
		{
			TitleLabel.Frame = new RectangleF(56f, 6f, 172f, 32f);
			TitleLabel.Text = title;
			NormalIcon = normalIcon;
			HighlightIcon = highlightIcon;

			IconView.Image = NormalIcon;

			IsMajor = true;
		}

		public override void SetSelected(bool selected, bool animated)
		{
			base.SetSelected(false, false);
		}
	}
}
