﻿using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace POPStationiOS
{
	public class SPTextFieldWithConfirmation : UITextField
	{
		UIImageView ConfirmationIconView;
		UIImage SuccessImage;
		UIImage FailImage;

		public SPTextFieldWithConfirmation(RectangleF frame) : base(frame)
		{
			Background = UIImage.FromFile("Body/Text-Field-Backgrounds.png");
			BorderStyle = UITextBorderStyle.Bezel;
			AutocapitalizationType = UITextAutocapitalizationType.None;
			AutocorrectionType = UITextAutocorrectionType.No;
			KeyboardType = UIKeyboardType.Default;
			Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
			TextColor = SPShared.Instance.BlueTintColor;

			ConfirmationIconView = new UIImageView(new RectangleF(245f, 7f, 30f, 30f));
			SuccessImage = UIImage.FromFile("Body/checked-fillgreen.png");
			FailImage = UIImage.FromFile("Body/fail-fillred.png");
			AddSubview(ConfirmationIconView);
		}

		public void SetConfirmation(bool success)
		{
			ConfirmationIconView.Image = success ? SuccessImage : FailImage;
		}

		public void ClearConfirmation()
		{
			ConfirmationIconView.Image = null;
		}
	}
}

