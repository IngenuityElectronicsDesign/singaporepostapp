using POPStation.Core.iOS;
using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MonoTouch.CoreText;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace POPStationiOS
{
	public partial class SPForgotPasswordViewController : UIViewController
	{
		// Form Views
		UILabel TopLabel;
		UITextField EmailField;
		SPButton SendRequestButton;

		public SPForgotPasswordViewController(IntPtr handle) : base(handle)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			Title = "Forgot Password";

			// Set the nav items of the new view
			UITextAttributes attr = new UITextAttributes();
			attr.TextColor = SPShared.Instance.BlueTintColor;
			attr.Font = UIFont.FromName("AvenirNextCondensed-Regular", 18f);

			UIBarButtonItem negativeSpacer = new UIBarButtonItem(UIBarButtonSystemItem.FixedSpace);
			negativeSpacer.Width = -12f; // -16 is the left edge

			UIBarButtonItem cancelButton = new UIBarButtonItem(UIImage.FromFile("Bar/arrowleft.png"), UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
				{
					NavigationController.PopViewControllerAnimated(true);
				});
			NavigationItem.LeftBarButtonItems = new UIBarButtonItem[] {negativeSpacer, cancelButton};

			UIBarButtonItem doneButton = new UIBarButtonItem("Done", UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
				{
					// Save some stuff, then on completion
					NavigationController.PopViewControllerAnimated(true);
				});
			doneButton.SetTitleTextAttributes(attr, UIControlState.Normal);
			NavigationItem.RightBarButtonItem = doneButton;

			// Form Views
			TopLabel = new UILabel(new RectangleF(40f, 20f, 240f, 70f));
			TopLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);
			TopLabel.Text = "Please enter your registered email\naddress. You will receive a link to create\na new password via your email.";
			TopLabel.TextColor = UIColor.FromWhiteAlpha(0.66f, 1f);
			TopLabel.TextAlignment = UITextAlignment.Center;
			TopLabel.Lines = 3;
			ContentView.AddSubview(TopLabel);

			EmailField = new UITextField(new RectangleF(20f, 120f, 280f, 45f));
			EmailField.Background = UIImage.FromFile("Body/Text-Field-Backgrounds.png");
			EmailField.BorderStyle = UITextBorderStyle.Bezel;
			EmailField.AutocapitalizationType = UITextAutocapitalizationType.None;
			EmailField.AutocorrectionType = UITextAutocorrectionType.No;
			EmailField.KeyboardType = UIKeyboardType.EmailAddress;
			EmailField.ReturnKeyType = UIReturnKeyType.Send;
			EmailField.Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
			EmailField.TextColor = SPShared.Instance.BlueTintColor;
			EmailField.Placeholder = "EMAIL ADDRESS";
			EmailField.ShouldReturn += (sender) =>
			{
				EmailField.ResignFirstResponder();
				SendRequest();
				return true;
			};
			ContentView.AddSubview(EmailField);

			SendRequestButton = new SPButton(new PointF(20f, 180f), UIImage.FromFile("Body/Sign-In.png"), "Send Request", UIImage.FromFile("Body/parcellistarrow-collected.png"), false);
			SendRequestButton.Button.TouchUpInside += (sender, e) => // Button Pressed
			{
				SendRequest();
			};
			ContentView.AddSubview(SendRequestButton);
		}

		public override void ViewDidLayoutSubviews()
		{
			base.ViewDidLayoutSubviews ();

			ContentView.ContentSize = new SizeF(ContentView.Frame.Width, 230f);
		}
		#endregion View Controller Logic

		public void SendRequest()
		{
			SendRequestButton.IsActive = true;

			if( EmailIsValid(EmailField.Text) )
			{
				SPServerData.Instance.ServerRequestLostPassword(EmailField.Text, (success) =>
					{
						InvokeOnMainThread( () =>
							{
								SendRequestButton.IsActive = false;

								if( success )
								{
									UIAlertView alert = new UIAlertView("Reset Request Successful", "Please check your email account for password reset instructions.", null, "OK");
									alert.Show();
								}
								else
								{
									UIAlertView alert = new UIAlertView("Error Requesting Reset", "Please check your email address and try again, or contact support if the problem persists.", null, "OK");
									alert.Show();
								}
							});
					});
			}
			else
			{
				UIAlertView alert = new UIAlertView("Email Address Format Invalid", "Please check your email address and try again.", null, "OK");
				alert.Show();
			}
		}

		public bool EmailIsValid(string email)
		{
			if( Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase) )
			{
				return true;
			}
			else
			{
				SendRequestButton.IsActive = false;
				return false;
			}
		}
	}
}
