using POPStation.Core.iOS;
using System;
using System.Timers;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MonoTouch.CoreText;

namespace POPStationiOS
{
	public partial class SPMyParcelGuestViewController : SPAnimatedStateViewController
	{
		private enum ScreenState
		{
			Initial,
			SignInFields,
			FirstTimeFields
		}
		private ScreenState CurrentState;

		public UIBarButtonItem TrackAndTraceButton { get; private set; }
		private SPTrackAndTrackPopoutBar TrackAndTracePopoutBar;

		// Initial Form Views
		UIImageView TopIconView;
		UILabel TopLabel;
		SPButton SignInButton;
		UILabel BottomLabel;
		UIButton BottomLabelLink;

		// SignInFields Form Views
		UITextField EmailField;
		UITextField PasswordField;
		UIButton ForgotPasswordButton;

		// FirstTimeFields Form Views
		UIView FirstTimeFieldsContainer;
		UILabel FirstTimeTopLabel;
		SPTextFieldWithConfirmation FirstTimeOldPasswordField;
		SPTextFieldWithConfirmation FirstTimeNewPassword1Field;
		SPTextFieldWithConfirmation FirstTimeNewPassword2Field;
		UITextField FirstTimePostCodeField;
		UIImageView FirstTimeAddressBackground;
		UIActivityIndicatorView FirstTimeAddressActivity;
		UILabel FirstTimeAddressLabel;
		UITextField FirstTimeUnitNumber;
		UIImageView FirstTimeCountryBackground;
		UILabel FirstTimeCountryLabel;
		SPButton FirstTimeSaveChangesButton;
		SearchAddressData FirstTimeSearchAddressData;

		public SPMyParcelGuestViewController(IntPtr handle) : base(handle)
		{
		}

		#region View Controller Logic
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			NavigationItem.TitleView = new UIImageView(UIImage.FromFile("Bar/logo-pops.png"));

			// Automatically adjust scroll view content insets to compensate for keyboard
			NSNotificationCenter.DefaultCenter.AddObserver("UIKeyboardWillShowNotification", (NSNotification notification) =>
				{
					UIEdgeInsets contentInsets = new UIEdgeInsets(ContentView.ContentInset.Top, ContentView.ContentInset.Left, 216f, ContentView.ContentInset.Right);
					ContentView.ContentInset = contentInsets;
					ContentView.ScrollIndicatorInsets = contentInsets;
				});

			NSNotificationCenter.DefaultCenter.AddObserver("UIKeyboardWillHideNotification", (NSNotification notification) =>
				{
					UIEdgeInsets contentInsets = new UIEdgeInsets(ContentView.ContentInset.Top, ContentView.ContentInset.Left, 0f, ContentView.ContentInset.Right);
					ContentView.ContentInset = contentInsets;
					ContentView.ScrollIndicatorInsets = contentInsets;
				});

			// Setup Track and Trace Button
			TrackAndTraceButton = new UIBarButtonItem(UIImage.FromFile("Bar/trackntrace-active.png"), UIBarButtonItemStyle.Plain, (sender, e) => // Button Pressed
				{
					if( TrackAndTracePopoutBar.IsVisible )	// Currently Showing
					{
						TrackAndTracePopoutBar.Hide(true, () =>
							{
								((SPNavBar)TrackAndTracePopoutBar.ParentBar).SetBorderLineShouldHide(false, false);
								TrackAndTraceButton.Image = UIImage.FromFile("Bar/trackntrace.png");
							});
					}
					else 							// Current Hidden
					{
						((SPNavBar)TrackAndTracePopoutBar.ParentBar).SetBorderLineShouldHide(true, false);
						TrackAndTracePopoutBar.Show(true, () =>
							{
								TrackAndTraceButton.Image = UIImage.FromFile("Bar/trackntrace-active.png");
							});
					}
				});
			NavigationItem.RightBarButtonItem = TrackAndTraceButton;

			// Setup Track and Trace Popout Bar
			TrackAndTracePopoutBar = new SPTrackAndTrackPopoutBar(NavigationController.NavigationBar, ContentView);
			((SPNavBar)TrackAndTracePopoutBar.ParentBar).SetBorderLineShouldHide(true, false);

			// Setup Current Screen State
			CurrentState = ScreenState.Initial;

			// Initial Form Views
			TopIconView = new UIImageView(UIImage.FromFile("Body/icon-parcel-home-grey.png"));

			ViewList.Add(TopIconView);
			PointsList.Add(new PointF[] { new PointF(130f, 15f), new PointF(130f, 15f), new PointF(130f, 5f) });
			VisibilityList.Add(new bool[] { true, true, true });

			TopLabel = new UILabel(new RectangleF(40f, 80f, 240f, 50f));
			TopLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);
			TopLabel.Text = "Log in to track your POPStation parcels\nand get more parcel details.";
			TopLabel.TextColor = UIColor.FromWhiteAlpha(0.66f, 1f);
			TopLabel.TextAlignment = UITextAlignment.Center;
			TopLabel.Lines = 2;

			ViewList.Add(TopLabel);
			PointsList.Add(new PointF[] { new PointF(40f, 80f), new PointF(40f, 80f), new PointF(40f, 60f) });
			VisibilityList.Add(new bool[] { true, true, false });

			SignInButton = new SPButton(new PointF(20f, 145f), UIImage.FromFile("Body/Sign-In.png"), "Sign in", UIImage.FromFile("Body/parcellistarrow-collected.png"), false);
			SignInButton.Button.TouchUpInside += (sender, e) => // Button Pressed
			{
				switch( CurrentState )
				{
					case ScreenState.Initial:
					{
						CurrentState = ScreenState.SignInFields;
						LayoutContentSubviews(CurrentState, true, null);
						break;
					}
					case ScreenState.SignInFields:
					{
						SignInButtonPress();
						break;
					}
					default:
					{
						break;
					}
				}
			};

			ViewList.Add(SignInButton);
			PointsList.Add(new PointF[] { new PointF(20f, 145f), new PointF(20f, 255f), new PointF(20f, 70f) });
			VisibilityList.Add(new bool[] { true, true, true });

			BottomLabel = new UILabel(new RectangleF(40f, 205f, 240f, 50f));
			BottomLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);
			BottomLabel.Text = "You must be registered to use POPStation\nservices.                           ."; // Bit of a hack, to get the hyperlink word looking good.
			BottomLabel.TextColor = UIColor.FromWhiteAlpha(0.66f, 1f);
			BottomLabel.TextAlignment = UITextAlignment.Center;
			BottomLabel.Lines = 2;

			ViewList.Add(BottomLabel);
			PointsList.Add(new PointF[] { new PointF(40f, 205f), new PointF(40f, 335f), new PointF(40f, 110f) });
			VisibilityList.Add(new bool[] { true, true, false });

			BottomLabelLink = new UIButton(UIButtonType.System);
			BottomLabelLink.Frame = new RectangleF(124f, 229f, 120f, 25f);
			BottomLabelLink.Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);
			BottomLabelLink.SetTitle("Find out more", UIControlState.Normal);
			BottomLabelLink.TintColor = SPShared.Instance.BlueTintColor;
			BottomLabelLink.TouchUpInside += (sender, e) => // Button Press
			{
				UIApplication.SharedApplication.OpenUrl(new NSUrl(SPSharedData.PopstationURLMain + "find-out-more"));//"faqs#faq-02-02"));
			};

			ViewList.Add(BottomLabelLink);
			PointsList.Add(new PointF[] { new PointF(124f, 229f), new PointF(124f, 359f), new PointF(124f, 138f) });
			VisibilityList.Add(new bool[] { true, true, false });

			// SignInFields Form Views
			EmailField = new UITextField(new RectangleF(20f, 120f, 280f, 45f));
			EmailField.Background = UIImage.FromFile("Body/Text-Field-Backgrounds.png");
			EmailField.BorderStyle = UITextBorderStyle.Bezel;
			EmailField.AutocapitalizationType = UITextAutocapitalizationType.None;
			EmailField.AutocorrectionType = UITextAutocorrectionType.No;
			EmailField.KeyboardType = UIKeyboardType.EmailAddress;
			EmailField.ReturnKeyType = UIReturnKeyType.Next;
			EmailField.Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
			EmailField.TextColor = SPShared.Instance.BlueTintColor;
			EmailField.Placeholder = "ID (EMAIL ADDRESS)";
			EmailField.ShouldReturn += (sender) =>
			{
				PasswordField.BecomeFirstResponder();
				return true;
			};

			ViewList.Add(EmailField);
			PointsList.Add(new PointF[] { new PointF(20f, 120f), new PointF(20f, 140f), new PointF(20f, 100f) });
			VisibilityList.Add(new bool[] { false, true, false });

			PasswordField = new UITextField(new RectangleF(20f, 170f, 280f, 45f));
			PasswordField.Background = UIImage.FromFile("Body/Text-Field-Backgrounds.png");
			PasswordField.BorderStyle = UITextBorderStyle.Bezel;
			PasswordField.AutocapitalizationType = UITextAutocapitalizationType.None;
			PasswordField.AutocorrectionType = UITextAutocorrectionType.No;
			PasswordField.KeyboardType = UIKeyboardType.Default;
			PasswordField.ReturnKeyType = UIReturnKeyType.Go;
			PasswordField.SecureTextEntry = true;
			PasswordField.Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
			PasswordField.TextColor = SPShared.Instance.BlueTintColor;
			PasswordField.Placeholder = "PASSWORD";
			PasswordField.ShouldReturn += (sender) =>
			{
				SignInButtonPress();
				return true;
			};

			ViewList.Add(PasswordField);
			PointsList.Add(new PointF[] { new PointF(20f, 170f), new PointF(20f, 190f), new PointF(20f, 150f) });
			VisibilityList.Add(new bool[] { false, true, false });

			ForgotPasswordButton = new UIButton(UIButtonType.System);
			ForgotPasswordButton.Frame = new RectangleF(100f, 165f, 120f, 30f);
			ForgotPasswordButton.Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);
			ForgotPasswordButton.SetTitle("Forgot Password?", UIControlState.Normal);
			ForgotPasswordButton.TintColor = SPShared.Instance.BlueTintColor;
			ForgotPasswordButton.TouchUpInside += (sender, e) => 
			{
				if( TrackAndTracePopoutBar.IsVisible )
				{
					TrackAndTracePopoutBar.Hide(true, () =>
						{
							((SPNavBar)TrackAndTracePopoutBar.ParentBar).SetBorderLineShouldHide(false, false);
							TrackAndTraceButton.Image = UIImage.FromFile("Bar/trackntrace.png");

							UIViewController toPush = (UIViewController)Storyboard.InstantiateViewController("ForgotPasswordVC");
							NavigationController.PushViewController(toPush, true);
						});
				}
				else
				{
					UIViewController toPush = (UIViewController)Storyboard.InstantiateViewController("ForgotPasswordVC");
					NavigationController.PushViewController(toPush, true);
				}
			};

			ViewList.Add(ForgotPasswordButton);
			PointsList.Add(new PointF[] { new PointF(100f, 185f), new PointF(100f, 305f), new PointF(100f, 70f) });
			VisibilityList.Add(new bool[] { false, true, false });

			// FirstTimeFields Form Views
			FirstTimeFieldsContainer = new UIView(new RectangleF(10f, 180f, 300f, 440f));

			ViewList.Add(FirstTimeFieldsContainer);
			PointsList.Add(new PointF[] { new PointF(10f, 180f), new PointF(10f, 300f), new PointF(10f, 115f) });
			VisibilityList.Add(new bool[] { false, false, true });

			FirstTimeTopLabel = new UILabel(new RectangleF(50f, 0f, 200f, 20f));
			FirstTimeTopLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);
			FirstTimeTopLabel.Text = "Change Your Password";
			FirstTimeTopLabel.TextColor = UIColor.FromWhiteAlpha(0.66f, 1f);
			FirstTimeTopLabel.TextAlignment = UITextAlignment.Center;
			FirstTimeFieldsContainer.AddSubview(FirstTimeTopLabel);

			FirstTimeOldPasswordField = new SPTextFieldWithConfirmation(new RectangleF(10f, 25f, 280f, 45f));
			FirstTimeOldPasswordField.ReturnKeyType = UIReturnKeyType.Next;
			FirstTimeOldPasswordField.SecureTextEntry = true;
			FirstTimeOldPasswordField.Placeholder = "OLD PASSWORD";
			FirstTimeOldPasswordField.EditingChanged += (sender, e) => 
			{
				FirstTimeOldPasswordField.ClearConfirmation();
			};
			FirstTimeOldPasswordField.ShouldReturn += (sender) =>
			{
				FirstTimeNewPassword1Field.BecomeFirstResponder();
				return true;
			};
			FirstTimeFieldsContainer.AddSubview(FirstTimeOldPasswordField);

			FirstTimeNewPassword1Field = new SPTextFieldWithConfirmation(new RectangleF(10f, 75f, 280f, 45f));
			FirstTimeNewPassword1Field.ReturnKeyType = UIReturnKeyType.Next;
			FirstTimeNewPassword1Field.SecureTextEntry = true;
			FirstTimeNewPassword1Field.Placeholder = "NEW PASSWORD";
			FirstTimeNewPassword1Field.EditingChanged += (sender, e) => 
			{
				if( string.IsNullOrEmpty(FirstTimeNewPassword1Field.Text) )
				{
					FirstTimeNewPassword1Field.SetConfirmation( false );
				}
				else if( FirstTimeNewPassword1Field.Text.Length >= 7 )
				{
					FirstTimeNewPassword1Field.SetConfirmation( true );
				}
				else
				{
					FirstTimeNewPassword1Field.ClearConfirmation();
				}
			};
			FirstTimeNewPassword1Field.ShouldReturn += (sender) =>
			{
				FirstTimeNewPassword2Field.BecomeFirstResponder();
				return true;
			};
			FirstTimeFieldsContainer.AddSubview(FirstTimeNewPassword1Field);

			FirstTimeNewPassword2Field = new SPTextFieldWithConfirmation(new RectangleF(10f, 125f, 280f, 45f));
			FirstTimeNewPassword2Field.ReturnKeyType = UIReturnKeyType.Next;
			FirstTimeNewPassword2Field.SecureTextEntry = true;
			FirstTimeNewPassword2Field.Placeholder = "CONFIRM NEW PASSWORD";
			FirstTimeNewPassword2Field.EditingChanged += (sender, e) => 
			{
				if( string.IsNullOrEmpty(FirstTimeNewPassword2Field.Text) )
				{
					FirstTimeNewPassword2Field.SetConfirmation( false );
				}
				else if( FirstTimeNewPassword2Field.Text == FirstTimeNewPassword1Field.Text )
				{
					FirstTimeNewPassword2Field.SetConfirmation( true );
				}
				else
				{
					FirstTimeNewPassword2Field.ClearConfirmation();
				}
			};
			FirstTimeNewPassword2Field.ShouldReturn += (sender) =>
			{
				FirstTimePostCodeField.BecomeFirstResponder();
				return true;
			};
			FirstTimeFieldsContainer.AddSubview(FirstTimeNewPassword2Field);

			FirstTimePostCodeField = new UITextField(new RectangleF(10f, 175f, 280f, 45f));
			FirstTimePostCodeField.Background = UIImage.FromFile("Body/Text-Field-Backgrounds.png");
			FirstTimePostCodeField.BorderStyle = UITextBorderStyle.Bezel;
			FirstTimePostCodeField.AutocapitalizationType = UITextAutocapitalizationType.None;
			FirstTimePostCodeField.AutocorrectionType = UITextAutocorrectionType.No;
			FirstTimePostCodeField.KeyboardType = UIKeyboardType.NumbersAndPunctuation;
			FirstTimePostCodeField.ReturnKeyType = UIReturnKeyType.Next;
			FirstTimePostCodeField.Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
			FirstTimePostCodeField.TextColor = SPShared.Instance.BlueTintColor;
			FirstTimePostCodeField.Placeholder = "POST CODE";
			FirstTimePostCodeField.ShouldReturn += (sender) =>
			{
				FirstTimeUnitNumber.BecomeFirstResponder();
				return true;
			};
			FirstTimePostCodeField.EditingDidEnd += (sender, e) =>
			{
				if( !string.IsNullOrEmpty(FirstTimePostCodeField.Text) )
				{
					FirstTimeAddressActivity.StartAnimating();
					SPServerData.Instance.RESTSearchAddress( FirstTimePostCodeField.Text, (success, address) =>
						{
							InvokeOnMainThread( () =>
								{
									FirstTimeAddressActivity.StopAnimating();
									if( success && address != null )
									{
										FirstTimeSearchAddressData = address;
										FirstTimeAddressLabel.Text = address.BuildingName + (!string.IsNullOrEmpty(address.BuildingName) ? "\n" : "" ) + address.BuildingNo + " " + address.StreetName;
										FirstTimeAddressLabel.TextColor = SPShared.Instance.BlueTintColor;
									}
									else
									{
										FirstTimePostCodeField.BecomeFirstResponder();
										UIAlertView alert = new UIAlertView("Error Retrieving Address", "Please try again, or contact support if the problem persists.", null, "OK");
										alert.Show();
									}
								});
						});
				}
			};
			FirstTimeFieldsContainer.AddSubview(FirstTimePostCodeField);

			FirstTimeAddressBackground = new UIImageView(new RectangleF(10f, 225f, 280f, 65f));
			FirstTimeAddressBackground.Image = UIImage.FromFile("Body/Text-Field-Backgrounds.png").CreateResizableImage(new UIEdgeInsets(4f, 4f, 4f, 4f));
			FirstTimeFieldsContainer.AddSubview(FirstTimeAddressBackground);

			FirstTimeAddressActivity = new UIActivityIndicatorView(new RectangleF(240f, 17f, 30f, 30f));
			FirstTimeAddressActivity.HidesWhenStopped = true;
			FirstTimeAddressActivity.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray;
			FirstTimeAddressBackground.AddSubview(FirstTimeAddressActivity);

			FirstTimeAddressLabel = new UILabel(new RectangleF(17f, 225f, 260f, 65f));
			FirstTimeAddressLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
			FirstTimeAddressLabel.Text = "ENTER POST CODE ABOVE TO\nGET ADDRESS AUTOMATICALLY";
			FirstTimeAddressLabel.TextColor = UIColor.FromWhiteAlpha(0.7f, 1f);
			FirstTimeAddressLabel.Lines = 0;
			FirstTimeFieldsContainer.AddSubview(FirstTimeAddressLabel);

			FirstTimeUnitNumber = new UITextField(new RectangleF(10f, 295f, 90f, 45f));
			FirstTimeUnitNumber.Background = UIImage.FromFile("Body/Text-Field-Backgrounds.png").CreateResizableImage(new UIEdgeInsets(4f, 4f, 4f, 4f));;
			FirstTimeUnitNumber.BorderStyle = UITextBorderStyle.Bezel;
			FirstTimeUnitNumber.AutocapitalizationType = UITextAutocapitalizationType.None;
			FirstTimeUnitNumber.AutocorrectionType = UITextAutocorrectionType.No;
			FirstTimeUnitNumber.KeyboardType = UIKeyboardType.NumbersAndPunctuation;
			FirstTimeUnitNumber.ReturnKeyType = UIReturnKeyType.Done;
			FirstTimeUnitNumber.Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
			FirstTimeUnitNumber.TextColor = SPShared.Instance.BlueTintColor;
			FirstTimeUnitNumber.Placeholder = "UNIT NO";
			FirstTimeUnitNumber.ShouldReturn += (sender) =>
			{
				FirstTimeSaveChangesButtonPress();
				return true;
			};
			FirstTimeFieldsContainer.AddSubview(FirstTimeUnitNumber);

			FirstTimeCountryBackground = new UIImageView(new RectangleF(105f, 295f, 185f, 45f));
			FirstTimeCountryBackground.Image = UIImage.FromFile("Body/Text-Field-Backgrounds.png").CreateResizableImage(new UIEdgeInsets(4f, 4f, 4f, 4f));
			FirstTimeFieldsContainer.AddSubview(FirstTimeCountryBackground);

			FirstTimeCountryLabel = new UILabel(new RectangleF(115f, 295f, 180f, 45f));
			FirstTimeCountryLabel.Font = UIFont.FromName("AvenirNextCondensed-Regular", 17f);
			FirstTimeCountryLabel.Text = "SINGAPORE";
			FirstTimeCountryLabel.TextColor = UIColor.FromWhiteAlpha(0.7f, 1f);
			FirstTimeFieldsContainer.AddSubview(FirstTimeCountryLabel);

			FirstTimeSaveChangesButton = new SPButton(new PointF(10f, 355f), UIImage.FromFile("Body/Sign-In.png"), "Save Changes", UIImage.FromFile("Body/parcellistarrow-collected.png"), false);
			FirstTimeSaveChangesButton.Button.TouchUpInside += (sender, e) => // Button Pressed
			{
				FirstTimeSaveChangesButtonPress();
			};
			FirstTimeFieldsContainer.AddSubview(FirstTimeSaveChangesButton);

			ContentView.AddSubviews(ViewList.ToArray());

			LayoutContentSubviews(CurrentState, false, null);
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			TrackAndTracePopoutBar.Show(false, null);
			((SPNavBar)TrackAndTracePopoutBar.ParentBar).SetBorderLineShouldHide(true, false);

			// Ensure the Search Button Action is reset when we segue back
			TrackAndTracePopoutBar.SearchAction = () =>
			{
				if( string.IsNullOrEmpty(TrackAndTracePopoutBar.SearchField.Text) )
				{
					return;
				}

				if( !TrackAndTracePopoutBar.IsVisible )
				{
					((SPNavBar)TrackAndTracePopoutBar.ParentBar).SetBorderLineShouldHide(true, false);
					TrackAndTracePopoutBar.Show(true, () =>
						{
							TrackAndTraceButton.Image = UIImage.FromFile("Bar/trackntrace-active.png");

							SPTrackAndTraceTableViewController toPush = (SPTrackAndTraceTableViewController)Storyboard.InstantiateViewController("TrackAndTraceTVC");
							toPush.Setup(TrackAndTracePopoutBar);
							NavigationController.PushViewController(toPush, true);
						});
				}
				else
				{
					TrackAndTracePopoutBar.Hide(true, () =>
						{
							((SPNavBar)TrackAndTracePopoutBar.ParentBar).SetBorderLineShouldHide(false, false);
							TrackAndTraceButton.Image = UIImage.FromFile("Bar/trackntrace.png");
						});
					SPTrackAndTraceTableViewController toPush = (SPTrackAndTraceTableViewController)Storyboard.InstantiateViewController("TrackAndTraceTVC");
					toPush.Setup(TrackAndTracePopoutBar);
					NavigationController.PushViewController(toPush, true);
				}

//				if( !TrackAndTracePopoutBar.IsVisible )
//				{
//					((SPNavBar)TrackAndTracePopoutBar.ParentBar).SetBorderLineShouldHide(true, false);
//					TrackAndTracePopoutBar.Show(true, () =>
//						{
//							TrackAndTraceButton.Image = UIImage.FromFile("Bar/trackntrace-active.png");
//
//							SPTrackAndTraceTableViewController toPush = (SPTrackAndTraceTableViewController)Storyboard.InstantiateViewController("TrackAndTraceTVC");
//							toPush.Setup(TrackAndTracePopoutBar);
//							NavigationController.PushViewController(toPush, true);
//						});
//				}
//				else
//				{
//					SPTrackAndTraceTableViewController toPush = (SPTrackAndTraceTableViewController)Storyboard.InstantiateViewController("TrackAndTraceTVC");
//					toPush.Setup(TrackAndTracePopoutBar);
//					NavigationController.PushViewController(toPush, true);
//				}
			};
		}

		public override void ViewDidLayoutSubviews()
		{
			base.ViewDidLayoutSubviews ();

			float height = 0f;

			switch( CurrentState )
			{
			case ScreenState.Initial:
				height = 250f;
				break;
			case ScreenState.SignInFields:
				height = 390f;
				break;
			case ScreenState.FirstTimeFields:
				height = 530f;
				break;
			}

			ContentView.ContentSize = new SizeF(ContentView.Frame.Width, height);
		}
		#endregion View Controller Logic

		private void SignInButtonPress()
		{
			PasswordField.ResignFirstResponder();
			SPServerData.Instance.ServerAuthenticate(EmailField.Text, PasswordField.Text, (authSuccess) =>
				{
					if( authSuccess )
					{
						SPServerData.Instance.ServerGetProfile( (profileSuccess, isFirstTime) =>
							{
								InvokeOnMainThread( () =>
									{
										SignInButton.IsActive = false;
										TopIconView.Image = UIImage.FromFile("Body/icon-parcel-home-red.png");

										if( profileSuccess )
										{
											if( SPSharedData.Instance.UserProfile.IsFirstTime )
											{
												SignInButton.Button.SetImage(UIImage.FromFile("Body/Welcome-First-Time-User-Buttons.png"), UIControlState.Normal);
												SignInButton.Button.SetImage(UIImage.FromFile("Body/Welcome-First-Time-User-Buttons.png"), UIControlState.Disabled);
												SignInButton.Button.Enabled = false;
												SignInButton.TitleLabel.Text = "Welcome, first time sign in";
												SignInButton.IconView.Image = UIImage.FromFile("Body/checked-linewhite.png");

												var timer = new Timer(2000);
												timer.Elapsed += (s, ev) => 
												{
													InvokeOnMainThread( () =>
														{
															CurrentState = ScreenState.FirstTimeFields;
															LayoutContentSubviews(CurrentState, true, null);
														});
													timer.Stop();
													timer = null;
												};
												timer.Start();
											}
											else
											{
												SPShared.Instance.SetIsLoggedIn(true, true, null);
												if( UIDevice.CurrentDevice.CheckSystemVersion(8,0) )
												{
													UIApplication.SharedApplication.RegisterUserNotificationSettings(UIUserNotificationSettings.GetSettingsForTypes(UIUserNotificationType.Alert|UIUserNotificationType.Badge, null));
													UIApplication.SharedApplication.RegisterForRemoteNotifications();
												}
												else
												{
													UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(UIRemoteNotificationType.Alert|UIRemoteNotificationType.Badge);
												}
											}
										}
										else
										{
											UIAlertView alert = new UIAlertView("Error Retrieving User Account", "Please try again, or contact support if the problem persists.", null, "OK");
											alert.Show();
										}
									});
							});
					}
					else
					{
						InvokeOnMainThread( () =>
							{
								UIAlertView alert = new UIAlertView("Invalid Email or Password", "Please check your details carefully and try again.", null, "OK");
								alert.Show();
								SignInButton.IsActive = false;
							});
					}
				});
			SignInButton.IsActive = true;
		}

		private void FirstTimeSaveChangesButtonPress()
		{
			FirstTimeUnitNumber.ResignFirstResponder();
			FirstTimeSaveChangesButton.IsActive = true;

			if( FirstTimeSearchAddressData != null )
			{
				SPServerData.Instance.ServerPushFirstTimeUserProfile( FirstTimeSearchAddressData.BuildingNo, FirstTimeSearchAddressData.StreetName, FirstTimeUnitNumber.Text, FirstTimePostCodeField.Text, FirstTimeOldPasswordField.Text, FirstTimeNewPassword1Field.Text, FirstTimeNewPassword2Field.Text, (success, message) =>
					{
						InvokeOnMainThread( () =>
							{
								if( success )
								{
									SPShared.Instance.SetIsLoggedIn(true, true, null);
								}
								else if( !string.IsNullOrEmpty(message))
								{
									UIAlertView alert = new UIAlertView("Error Updating Profile", message, null, "OK");
									alert.Show();
								}
								FirstTimeSaveChangesButton.IsActive = false;
							});
					});
			}
			else
			{
				UIAlertView alert = new UIAlertView("Error With Address", "Please try again, or contact support if the problem persists.", null, "OK");
				alert.Show();
				FirstTimeSaveChangesButton.IsActive = false;
			}
		}

		#region AnimatedState Logic
		private void LayoutContentSubviews(ScreenState state, bool animated, Action completion)
		{
			base.LayoutContentSubviews((int)state, animated, completion);

			switch( CurrentState )
			{
			case ScreenState.Initial:
				{
					TopIconView.Image = UIImage.FromFile("Body/icon-parcel-home-grey.png");
					SignInButton.Button.SetImage(UIImage.FromFile("Body/Sign-In.png"), UIControlState.Normal);
					SignInButton.Button.SetImage(UIImage.FromFile("Body/Sign-In.png"), UIControlState.Disabled);
					SignInButton.IconView.Image = UIImage.FromImage(UIImage.FromFile("Body/parcellistarrow-collected.png").CGImage, 1f, UIImageOrientation.Right);
					SignInButton.TitleLabel.Text = "Sign in";
					break;
				}
			case ScreenState.SignInFields:
				{
					TopIconView.Image = UIImage.FromFile("Body/icon-parcel-home-grey.png");
					SignInButton.Button.SetImage(UIImage.FromFile("Body/Sign-In.png"), UIControlState.Normal);
					SignInButton.Button.SetImage(UIImage.FromFile("Body/Sign-In.png"), UIControlState.Disabled);
					SignInButton.IconView.Image = UIImage.FromFile("Body/parcellistarrow-collected.png");
					SignInButton.TitleLabel.Text = "Sign in";
					break;
				}
			case ScreenState.FirstTimeFields:
				{
					TopIconView.Image = UIImage.FromFile("Body/icon-parcel-home-red.png");
					SignInButton.Button.SetImage(UIImage.FromFile("Body/Welcome-First-Time-User-Buttons.png"), UIControlState.Normal);
					SignInButton.Button.SetImage(UIImage.FromFile("Body/Welcome-First-Time-User-Buttons.png"), UIControlState.Disabled);
					SignInButton.Button.Enabled = false;
					SignInButton.TitleLabel.Text = "Welcome, first time sign in";
					SignInButton.IconView.Image = UIImage.FromFile("Body/checked-linewhite.png");

					TrackAndTracePopoutBar.Hide(true, () =>
						{
							((SPNavBar)TrackAndTracePopoutBar.ParentBar).SetBorderLineShouldHide(false, false);
							TrackAndTraceButton.Image = UIImage.FromFile("Bar/trackntrace.png");
						});

					break;
				}
			}

			ViewDidLayoutSubviews();
		}
		#endregion AnimatedState Logic
	}
}
