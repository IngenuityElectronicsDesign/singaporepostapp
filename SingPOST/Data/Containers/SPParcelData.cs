﻿using System;
using POPStation.Core.iOS;

namespace POPStationiOS
{
	/*public class SPParcelData
	{
		public string MerchantName { get; private set; }
		public string ParcelNumber { get; private set; }
		public string TrackingStatus { get; private set; }
		public ParcelStatus ParcelStatus { get; private set; }

		public SPParcelData(SPParcelData data) : this(data.MerchantName, data.ParcelNumber, data.TrackingStatus, data.ParcelStatus) { }

		public SPParcelData(string merchantName, string parcelNumber, string trackingStatus, ParcelStatus parcelStatus)
		{
			MerchantName = merchantName;
			ParcelNumber = parcelNumber;
			TrackingStatus = trackingStatus;
			ParcelStatus = parcelStatus;
		}

		public string GetIconFileNameForStatus()
		{
			switch(ParcelStatus)
			{
				case ParcelStatus.PendingShipment:
				case ParcelStatus.InProcessing:
					return "pending.png";
				case ParcelStatus.WithDeliveryCourier:
				case ParcelStatus.TransitToAnotherPOPStationPostOfﬁce:
					return "courier.png";
				case ParcelStatus.RecoveredItemFromPOPStationForServiceRecovery:
				case ParcelStatus.RecoveredExpiredItemFromPOPStationAndRedirectedToPostOfﬁce:
				case ParcelStatus.FailedDropOffDeliveryAtPOPStation:
					return "redirected.png";
				case ParcelStatus.DeliveredToPOPStationByCourier:
				case ParcelStatus.DeliveredToPostOfﬁce:
					return "delivered.png";
				case ParcelStatus.UnsuccessfulDelivery:
					return "failed.png";
				case ParcelStatus.CollectedAtPOPStationByCustomer:
				case ParcelStatus.CollectedAtPostOfﬁceByCustomer:
				case ParcelStatus.FinalDelivery:
					return "collected.png";
				default:
					Console.WriteLine ("Unrecognized status.  Returning pending.png");
					return "pending.png";
			}
		}
	}*/
}

