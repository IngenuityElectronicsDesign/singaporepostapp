﻿using POPStation.Core.iOS;
using System;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.CoreLocation;
using MonoTouch.UIKit;

namespace POPStationiOS
{
	public class SPShared
	{
		#region Const Defs
		// Layout Defs
		// Notification Strings
		#endregion Const Defs

		#region Properties
		public bool IsWidescreen { get; private set; }
		public SPNavMenuTableViewController NavMenuTableViewController { get; set; }

		// Color
		public UIColor RedTintColor { get; private set; }
		public UIColor BlueTintColor { get; private set; }

		// Server
		public bool IsLoggedIn
		{
			get
			{
				return SPSharedData.Instance.IsLoggedIn;
			}
			private set	{ }
		}
		public bool PushNotificationsEnabled
		{
			get
			{
				return SPSharedData.Instance.PushNotificationsEnabled;
			}
			set
			{
				if( value ) // Push Notifications Enabled
				{

				}
				SPSharedData.Instance.PushNotificationsEnabled = value;
			}
		}
		#endregion Properties

		#region Singleton Instance
		private static SPShared m_Instance;
		public static SPShared Instance
		{
			get
			{
				if( m_Instance == null )
				{
					m_Instance = new SPShared();
				}
				return m_Instance;
			}
		}
		#endregion Singeton Instance

		// Singleton requires a private constructor.
		private SPShared ()
		{
			// System
			IsWidescreen = false;
			if( UIScreen.MainScreen.Bounds.Size.Height == 568 )
			{
				IsWidescreen = true;
			}
				
			PushNotificationsEnabled = true;

			// iBeacon Setup
//			if( CLLocationManager.IsMonitoringAvailable(typeof(CLBeaconRegion)) )
//			{
//				iBeaconComms.Instance.RegionEnteredAction += (e) =>
//				{
//					if( e.Region.Identifier == iBeaconComms.BeaconID )
//					{
//						UILocalNotification notification = new UILocalNotification() { AlertBody = "You are near a POPStation." };
//						UIApplication.SharedApplication.PresentLocationNotificationNow(notification);
//					}
//				};
//			}

			// Color
			RedTintColor = UIColor.FromRGB(0.83f, 0.25f, 0.21f);
			BlueTintColor = UIColor.FromRGB(0.2f, 0.43f, 0.65f);

			// Setup font in TableViews
			//UILabel.AppearanceWhenContainedIn(typeof(UITableViewHeaderFooterView)).Font = UIFont.FromName("AvenirNextCondensed-Regular", 16f);

			UIWebView webView = new UIWebView(System.Drawing.RectangleF.Empty);
			string userAgent = webView.EvaluateJavascript("navigator.userAgent") + " POPStation";

			Console.WriteLine(userAgent);

			NSUserDefaults.StandardUserDefaults.RegisterDefaults(new NSDictionary("UserAgent", userAgent));
		}
			
		public void SetIsLoggedIn(bool isLoggedIn, bool animated, Action completion)
		{
			SPSharedData.Instance.IsLoggedIn = isLoggedIn;
			NavMenuTableViewController.ShouldTransition(0, animated, completion);
		}
	}
}
